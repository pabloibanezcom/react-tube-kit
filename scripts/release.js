/* eslint-disable import/no-extraneous-dependencies */
// Ensure environment variables are read.
process.env.NODE_ENV = 'development';
require('../config/env');
const axios = require('axios');

const start = async () => {
  const branchName = process.argv[2].replace('--branchName=', '');
  const projectId = process.argv[3].replace('--projectId=', '');
  const privateToken = process.argv[4].replace('--token=', '');
  const version = branchName.replace('release-', '');

  console.log(`branchName: ${branchName}`);
  console.log(`projectId: ${projectId}`);
  console.log(`version: ${version}`);

  const gitLabApi = axios.create({
    baseURL: `https://gitlab.com/api/v4/projects/${projectId}`,
    headers: { 'Private-Token': privateToken }
  });

  // Create tag
  const tagResult = await gitLabApi.post('repository/tags', {
    id: projectId,
    tag_name: `v${version}`,
    ref: branchName,
    release_description: 'View CHANGELOG.md to see details.'
  });

  console.log(tagResult);

  console.log(`Tag v${version} was created`);

  // Create release
  // const releaseResult = await gitLabApi.post('releases', {
  //   id: projectId,
  //   name: `v${version}`,
  //   tag_name: `v${version}`,
  //   ref: branchName,
  //   description: 'View CHANGELOG.md to see details.'
  // });

  // console.log(releaseResult);

  // console.log(`Release v${version} was created`);
};

start();
