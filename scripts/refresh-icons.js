/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs');
const path = require('path');
const replace = require('replace-in-file');
const glob = require('glob');
const svgr = require('@svgr/core').default;

const ICONS_SOURCE_DIR = 'src/lib/assets/icons/svg';
const COMPONENTS_DIR = 'src/lib/components/icon/icons';

const refreshIcons = async () => {
  const iconsObj = {};
  let componentsImportStr = '';
  let componentsSwitchStr = '';
  let svgImportStr = '';
  let svgObjStr = '';

  const fromFileNameToComponentName = str => {
    return str
      .replace('.svg', '')
      .split('-')
      .map(s => s.charAt(0).toUpperCase() + s.slice(1))
      .join('');
  };

  const generateIconComponents = () => {

    const generateIconComponent = icon => {
      const svg = fs.readFileSync(icon, 'utf8');
      const componentName = path.parse(icon).name;
      console.log(componentName);
    }

    const icons = glob.sync(`${ICONS_SOURCE_DIR}/**.svg`);
    for (const icon of icons) {
      generateIconComponent(icon);
    }
  }

  fs.readdirSync('src/lib/assets/icons/svg').forEach(file => {
    const fileName = file.replace('.svg', '');

    const componentName = fromFileNameToComponentName(file);
    componentsImportStr += `import ${componentName} from '../icons/${componentName}';\n`;
    componentsSwitchStr += `case '${fileName}':\nreturn ${componentName};\n`;

    svgImportStr += `import ${componentName} from './svg/${file}';\n`;
    svgObjStr += `'${fileName}': ${componentName},\n`;

    const svgData = fs.readFileSync(`src/lib/assets/icons/svg/${file}`, 'utf8');
    iconsObj[fileName] = svgData;
  });

  await fs.copyFileSync(
    'src/lib/components/icon/util/getIconReactComponent-tpl.js',
    'src/lib/components/icon/util/getIconReactComponent.js'
  );

  await replace({
    files: './src/lib/components/icon/util/getIconReactComponent.js',
    from: '// THIS IS A TEMPLEATE FILE. DO NOT MODIFY IT!',
    to: componentsImportStr
  });

  await replace({
    files: './src/lib/components/icon/util/getIconReactComponent.js',
    from: '// CASES HERE',
    to: componentsSwitchStr
  });

  await fs.copyFileSync('src/lib/assets/icons/index-tpl.js', 'src/lib/assets/icons/index.js');

  await replace({
    files: './src/lib/assets/icons/index.js',
    from: '// THIS IS A TEMPLEATE FILE. DO NOT MODIFY IT!',
    to: svgImportStr
  });

  await replace({
    files: './src/lib/assets/icons/index.js',
    from: '// ICONS HERE',
    to: svgObjStr
  });

  fs.writeFileSync('src/lib/assets/icons/icons.json', JSON.stringify(iconsObj));

  console.log('SVG icon file & Icon components file generated');
};

refreshIcons();
