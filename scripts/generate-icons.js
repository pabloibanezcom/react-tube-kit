const fs = require('fs');
const path = require('path');
const glob = require('glob');
const svgr = require('@svgr/core').default;

const ICONS_SOURCE_DIR = 'src/lib/assets/icons/svg';
const COMPONENTS_DIR = 'src/lib/components/icon/icons';

const fromFileNameToComponentName = str => {
  return str
    .replace('.svg', '')
    .split('-')
    .map(s => s.charAt(0).toUpperCase() + s.slice(1))
    .join('');
};

// Template to generate named exports instaed of default ones
const iconComponentTemplate = (
  { template },
  opts,
  { imports, componentName, jsx }
) =>
  template.smart({ plugins: ['jsx'] }).ast`
        // eslint-disable jsx-props-no-spreading
        ${imports}
        ${'\n'}
        export const ${componentName} = props => ${jsx};
        ${'\n'}
        export default ${componentName};
    `;

const icons = glob.sync(`${ICONS_SOURCE_DIR}/**.svg`);

for (const icon of icons) {
  const svg = fs.readFileSync(icon, 'utf8');
  const componentName = fromFileNameToComponentName(path.parse(icon).name);
  const componentCode = svgr.sync(
    svg,
    {
      template: iconComponentTemplate,
      // 1. Clean SVG files using SVGO
      // 2. Generate JSX
      // 3. Format the result using Prettier
      plugins: [
        '@svgr/plugin-svgo',
        '@svgr/plugin-jsx',
        '@svgr/plugin-prettier'
      ],
      // Replace hardcoded colors with `currentColor`
      svgoConfig: {
        plugins: {removeViewBox: false }
      }
    },
    { componentName }
  );
  fs.writeFileSync(
    `${COMPONENTS_DIR}/${componentName}.jsx`,
    componentCode
  );
}