/* eslint-disable import/no-extraneous-dependencies */
// Ensure environment variables are read.
process.env.NODE_ENV = 'development';
require('../config/env');
const fs = require('fs');
const axios = require('axios');
const NodeGit = require('nodegit');
const pathToRepo = require('path').resolve('.git');
const simpleGit = require('simple-git/promise')();
const components = require('../src/demo/components.json');
const releases = require('../src/demo/releases.json');

const gitLabApi = axios.create({
  baseURL: `https://gitlab.com/api/v4/projects/${process.env.GITLAB_REPO_ID}`,
  headers: { 'Private-Token': process.env.GITLAB_PERSONAL_ACCESS_TOKEN }
});

const updateLocalPackageVersion = version => {
  ['./package.json', './package-lock.json'].forEach(filePath => {
    const fileData = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    fs.writeFileSync(filePath, JSON.stringify({ ...fileData, version }, null, 2));
  });
};

const getFileFromBranch = (filePath, branch) => {
  return gitLabApi.get(`repository/files/${filePath}/raw?ref=${branch || 'master'}`);
};

const getCurrentBranchName = async () => {
  return new Promise((resolve, reject) => {
    NodeGit.Repository.open(pathToRepo)
      .then(repo => {
        return repo.getCurrentBranch();
      })
      .then(branchRef => {
        return NodeGit.Branch.name(branchRef);
      })
      .then(branchName => {
        resolve(branchName);
      })
      .catch(err => reject(err));
  });
};

const getNewVersion = (currentVersion, versionLevel) => {
  const result = currentVersion.split('.').map(n => parseInt(n, 10));
  if (versionLevel === 'patch') {
    result[2] += 1;
  }
  if (versionLevel === 'minor') {
    result[1] += 1;
    result[2] = 0;
  }
  if (versionLevel === 'major') {
    result[0] += 1;
    result[1] = 0;
    result[2] = 0;
  }
  return result.join('.');
};

const compareBranches = async (sourceBranch, targetBranch) => {
  return new Promise((resolve, reject) => {
    gitLabApi
      .get(`repository/compare?from=${sourceBranch}&to=${targetBranch}`)
      .then(res => {
        resolve(res.data.diffs);
      })
      .catch(err => reject(err));
  });
};

const analyseDiffFiles = (files, releaseData) => {
  const componentsChanges = {};

  const checkChangeInFile = (file, ext) => {
    if (ext === 'jsx' && file.new_file) {
      return 'added';
    }
    if (ext === 'jsx' && file.deleted_file) {
      return 'removed';
    }
    return 'modified';
  };

  const applyComponentChange = (componentPath, change) => {
    if (!componentsChanges[componentPath] || componentsChanges[componentPath] === 'modified') {
      componentsChanges[componentPath] = change;
    }
  };

  files.forEach(file => {
    // component file
    if (file.new_path.startsWith('src/lib/components')) {
      const pathElements = file.new_path.split('/');
      // exclude tests files
      if (!pathElements.slice(-1)[0].includes('.test.js')) {
        const componentPath = pathElements[pathElements.length - 2];
        applyComponentChange(
          componentPath,
          checkChangeInFile(
            file,
            pathElements
              .slice(-1)[0]
              .split('.')
              .slice(-1)[0]
          )
        );
      }
    }
  });

  Object.keys(componentsChanges).forEach(key => {
    releaseData[componentsChanges[key]].push(components.find(c => c.url === key).component);
  });
};

const appendNewReleaseData = releaseData => {
  const updatedReleases = [releaseData].concat(releases);
  fs.writeFileSync('./src/demo/releases.json', JSON.stringify(updatedReleases, null, 2));
};

const start = async () => {
  // Initial releaseData
  const releaseData = {
    version: null,
    date: new Date().toISOString().substring(0, 10),
    added: [],
    modified: [],
    removed: []
  };

  // Get version level
  let versionLevel = JSON.parse(process.env.npm_config_argv)
    .original.slice(-1)[0]
    .replace('--', '');

  if (!['major', 'minor', 'patch'].includes(versionLevel)) {
    versionLevel = 'patch';
  }

  // Check current branch
  const currentBranchName = await getCurrentBranchName();

  // Get current version from master and generate new version
  const packageRes = await getFileFromBranch('package.json');
  const currentVersion = packageRes.data.version;
  releaseData.version = getNewVersion(currentVersion, versionLevel);

  // Get diffFiles between last release branch and current branch
  const fileDiffs = await compareBranches(`release-${currentVersion}`, currentBranchName);

  // Apply diffFiles to releaseData and append to releases file
  // analyseDiffFiles(fileDiffs, releaseData);
  // appendNewReleaseData(releaseData);

  // Upgrade version package.json
  updateLocalPackageVersion(releaseData.version);

  // Create local branch for new release
  const branchName = `release-${releaseData.version}`;
  await simpleGit.checkoutLocalBranch(branchName);

  // Create commit and push to remote
  await simpleGit.commit(`v.${releaseData.version}`, [
    'package.json',
    'package-lock.json',
    'src/demo/releases.json'
  ]);
  await simpleGit.push('origin', branchName);

  // Create merge request
  const mergeRequestRes = await gitLabApi.post(`merge_requests`, {
    id: process.env.GITLAB_REPO_ID,
    source_branch: branchName,
    target_branch: 'master',
    title: `Release v${releaseData.version}`
  });

  console.log('Merge request "Release v0.1.2" successfully created!');
  console.log(mergeRequestRes.data.web_url);

  console.log('Done!');
};

start();
