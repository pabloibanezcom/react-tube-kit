const fs = require('fs');
const ncp = require('ncp').ncp;

const packageJson = require('../package.json');

// Ensure environment variables are read.
process.env.NODE_ENV = 'production';
require('../config/env');

const start = async () => {
  const transformPackageJson = () => {
    return {
      name: packageJson.name,
      description: packageJson.description,
      version: packageJson.version,
      private: packageJson.private,
      repository: packageJson.repository,
      main: 'index.js',
      module: 'index.js',
      dependencies: Object.keys(packageJson.dependencies)
        .filter(key => packageJson.distDependencies.includes(key))
        .reduce((obj, key) => {
          obj[key] = packageJson.dependencies[key];
          return obj;
        }, {})
    };
  };

  fs.writeFileSync('./dist/package.json', JSON.stringify(transformPackageJson(), null, 2));

  if (process.env.EXTERNAL_DIST_FOLDER) {
    ncp('./dist/', process.env.EXTERNAL_DIST_FOLDER);
  }
};

start();
