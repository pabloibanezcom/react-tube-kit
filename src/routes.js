import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Demo from './demo/demo';
import BottomNavbarBasic from './demo/pages/components/bottom-navbar/bottom-navbar-basic/bottom-navbar-basic';
import BottomNavbarButtonElement from './demo/pages/components/bottom-navbar/bottom-navbar-button-element/bottom-navbar-button-element';
import BottomNavbarColors from './demo/pages/components/bottom-navbar/bottom-navbar-colors/bottom-navbar-colors';
import BottomNavbarIconsOnly from './demo/pages/components/bottom-navbar/bottom-navbar-icons-only/bottom-navbar-icons-only';
import BottomNavbarScrollHide from './demo/pages/components/bottom-navbar/bottom-navbar-scroll-hide/bottom-navbar-scroll-hide';
import SideNavbarBasic from './demo/pages/components/side-navbar/side-navbar-basic/side-navbar-basic';
import SideNavbarDisplay from './demo/pages/components/side-navbar/side-navbar-display/side-navbar-display';
import TopNavbarBasic from './demo/pages/components/top-navbar/top-navbar-basic/top-navbar-basic';
import TopNavbarColors from './demo/pages/components/top-navbar/top-navbar-colors/top-navbar-colors';
import TopNavbarMenuButtonPosition from './demo/pages/components/top-navbar/top-navbar-menu-button-position/top-navbar-menu-button-position';
import TopNavbarRightContent from './demo/pages/components/top-navbar/top-navbar-right-content/top-navbar-right-content';
import TopNavbarScrollHide from './demo/pages/components/top-navbar/top-navbar-scroll-hide/top-navbar-scroll-hide';
import LayoutPlayground from './demo/pages/layout/playground/playground';

const routes = () => (
  <Switch>
    <Route path="/layout/playground" component={LayoutPlayground} />
    <Route path="/components/bottom-navbar/basic" component={BottomNavbarBasic} />
    <Route path="/components/bottom-navbar/scroll-hide" component={BottomNavbarScrollHide} />
    <Route path="/components/bottom-navbar/colors" component={BottomNavbarColors} />
    <Route path="/components/bottom-navbar/icons-only" component={BottomNavbarIconsOnly} />
    <Route path="/components/bottom-navbar/button-element" component={BottomNavbarButtonElement} />
    <Route path="/components/side-navbar/basic" component={SideNavbarBasic} />
    <Route path="/components/side-navbar/display" component={SideNavbarDisplay} />
    <Route path="/components/top-navbar/basic" component={TopNavbarBasic} />
    <Route path="/components/top-navbar/right-content" component={TopNavbarRightContent} />
    <Route
      path="/components/top-navbar/menu-button-position"
      component={TopNavbarMenuButtonPosition}
    />
    <Route path="/components/top-navbar/scroll-hide" component={TopNavbarScrollHide} />
    <Route path="/components/top-navbar/colors" component={TopNavbarColors} />
    <Route path="/" component={Demo} />
    <Redirect to="/" />
  </Switch>
);

export default routes;
