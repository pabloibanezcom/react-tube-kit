import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import routes from './routes';

function App() {
  return (
    <Router>
      <div id="site-container" className="site-container">
        {routes()}
        <div id="toastr-container" />
      </div>
    </Router>
  );
}

export default App;
