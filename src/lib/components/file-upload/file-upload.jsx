/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unneeded-ternary */
import React, { Fragment, useRef, useState } from 'react';
import { chunkArray } from '../../util/array';
import { generateComponentProps } from '../../util/component';
import getFileIconName from '../../util/getFileIconName';
import ButtonLink from '../button-link/button-link';
import Icon from '../icon/icon';
import Image from '../image/image';
import Input from '../input/input';
import componentData from './file-upload.data.json';

const FileUpload = ({
  accept,
  className,
  color,
  filesLimit,
  hoverColor,
  id,
  inputProps,
  inputFileProps,
  imageProps,
  name,
  previewsPerRow,
  showPreview,
  style,
  onChange
}) => {
  const inputFileEl = useRef(null);

  const [files, setFiles] = useState([]);
  const [error, setError] = useState(null);

  const getFilesArray = _files => {
    const result = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0, numFiles = _files.length; i < numFiles; i++) {
      result.push(_files[i]);
    }
    return result;
  };

  const handleFileInputOpen = () => {
    inputFileEl.current.click();
  };

  const handleFileSelected = () => {
    const newFiles =
      files.length < filesLimit
        ? [...files, ...getFilesArray(inputFileEl.current.files)]
        : getFilesArray(inputFileEl.current.files);
    if (newFiles.length > filesLimit) {
      setError('Too many files');
    } else {
      setFiles(newFiles);
      setError(null);
      onChange(newFiles);
    }
  };

  const handleRemoveFile = fileName => {
    setFiles(files.filter(file => file.name !== fileName));
  };

  const handleClearExistingValue = evt => {
    evt.target.value = '';
  };

  const renderFileName = file => {
    return (
      <div className="d-flex align-items-center justify-content-between mt-2 ml-1 mr-1">
        <div className="text-secondary font-weight-normal font-size-13">
          <Icon className="mr-1" size="sm" iconName={getFileIconName(file)} />
          {file.name}
        </div>
        <ButtonLink type="link" color="secondary" onClick={() => handleRemoveFile(file.name)}>
          <Icon iconName="close" size="sm" />
        </ButtonLink>
      </div>
    );
  };

  const renderFilePreview = file => {
    return (
      <Image
        className="mt-2"
        actionText="Remove image"
        showOverlay
        src={URL.createObjectURL(file)}
        onClick={() => handleRemoveFile(file.name)}
        {...imageProps}
      />
    );
  };

  const renderFiles = () => {
    const renderRow = row => {
      return (
        <div className="row">
          {row.map(file => (
            <div key={file.name} className={`col-${12 / previewsPerRow}`}>
              {renderFilePreview(file)}
            </div>
          ))}
        </div>
      );
    };

    return !showPreview
      ? files.map(file => <Fragment key={file.name}>{renderFileName(file)}</Fragment>)
      : chunkArray(files, previewsPerRow).map((row, i) => {
          return <Fragment key={i}>{renderRow(row)}</Fragment>;
        });
  };

  const classList = [];
  const componentStyle = { ...style };

  // custom className
  classList.push(className);

  return (
    <div>
      <Input
        className={classList.filter(c => c && !c.includes('null')).join(' ')}
        style={componentStyle}
        disabled={files.length >= filesLimit}
        color={color}
        hoverColor={error ? 'danger' : hoverColor}
        placeholder={
          !(!files.length < filesLimit && files[0]) ? 'Click here to upload an image' : ''
        }
        extensionProps={{
          onClick: handleFileInputOpen,
          onKeyDown: e => {
            e.preventDefault();
          }
        }}
        {...inputProps}
      />
      {error ? <div className="font-size-13 font-weight-normal text-danger">{error}</div> : null}
      <Input
        id={id || undefined}
        name={name || undefined}
        className="d-none"
        type="file"
        accept={accept}
        ref={inputFileEl}
        multiple={filesLimit > 1}
        onClick={handleClearExistingValue}
        onChange={handleFileSelected}
        {...inputFileProps}
      />
      {files[0] ? renderFiles() : null}
    </div>
  );
};

Object.assign(FileUpload, generateComponentProps(componentData));

export default FileUpload;
