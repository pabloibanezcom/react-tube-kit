// import { mount, shallow } from 'enzyme';
// import React from 'react';
// import Input from '../input/input';
// import FileUpload from './file-upload';

// describe('<FileUpload />', () => {
//   let wrapper;

//   it('renders <FileUpload /> component', () => {
//     wrapper = shallow(<FileUpload />);
//     expect(wrapper.find(Input)).toHaveLength(2);
//     expect(
//       wrapper
//         .find(Input)
//         .first()
//         .prop('type')
//     ).toBe('text');
//     expect(
//       wrapper
//         .find(Input)
//         .last()
//         .prop('type')
//     ).toBe('file');
//     expect(
//       wrapper
//         .find(Input)
//         .last()
//         .prop('className')
//     ).toBe('d-none');
//     expect(wrapper).toMatchSnapshot();
//   });

//   describe('id, name, className & style', () => {
//     beforeAll(() => {
//       wrapper = shallow(
//         <FileUpload
//           id="fakeId"
//           name="fakeName"
//           className="fakeClassName"
//           style={{ color: 'red' }}
//         />
//       );
//     });

//     it('applies className and style to visual input', () => {
//       expect(
//         wrapper
//           .find(Input)
//           .first()
//           .prop('className')
//       ).toBe('fakeClassName');

//       expect(
//         wrapper
//           .find(Input)
//           .first()
//           .prop('style')
//       ).toEqual({ color: 'red' });
//     });

//     it('applies id and name to file input', () => {
//       expect(
//         wrapper
//           .find(Input)
//           .last()
//           .prop('id')
//       ).toBe('fakeId');

//       expect(
//         wrapper
//           .find(Input)
//           .last()
//           .prop('name')
//       ).toBe('fakeName');
//     });
//   });

//   describe('filesLimit', () => {
//     it('set multiple prop in file Input when filesLimit is over 1', () => {
//       wrapper = mount(<FileUpload filesLimit={2} />);
//       expect(
//         wrapper
//           .find(Input)
//           .last()
//           .prop('multiple')
//       ).toBe(true);
//     });
//   });
// });
