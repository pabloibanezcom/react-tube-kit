import dark from './dark.json';
import plain from './plain.json';

export default { plain, dark };
