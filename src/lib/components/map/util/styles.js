import predefinedStyles from '../styles';

export const getMapStyles = styles => {
  if (!styles) {
    return null;
  }
  let result = [];
  styles.forEach(style => {
    if (typeof style !== 'string') {
      result = result.concat(style);
    } else {
      result = result.concat(predefinedStyles[style]);
    }
  });

  return result;
};

export const refreshMapStyles = (map, styles) => {
  const styledMapType = new window.google.maps.StyledMapType(getMapStyles(styles), {
    name: 'Styled Map'
  });

  map.mapTypes.set('styled_map', styledMapType);
  map.setMapTypeId('styled_map');
};
