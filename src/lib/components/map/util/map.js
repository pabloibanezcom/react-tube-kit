import { getMapStyles } from './styles';

export const createMap = (mapId, { center, zoom, maxZoom, minZoom, mapStyles, onClick }) => {
  const htmlElement = document.getElementById(mapId);
  if (!htmlElement) {
    return null;
  }
  const map = new window.google.maps.Map(htmlElement, {
    center,
    zoom,
    maxZoom,
    minZoom,
    disableDefaultUI: true,
    styles: getMapStyles(mapStyles)
  });

  if (onClick) {
    map.addListener('click', evt => {
      onClick(evt);
    });
  }

  return map;
};

export const refreshMapCenter = (map, center) => {
  map.panTo(center);
};

export const refreshMapZoom = (map, zoom) => {
  map.setZoom(zoom);
};
