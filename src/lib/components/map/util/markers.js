import { getColoredIconUrl } from '../../../util/icon';

export const refreshMarkers = (map, markers, currentMarkers, mapProps) => {
  const markersWithId = markers.map(m => {
    return { ...m, id: generateMarkerId(m) };
  });

  const newMarkers = [];

  markersWithId.forEach(marker => {
    const indexOfMarker = currentMarkers.map(m => m.id).indexOf(marker.id);
    // new marker
    if (indexOfMarker < 0) {
      newMarkers.push(addMarkerToMap(map, marker, mapProps));
    } else {
      // check if updated marker
      newMarkers.push(checkUpdatedMarker(marker, currentMarkers[indexOfMarker]));
    }
  });

  // oldMarkers;
  const oldMarkers = currentMarkers.filter(m => !markersWithId.map(_m => _m.id).includes(m.id));
  oldMarkers.forEach(m => removeMarkerFromMap(m));

  return newMarkers;
};

export const clearMarkers = currentMarkers => {
  currentMarkers.forEach(marker => {
    marker.setMap(null);
  });
};

const addMarkerToMap = (map, marker, mapProps) => {
  const newMarker = new window.google.maps.Marker({
    ...marker,
    icon: generateMarkerSVG(
      marker.markerName || mapProps.markerName,
      marker.color || mapProps.markerColor,
      marker.borderColor || mapProps.markerBorderColor,
      marker.size || mapProps.markerSize,
      marker.rotation || mapProps.markerRotation
    ),
    animation: getMarkerAnimation(marker, mapProps.markerAnimation)
  });

  newMarker.setMap(map);
  return newMarker;
};

const removeMarkerFromMap = marker => {
  marker.setMap(null);
};

const checkUpdatedMarker = (newMarker, currentMarker) => {
  if (
    newMarker.position.lat !== currentMarker.getPosition().lat() ||
    newMarker.position.lng !== currentMarker.getPosition().lng()
  ) {
    currentMarker.setPosition(newMarker.position);
  }
  return currentMarker;
};

const generateMarkerSVG = (name, color, borderColor, size, rotation) => {
  return {
    url: getColoredIconUrl(name, color, borderColor, rotation),
    scaledSize: new window.google.maps.Size(size, size)
  };
};

const generateMarkerId = marker => {
  return typeof marker.id !== 'undefined'
    ? marker.id
    : `${typeof marker.position.lat === 'function' ? marker.position.lat() : marker.position.lat}|${
        typeof marker.position.lng === 'function' ? marker.position.lng() : marker.position.lng
      }`;
};

const getMarkerAnimation = (marker, markerAnimation) => {
  if (marker.animation && ['drop', 'bounce'].includes(marker.animation)) {
    return window.google.maps.Animation[marker.animation.toUpperCase()];
  }
  if (marker.animation === 'none') {
    return null;
  }
  if (['drop', 'bounce'].includes(markerAnimation)) {
    return window.google.maps.Animation[markerAnimation.toUpperCase()];
  }
  return null;
};
