/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import randomId from '../../util/randomId';
import componentData from './map.data.json';
import { createMap, refreshMapCenter, refreshMapZoom } from './util/map';
import { refreshMarkers } from './util/markers';
import { refreshMapStyles } from './util/styles';

const Map = ({
  center,
  className,
  // eslint-disable-next-line no-unused-vars
  dynamicCenter,
  height,
  id,
  mapStyles,
  markers,
  markerAnimation,
  markerColor,
  markerBorderColor,
  markerName,
  markerRotation,
  markerSize,
  maxZoom,
  minZoom,
  name,
  style,
  width,
  zoom,
  onClick,
  onMapLoaded
}) => {
  // eslint-disable-next-line no-unused-vars
  const [map, setMap] = useState(null);
  const [currentMarkers, setCurrentMarkers] = useState([]);
  const mapId = id || `map-${randomId()}`;

  // Map
  useEffect(() => {
    if (!map) {
      const refreshIntervalId = setInterval(() => {
        if (window.google && window.google.maps) {
          clearInterval(refreshIntervalId);
          initMap();
        }
      }, 500);
    }
  }, []);

  // Center
  useEffect(() => {
    if (map) {
      refreshMapCenter(map, center);
    }
  }, [center]);

  // Zoom
  useEffect(() => {
    if (map) {
      refreshMapZoom(map, zoom);
    }
  }, [zoom]);

  // Markers
  useEffect(() => {
    if (map && markers) {
      refreshAndSetMarkers(map, markers);
    }
  }, [markers]);

  // Styles
  useEffect(() => {
    if (map && mapStyles) {
      refreshMapStyles(map, mapStyles);
    }
  }, [mapStyles]);

  const initMap = () => {
    const newMap = createMap(mapId, {
      center,
      zoom,
      maxZoom,
      minZoom,
      mapStyles,
      onClick
    });

    if (!newMap) {
      return;
    }

    if (markers) {
      refreshAndSetMarkers(newMap, markers);
    }
    setMap(newMap);
    onMapLoaded(newMap);
  };

  const refreshAndSetMarkers = (_map, _markers) => {
    setCurrentMarkers(
      refreshMarkers(_map, _markers, currentMarkers, {
        markerAnimation,
        markerColor,
        markerBorderColor,
        markerName,
        markerSize,
        markerRotation
      })
    );
  };

  const classList = ['map'];
  const componentStyle = { ...style };

  // custom className
  classList.push(className);

  // height and width
  componentStyle.height = height;
  componentStyle.width = width;

  return (
    <div
      id={mapId}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    />
  );
};

Object.assign(Map, generateComponentProps(componentData));

export default Map;
