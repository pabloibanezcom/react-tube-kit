import React from 'react';
import { generateComponentProps } from '../../util/component';
import Icon from '../icon/icon';
import componentData from './panel.data.json';

const panel = ({
  contentClassName,
  contentStyle,
  color,
  fontColor,
  children,
  className,
  headerColor,
  headerFontColor,
  headerIcon,
  headerStyle,
  headerText,
  id,
  name,
  style
}) => {
  const classList = ['panel'];
  const componentStyle = { ...style };

  const headerClassList = ['panel-header'];
  const headerInlineStyle = { ...headerStyle };

  const contentClassList = ['panel-content'];
  const contentInlineStyle = { ...contentStyle };

  // border radius
  classList.push('rounded');
  if (headerText) {
    headerClassList.push('rounded-top');
    contentClassList.push('rounded-bottom');
  } else {
    contentClassList.push('rounded');
  }

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else if (color) {
    classList.push(`bg--${color}`);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    componentStyle.color = fontColor;
  } else if (fontColor) {
    classList.push(`text--${fontColor}`);
  }

  // headerColor
  if (headerColor && headerColor.startsWith('#')) {
    headerInlineStyle.backgroundColor = headerColor;
  } else if (headerColor) {
    headerClassList.push(`bg--${headerColor}`);
  }

  // headerFontColor
  if (headerFontColor && headerFontColor.startsWith('#')) {
    headerInlineStyle.color = headerFontColor;
  } else if (headerFontColor) {
    classList.push(`text--${headerFontColor}`);
  }

  // contentClassName
  contentClassList.push(contentClassName);

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      {headerText ? (
        <div
          className={headerClassList.filter(c => c && !c.includes('null')).join(' ')}
          style={headerInlineStyle}
        >
          <span className="mb-0 mt-0 d-flex align-items-center font-weight-normal">
            {headerIcon ? <Icon iconName={headerIcon} className="mr-2" /> : null}
            {headerText}
          </span>
        </div>
      ) : null}
      <div
        className={contentClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={contentInlineStyle}
      >
        {children}
      </div>
    </div>
  );
};

Object.assign(panel, generateComponentProps(componentData));

export default panel;
