import { shallow } from 'enzyme';
import React from 'react';
import Panel from './panel';

describe('<Panel />', () => {
  let wrapper;

  it('renders <Panel /> component', () => {
    wrapper = shallow(
      <Panel>
        <div className="fake-panel__content">Fake panel content</div>
      </Panel>
    );
    expect(wrapper.find('.fake-panel__content').text()).toBe('Fake panel content');
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Panel className="my-class">Fake panel content</Panel>);
      expect(wrapper.find('.panel.my-class')).toHaveLength(1);
    });
  });

  describe('color & fontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<Panel>Fake panel content</Panel>);
      expect(wrapper.find('.bg--transparent')).toHaveLength(1);

      wrapper = shallow(<Panel color="primary">Fake panel content</Panel>);
      expect(wrapper.find('.bg--primary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(
        <Panel color="#1aaa55" fontColor="#222222">
          Fake panel content
        </Panel>
      );
      expect(wrapper.get(0).props.style).toHaveProperty('backgroundColor', '#1aaa55');
      expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('headerColor & headerFontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<Panel headerText="Fake panel header">Fake panel content</Panel>);
      expect(wrapper.find('.panel-header.bg--primary')).toHaveLength(1);

      wrapper = shallow(
        <Panel headerText="Fake panel header" headerColor="secondary">
          Fake panel content
        </Panel>
      );
      expect(wrapper.find('.panel-header.bg--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(
        <Panel headerText="Fake panel header" headerColor="#1aaa55" headerFontColor="#222222">
          Fake panel content
        </Panel>
      );
      expect(wrapper.find('.panel-header').get(0).props.style).toHaveProperty(
        'backgroundColor',
        '#1aaa55'
      );
      expect(wrapper.find('.panel-header').get(0).props.style).toHaveProperty('color', '#222222');
    });
  });
});
