import React, { useState } from 'react';
import { generateComponentProps } from '../../util/component';
import Icon from '../icon/icon';
import componentData from './collapsible-heading.data.json';

const CollapsibleHeading = ({
  children,
  className,
  collapsed,
  headingLevel,
  headingContent,
  id,
  name,
  style
}) => {
  // eslint-disable-next-line no-unused-vars
  const [isCollapsed, setIsCollapsed] = useState(collapsed);
  const classList = ['collapsible-heading'];
  const componentStyle = { ...style };

  const CustomTag = `h${headingLevel}`;

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <div className="d-flex align-items-center">
        <CustomTag className="flex-grow-1 right-line mb-4">{headingContent}</CustomTag>
        <a
          className="pl-4 mb-4 text-hover--secondary collapsible-heading__toggle"
          onClick={() => setIsCollapsed(!isCollapsed)}
        >
          <Icon
            iconName="chevron-down"
            size="sm"
            className={`collapsible-heading__icon ${
              !isCollapsed ? 'collapsible-heading__icon--open' : ''
            }`}
          />
        </a>
      </div>
      <div className={`collapsible-heading__content ${!isCollapsed ? 'shown' : ''}`}>
        {children}
      </div>
    </div>
  );
};

Object.assign(CollapsibleHeading, generateComponentProps(componentData));

export default CollapsibleHeading;
