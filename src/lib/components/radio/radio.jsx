import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import componentData from './radio.data.json';

const Radio = ({
  className,
  checked,
  color,
  disabled,
  fontColor,
  id,
  label,
  name,
  style,
  onChange
}) => {
  const [currentChecked, setCurrentChecked] = useState(checked || '');

  useEffect(() => {
    setCurrentChecked(checked);
  }, [checked]);

  const handleChange = () => {
    if (disabled) {
      return;
    }
    setCurrentChecked(!currentChecked);
    onChange(!currentChecked);
  };

  const classList = ['radio'];
  const componentStyle = { ...style };

  const labelClassList = ['radio__label'];

  const checkerClassList = ['radio__checker'];

  const tickClassList = [];

  // currentChecked
  tickClassList.push(
    currentChecked ? 'radio__checker-checked-tick radio__checker-checked-tick--shown' : null
  );

  // disabled
  labelClassList.push(disabled ? 'radio__label--disabled' : null);

  // color
  checkerClassList.push(currentChecked ? `border--${color}` : null);
  tickClassList.push(currentChecked ? `bg--${color}` : null);

  // fontColor
  classList.push(`text--${fontColor}`);

  // custom className
  classList.push(className);

  return (
    <div
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <div
        className={labelClassList.filter(c => c && !c.includes('null')).join(' ')}
        onClick={handleChange}
      >
        <span className="radio__wrapper" aria-disabled={disabled}>
          <input
            id={id || undefined}
            name={name || undefined}
            type="radio"
            tabIndex="0"
            disabled={disabled}
            defaultChecked={currentChecked}
          />
          <div className={checkerClassList.filter(c => c && !c.includes('null')).join(' ')}>
            <span className={tickClassList.filter(c => c && !c.includes('null')).join(' ')} />
          </div>
        </span>
        {label}
      </div>
    </div>
  );
};

Object.assign(Radio, generateComponentProps(componentData));

export default Radio;
