/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bemBlockElement, generateComponentProps, renderClassName } from '../../../util/component';
import getTypeComponent from '../util/getTypeComponent';
import componentData from './form-field.data.json';

const FormField = ({
  className,
  error,
  errorClassName,
  fieldClassName,
  id,
  label,
  labelClassName,
  name,
  fieldProps,
  showLabel,
  style,
  type,
  value,
  valueProp,
  onChange
}) => {
  const FieldComponent = getTypeComponent(type);

  const blockName = 'form-field';

  const classList = [blockName];
  const componentStyle = { ...style };

  const labelClassList = [bemBlockElement(blockName, 'label'), 'text-muted'];
  const controlClassList = [bemBlockElement(blockName, 'control')];
  const errorClassList = [
    bemBlockElement(blockName, 'error-msg'),
    'animation-opacity',
    'font-size-13'
  ];

  // labelClassName
  labelClassList.push(labelClassName);

  // fieldClassName
  controlClassList.push(fieldClassName);

  // errorClassName
  errorClassList.push(errorClassName);

  // error
  errorClassList.push(error ? 'animation-opacity--shown' : null);

  // custom className
  classList.push(className);

  return (
    <div className={renderClassName(classList)} style={componentStyle}>
      {showLabel ? (
        <label htmlFor={name} className={renderClassName(labelClassList)}>
          {label}
        </label>
      ) : null}
      <div className={renderClassName(controlClassList)}>
        <FieldComponent
          id={id || undefined}
          name={name || undefined}
          {...fieldProps}
          {...{ [valueProp]: value }}
          onChange={onChange}
          error={!!error}
        />
      </div>
      <div className={renderClassName(errorClassList)}>{error || 'error msg'}</div>
    </div>
  );
};

Object.assign(FormField, generateComponentProps(componentData));

export default FormField;
