const getFieldValidation = validation => {
  return validation
    ? {
        required: getRequired(validation),
        maxLength: getMaxLength(validation),
        minLength: getMinLength(validation),
        max: getMax(validation),
        min: getMin(validation),
        pattern: getPattern(validation),
        validate: validation.validate
      }
    : {};
};

const getRequired = validation => {
  if (!validation.required) {
    return undefined;
  }
  return typeof validation.required === 'string' ? validation.required : 'This field is mandatory';
};

const getMaxLength = validation => {
  if (!validation.maxLength) {
    return undefined;
  }
  return {
    value: validation.maxLength.value,
    message: validation.maxLength.message || `Max length exceeded (${validation.maxLength.value})`
  };
};

const getMinLength = validation => {
  if (!validation.minLength) {
    return undefined;
  }
  return {
    value: validation.minLength.value,
    message: validation.minLength.message || `Min length exceeded (${validation.minLength.value})`
  };
};

const getMax = validation => {
  if (!validation.max) {
    return undefined;
  }
  return {
    value: validation.max.value,
    message: validation.max.message || `Max value exceeded (${validation.max.value})`
  };
};

const getMin = validation => {
  if (!validation.min) {
    return undefined;
  }
  return {
    value: validation.min.value,
    message: validation.min.message || `Min value exceeded (${validation.min.value})`
  };
};

const getPattern = validation => {
  if (!validation.pattern) {
    return undefined;
  }
  return {
    value: validation.pattern.value,
    message:
      validation.pattern.message || `Regex pattern doesn't match (${validation.pattern.value})`
  };
};

export default getFieldValidation;
