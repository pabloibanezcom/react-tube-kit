import { camel2title } from '../../../util/string';

const getFieldsFromValue = value => {
  return Object.keys(value).map(key => getFieldFromProperty(key, value[key]));
};

const getFieldFromProperty = (propName, propValue) => {
  return {
    name: propName,
    label: camel2title(propName),
    type: getTypeFromPropValue(propValue)
  };
};

const getTypeFromPropValue = propValue => {
  switch (typeof propValue) {
    case 'string':
      return 'input';
    case 'number':
      return 'input';
    case 'boolean':
      return 'toggle';
    default:
      return 'input';
  }
};

export default getFieldsFromValue;
