import CheckboxGroup from '../../checkbox-group/checkbox-group';
import ColorSelector from '../../color-selector/color-selector';
import Input from '../../input/input';
import RadioGroup from '../../radio-group/radio-group';
import Selector from '../../selector/selector';
import Slider from '../../slider/slider';
import Toggle from '../../toggle/toggle';

const getTypeComponent = type => {
  switch (type) {
    case 'checkbox':
      return CheckboxGroup;
    case 'colorSelector':
      return ColorSelector;
    case 'input':
      return Input;
    case 'radio':
      return RadioGroup;
    case 'selector':
      return Selector;
    case 'slider':
      return Slider;
    case 'toggle':
      return Toggle;
    default:
      return Input;
  }
};

export default getTypeComponent;
