/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useRef, useState } from 'react';
import useForm from 'react-hook-form';
import { generateComponentProps, renderClassName } from '../../util/component';
import ButtonLink from '../button-link/button-link';
import FormField from './form-field/form-field';
import componentData from './form.data.json';
import getFieldsFromValue from './util/getFieldsFromValue';
import getFieldValidation from './util/getFieldValidation';

const Form = ({
  autoSubmit,
  className,
  // eslint-disable-next-line no-unused-vars
  direction,
  errorClassName,
  externalErrors,
  fields,
  formFieldClassName,
  id,
  labelClassName,
  name,
  showLabel,
  style,
  submitText,
  submitBtnProps,
  value,
  onCancel,
  onValueChange,
  onSubmit
}) => {
  const [formFields, setFormFields] = useState(fields);
  const { errors, getValues, register, handleSubmit, setValue, setError } = useForm();
  const autoSubmitRef = useRef();

  useEffect(() => {
    let newFields;
    if (!fields.length) {
      newFields = getFieldsFromValue(value);
      setFormFields(newFields);
    }
    (formFields.length ? formFields : newFields).forEach(field => {
      register({ name: field.name }, getFieldValidation(field.validation));
      if (value && value[field.name]) {
        setValue(field.name, value[field.name]);
      }
    });
  }, [value]);

  useEffect(() => {
    if (externalErrors.length) {
      setError(externalErrors);
    }
  }, [externalErrors]);

  const handleFieldChange = (field, selectedValue) => {
    const currentValue = getValues()[field.name];
    setValue(
      field.name,
      field.fieldProps && field.fieldProps.valueProp
        ? selectedValue[field.fieldProps.valueProp]
        : selectedValue
    );
    setError(field.name, null);
    if (autoSubmit || !submitText) {
      if (!field.minStr || getValues()[field.name].length >= field.minStr) {
        autoSubmitRef.current.click();
      } else if (
        selectedValue.length < field.minStr &&
        currentValue &&
        currentValue.length >= field.minStr
      ) {
        setValue(field.name, '');
        autoSubmitRef.current.click();
      }
    }

    onValueChange(getValues());
  };

  const blockName = 'form';

  const classList = [blockName];
  const componentStyle = { ...style };

  const fieldsClassList = [];
  const fieldClassList = [formFieldClassName];

  // direction
  fieldsClassList.push('d-flex', direction === 'horizontal' ? 'flex-row' : 'flex-column');
  fieldClassList.push(direction === 'horizontal' ? 'mr-6' : null);
  const btnBlock = !(direction === 'horizontal');

  // custom className
  classList.push(className);

  return (
    <form
      id={id || undefined}
      name={name || undefined}
      className={renderClassName(classList)}
      style={componentStyle}
      onSubmit={handleSubmit(onSubmit)}
    >
      <div className={renderClassName(fieldsClassList)}>
        {formFields.map(field => {
          return (
            <FormField
              key={field.name}
              label={field.label}
              valueProp={field.valueProp || 'value'}
              value={value && value[field.name]}
              name={field.name}
              fieldProps={field.fieldProps}
              type={field.type}
              error={errors[field.name] && errors[field.name].message}
              className={renderClassName([...fieldClassList, field.className])}
              errorClassName={errorClassName}
              labelClassName={labelClassName}
              showLabel={field.showLabel !== null ? field.showLabel : showLabel}
              onChange={val => handleFieldChange(field, val)}
            />
          );
        })}
      </div>
      {!autoSubmit && submitText ? (
        <div className="row">
          <div className={onCancel ? 'col-6' : 'col-12'}>
            <ButtonLink submit block={btnBlock} color="secondary" inverse {...submitBtnProps}>
              {submitText}
            </ButtonLink>
          </div>

          {onCancel ? (
            <div className="col-6">
              <ButtonLink block={btnBlock} color="secondary" outline onClick={onCancel}>
                Cancel
              </ButtonLink>
            </div>
          ) : null}
        </div>
      ) : (
        <button ref={autoSubmitRef} type="submit" className="d-none">
          autoSubmit button
        </button>
      )}
    </form>
  );
};

Object.assign(Form, generateComponentProps(componentData));

export default Form;
