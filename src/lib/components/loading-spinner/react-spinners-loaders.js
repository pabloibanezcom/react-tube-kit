import {
  BarLoader,
  BeatLoader,
  BounceLoader,
  CircleLoader,
  ClimbingBoxLoader,
  ClipLoader,
  ClockLoader,
  DotLoader,
  FadeLoader,
  GridLoader,
  HashLoader,
  MoonLoader,
  PacmanLoader,
  PropagateLoader,
  PulseLoader,
  RingLoader,
  RiseLoader,
  RotateLoader,
  ScaleLoader,
  SkewLoader,
  SquareLoader,
  SyncLoader
} from 'react-spinners';

export const getShapeComponent = shape => {
  if (shape === 'bar') {
    return BarLoader;
  }
  if (shape === 'beat') {
    return BeatLoader;
  }
  if (shape === 'bounce') {
    return BounceLoader;
  }
  if (shape === 'circle') {
    return CircleLoader;
  }
  if (shape === 'climbingBox') {
    return ClimbingBoxLoader;
  }
  if (shape === 'clip') {
    return ClipLoader;
  }
  if (shape === 'clock') {
    return ClockLoader;
  }
  if (shape === 'dot') {
    return DotLoader;
  }
  if (shape === 'fade') {
    return FadeLoader;
  }
  if (shape === 'grid') {
    return GridLoader;
  }
  if (shape === 'hash') {
    return HashLoader;
  }
  if (shape === 'moon') {
    return MoonLoader;
  }
  if (shape === 'pacman') {
    return PacmanLoader;
  }
  if (shape === 'propagate') {
    return PropagateLoader;
  }
  if (shape === 'pulse') {
    return PulseLoader;
  }
  if (shape === 'ring') {
    return RingLoader;
  }
  if (shape === 'rise') {
    return RiseLoader;
  }
  if (shape === 'rotate') {
    return RotateLoader;
  }
  if (shape === 'scale') {
    return ScaleLoader;
  }
  if (shape === 'skew') {
    return SkewLoader;
  }
  if (shape === 'square') {
    return SquareLoader;
  }
  if (shape === 'sync') {
    return SyncLoader;
  }
  return null;
};
