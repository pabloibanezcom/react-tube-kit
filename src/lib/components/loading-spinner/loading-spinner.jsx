import React, { Fragment } from 'react';
// eslint-disable-next-line import/named
import { getHexColor } from '../../util/color';
import { generateComponentProps } from '../../util/component';
import componentData from './loading-spinner.data.json';
import { getShapeComponent } from './react-spinners-loaders';

const LoadingSpinner = ({
  background,
  backgroundOpacity,
  className,
  color,
  height,
  id,
  loading,
  name,
  noSpinner,
  radius,
  shape,
  size,
  style,
  width
}) => {
  const classList = ['loading-spinner'];
  const componentStyle = { ...style };

  const bgClassList = ['loading-spinner__background'];
  const bgStyle = {};

  // background
  if (background && background.startsWith('#')) {
    bgStyle.backgroundColor = background;
  } else {
    bgClassList.push(`bg--${background}`);
  }

  // background opacity
  bgStyle.opacity = backgroundOpacity;

  // custom className
  classList.push(className);

  const ShapeComponent = getShapeComponent(shape);

  return (
    <Fragment>
      {loading ? (
        <div
          id={id || undefined}
          name={name || undefined}
          className={classList.filter(c => c && !c.includes('null')).join(' ')}
          style={componentStyle}
        >
          <div
            className={bgClassList.filter(c => c && !c.includes('null')).join(' ')}
            style={bgStyle}
          />
          {!noSpinner && (
            <div className="loading-spinner__spinner">
              <ShapeComponent
                loading={loading}
                color={color.startsWith('#') ? color : getHexColor(color)}
                height={height || undefined}
                radius={radius || undefined}
                size={size || undefined}
                width={width || undefined}
              />
            </div>
          )}
        </div>
      ) : null}
    </Fragment>
  );
};

Object.assign(LoadingSpinner, generateComponentProps(componentData));

export default LoadingSpinner;
