import { shallow } from 'enzyme';
import React from 'react';
import { BarLoader, FadeLoader } from 'react-spinners';
import LoadingSpinner from './loading-spinner';

describe('<LoadingSpinner />', () => {
  let wrapper;

  it('renders <LoadingSpinner /> component', () => {
    wrapper = shallow(<LoadingSpinner loading />);
    expect(wrapper.find('.loading-spinner')).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  describe('loading', () => {
    it('hides entire component when loading is set to false', () => {
      wrapper = shallow(<LoadingSpinner loading={false} />);
      expect(wrapper.find('.loading-spinner')).toHaveLength(0);
    });
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<LoadingSpinner loading className="my-class" />);
      expect(wrapper.find('.loading-spinner.my-class')).toHaveLength(1);
    });
  });

  describe('background & backgroundOpacity', () => {
    it('applies background color when background is set as application as color string', () => {
      wrapper = shallow(<LoadingSpinner loading background="secondary" />);
      expect(wrapper.find('.loading-spinner__background.bg--secondary')).toHaveLength(1);
    });

    it('applies background color when background is set as application as color hex', () => {
      wrapper = shallow(<LoadingSpinner loading background="#1aaa55" />);
      expect(wrapper.find('.loading-spinner__background').get(0).props.style).toHaveProperty(
        'backgroundColor',
        '#1aaa55'
      );
    });

    it('applies background opacity when backgroundOpacity is set when defined', () => {
      wrapper = shallow(<LoadingSpinner loading backgroundOpacity="0.5" />);
      expect(wrapper.find('.loading-spinner__background').get(0).props.style).toHaveProperty(
        'opacity',
        '0.5'
      );
    });
  });

  describe('noSpinner', () => {
    it('hides loading spinner when noSpinner is true', () => {
      wrapper = shallow(<LoadingSpinner loading noSpinner />);
      expect(wrapper.find('.loading-spinner__background')).toHaveLength(1);
      expect(wrapper.find('.loading-spinner__spinner')).toHaveLength(0);
    });
  });

  describe('shape', () => {
    it('loads shape loader based on shape property', () => {
      wrapper = shallow(<LoadingSpinner loading />);
      expect(wrapper.find(BarLoader)).toHaveLength(1);

      wrapper = shallow(<LoadingSpinner loading shape="fade" />);
      expect(wrapper.find(FadeLoader)).toHaveLength(1);
    });
  });

  describe('height, radius, size & width', () => {
    it('loads shape loader with height prop when height is defined', () => {
      wrapper = shallow(<LoadingSpinner loading height="20" />);
      expect(wrapper.find(BarLoader).prop('height')).toEqual('20');
    });

    it('loads shape loader with radius prop when radius is defined', () => {
      wrapper = shallow(<LoadingSpinner loading radius="20" />);
      expect(wrapper.find(BarLoader).prop('radius')).toEqual('20');
    });

    it('loads shape loader with size prop when size is defined', () => {
      wrapper = shallow(<LoadingSpinner loading size="20" />);
      expect(wrapper.find(BarLoader).prop('size')).toEqual('20');
    });

    it('loads shape loader with width prop when width is defined', () => {
      wrapper = shallow(<LoadingSpinner loading width="20" />);
      expect(wrapper.find(BarLoader).prop('width')).toEqual('20');
    });
  });
});
