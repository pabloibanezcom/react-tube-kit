/* eslint-disable react/jsx-props-no-spreading */
import React, { Fragment, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import randomId from '../../util/randomId';
import Radio from '../radio/radio';
import componentData from './radio-group.data.json';

const RadioGroup = ({
  className,
  color,
  direction,
  fontColor,
  id,
  name,
  options,
  style,
  value,
  onChange
}) => {
  const [currentValue, setCurrentValue] = useState(value);

  const classList = ['radio-group'];
  const componentStyle = { ...style };

  // direction
  classList.push(
    'd-flex',
    direction === 'horizontal' ? 'flex-row justify-content-around' : 'flex-column'
  );

  // custom className
  classList.push(className);

  const handleOptionChanged = optionValue => {
    setCurrentValue(optionValue);
    onChange(optionValue);
  };

  const groupName = name || `checkbox-group-${randomId()}`;

  const optionRadio = option => (
    <Radio
      checked={currentValue === option.value}
      color={color}
      fontColor={fontColor}
      name={groupName}
      label={option.label}
      onChange={() => handleOptionChanged(option.value)}
      {...option.radio}
    />
  );

  return (
    <div
      id={id || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      role="button"
    >
      {options.map(opt => (
        <Fragment key={opt.value}>{optionRadio(opt)}</Fragment>
      ))}
    </div>
  );
};

Object.assign(RadioGroup, generateComponentProps(componentData));

export default RadioGroup;
