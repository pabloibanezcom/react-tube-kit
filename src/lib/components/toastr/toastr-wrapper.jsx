import React from 'react';

const ToastrWrapper = ({ children }) => {
  return (
    <div id="toastr-wrapper" className="toastr-wrapper">
      <div id="toastr-wrapper--inner" className="toastr-wrapper--inner">
        {children}
      </div>
    </div>
  );
};

export default ToastrWrapper;
