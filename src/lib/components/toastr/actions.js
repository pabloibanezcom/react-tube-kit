import React from 'react';
import ReactDOM from 'react-dom';
import Toastr from './toastr';
import ToastrWrapper from './toastr-wrapper';

const defaultOptions = {
  color: 'secondary',
  timeout: 3000
};

const fireToastr = (type, title, message, optionsParam) => {
  const options = optionsParam
    ? {
        color: optionsParam.color || defaultOptions.color,
        timeout: optionsParam.timeout || defaultOptions.timeout,
        icon: optionsParam.icon
      }
    : defaultOptions;

  ReactDOM.render(
    <ToastrWrapper>
      <Toastr
        type={type}
        color={options.color}
        title={title}
        icon={options.icon}
        message={message}
        demoMode={false}
        timeout={options.timeout}
      />
    </ToastrWrapper>,
    document.getElementById('toastr-container')
  );

  setTimeout(() => {
    ReactDOM.render(null, document.getElementById('toastr-container'));
  }, options.timeout + 500);
};

export const info = (title, message, optionsParam) => {
  fireToastr('info', title, message, optionsParam);
};

export const success = (title, message, optionsParam) => {
  fireToastr('success', title, message, optionsParam);
};

export const warning = (title, message, optionsParam) => {
  fireToastr('warning', title, message, optionsParam);
};

export const error = (title, message, optionsParam) => {
  fireToastr('error', title, message, optionsParam);
};
