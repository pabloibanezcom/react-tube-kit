import React, { useEffect, useState } from 'react';
import { generateComponentProps, renderClassName } from '../../util/component';
import Icon from '../icon/icon';
import componentData from './toastr.data.json';

const getIconName = type => {
  switch (type) {
    case 'success':
      return 'check';
    case 'info':
      return 'info';
    case 'warning':
      return 'warning';
    case 'error':
      return 'error';
    default:
      return '';
  }
};

const getColor = type => {
  switch (type) {
    case 'success':
      return 'success';
    case 'info':
      return null;
    case 'warning':
      return 'warning';
    case 'error':
      return 'danger';
    default:
      return null;
  }
};

const Toastr = ({
  className,
  color,
  demoMode,
  icon,
  id,
  message,
  name,
  style,
  timeout,
  title,
  type
}) => {
  const [shown, setShown] = useState(true);
  const classList = ['toastr', 'd-flex', 'flex-row'];
  const componentStyle = { ...style };

  useEffect(() => {
    setTimeout(() => {
      setShown(false);
    }, timeout);
  }, [timeout]);

  // shown
  if (!demoMode && shown) {
    classList.push('toastr--shown');
  } else if (!demoMode && !shown) {
    classList.push('toastr--hidden');
  }

  // type
  classList.push(`toastr--${type}`);

  // color
  classList.push(`bg--${color}`);

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={renderClassName(classList)}
      style={componentStyle}
    >
      <div className="toastr-icon">
        <Icon iconName={icon || getIconName(type)} size="lg" color={getColor(type)} />
      </div>
      <div>
        <div className="toastr-title">{title}</div>
        <div className="toastr-message">{message}</div>
      </div>
    </div>
  );
};

Object.assign(Toastr, generateComponentProps(componentData));

export default Toastr;
