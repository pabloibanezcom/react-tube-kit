export const getLeftPct = (min, max, value) => {
  return ((value - min) / (max - min)) * 100;
};

export const getWidthPct = (min, max, value) => {
  if (value[0]) {
    return ((value[1] - value[0]) / (max - min)) * 100;
  }
  return ((value - min) / (max - min)) * 100;
};

export const getValueFromLeftPct = (min, max, newX, end) => {
  return (getLeftPctFromCoordinates(newX, end) * (max - min)) / 100 + min;
};

export const getActiveStepValue = (value, steps, max) => {
  return [...steps, ...[max]].reduce((prev, curr) => {
    return Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev;
  });
};

export const getSteps = (min, max, step) => {
  return [...[min], ...[...Array((max - min) / step - 1).keys()].map(n => min + (n + 1) * step)];
};

export const getCloserHandler = (clickX, lowerLeft, mainLeft) => {
  if (clickX <= lowerLeft) {
    return 'lower';
  }
  if (clickX >= mainLeft) {
    return 'value';
  }
  if (clickX - lowerLeft <= mainLeft - clickX) {
    return 'lower';
  }
  return 'value';
};

const getLeftPctFromCoordinates = (newX, end) => {
  return (100 * newX) / end;
};
