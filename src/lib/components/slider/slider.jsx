/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { generateComponentProps, renderClassName } from '../../util/component';
import Tooltip from '../tooltip/tooltip';
import componentData from './slider.data.json';
import {
  getActiveStepValue,
  getCloserHandler,
  getLeftPct,
  getSteps,
  getValueFromLeftPct,
  getWidthPct
} from './util';

const Slider = ({
  barColor,
  className,
  color,
  defaultValue,
  fontColor,
  graduated,
  handleBorderColor,
  handleColor,
  id,
  max,
  min,
  name,
  progress,
  showMarks,
  range,
  renderMark,
  step,
  style,
  tooltip,
  onChange
}) => {
  const [currentValue, setCurrentValue] = useState(defaultValue || min);
  const [hover, setHover] = useState(false);
  const [handleActive, setHandleActive] = useState(null);
  const sliderRef = React.useRef();
  const handleRef = React.useRef();
  const handleLowerRef = React.useRef();
  const diff = React.useRef();

  const steps = graduated ? getSteps(min, max, step) : [];

  const classList = ['slider'];
  const componentStyle = { ...style };

  const containerClassList = ['slider__container'];
  const barClassList = ['slider__bar'];
  const barProgressClassList = ['slider__bar-progress'];
  const handleCircleClassList = ['slider__handle-circle'];
  const markContentClassList = ['slider__graduator-mark-content'];

  // Bar Color
  barClassList.push(`bg--${barColor}`);

  // Color
  barProgressClassList.push(`bg--${color}`);

  // Font colot
  markContentClassList.push(fontColor ? `text--${fontColor}` : null);

  // Handle color & border color
  handleCircleClassList.push(`bg--${handleColor}`);
  handleCircleClassList.push(`border--${handleBorderColor}`);

  // With marks
  barClassList.push(showMarks ? 'slider--with-marks' : null);

  // active
  handleCircleClassList.push(handleActive ? 'slider__handle-circle--active' : null);

  // custom className
  classList.push(className);

  const value = range ? currentValue[1] : currentValue;

  const getNewValue = (refinedValue, isRangeLower) => {
    if (!range) {
      return refinedValue;
    }
    return isRangeLower ? [refinedValue, currentValue[1]] : [currentValue[0], refinedValue];
  };

  // eslint-disable-next-line no-unused-vars
  const processMovement = (event, isRangeLower) => {
    let newX = event.clientX - (diff.current || 0) - sliderRef.current.getBoundingClientRect().left;
    const end =
      sliderRef.current.offsetWidth -
      (!isRangeLower ? handleRef.current.offsetWidth : handleLowerRef.current.offsetWidth);
    const start = 0;
    if (newX < start) {
      newX = 0;
    }
    if (newX > end) {
      newX = end;
    }

    let newValue = getValueFromLeftPct(min, max, newX, end);
    if (graduated) {
      newValue = getActiveStepValue(newValue, steps, max);
    } else {
      newValue = Math.round(newValue);
    }

    newValue = getNewValue(newValue, isRangeLower);

    if (newValue !== currentValue) {
      onChange(newValue);
    }

    setCurrentValue(newValue);
  };

  const handleMouseMove = event => {
    processMovement(event);
  };

  const handleMouseMoveLower = event => {
    processMovement(event, true);
  };

  const handleBarClick = evt => {
    if (!handleActive) {
      processMovement(
        evt,
        getCloserHandler(
          evt.clientX,
          handleLowerRef && handleLowerRef.current
            ? handleLowerRef.current.getBoundingClientRect().left
            : null,
          handleRef.current.getBoundingClientRect().left
        ) === 'lower'
      );
    }
  };

  const handleMouseUp = () => {
    setHandleActive(null);
    document.removeEventListener('mouseup', handleMouseUp);
    document.removeEventListener('mousemove', handleMouseMove);
    document.removeEventListener('mousemove', handleMouseMoveLower);
  };

  const handleMouseDown = (event, isRangeLower) => {
    setHandleActive(isRangeLower ? 'lower' : 'value');
    diff.current =
      event.clientX -
      (!isRangeLower
        ? handleRef.current.getBoundingClientRect().left
        : handleLowerRef.current.getBoundingClientRect().left);
    document.addEventListener('mousemove', !isRangeLower ? handleMouseMove : handleMouseMoveLower);
    document.addEventListener('mouseup', handleMouseUp);
  };

  const handleMouseEnter = isRangeLower => {
    setHover(isRangeLower ? 'lower' : 'value');
  };

  const handleMouseLeave = () => {
    setHover(null);
  };

  const generateStepPoint = (mark, isLast) => (
    <div
      className={`slider__graduator-step-point ${
        isLast ? 'slider__graduator-step-point--last' : ''
      }`}
    >
      <div
        className={`slider__graduator-step-point-circle border--${
          (!isLast ? mark : max) > value ? barColor : color
        } bg--${handleColor}`}
      />
    </div>
  );

  const sliderGraduator = (
    <div className="slider__graduator">
      <ul>
        {steps.map((_step, i, arr) => (
          <li key={_step} className={`slider__graduator-step ${_step}`}>
            {generateStepPoint(_step)}
            {arr.length - 1 === i ? generateStepPoint(_step, true) : null}
            {showMarks ? (
              <>
                <span className="slider__graduator-mark">
                  <span className={renderClassName(markContentClassList)}>
                    {renderMark ? renderMark(_step) : _step}
                  </span>
                </span>
                {arr.length - 1 === i ? (
                  <span className="slider__graduator-mark slider__graduator-mark--last">
                    <span className={renderClassName(markContentClassList)}>
                      {renderMark ? renderMark(max) : max}
                    </span>
                  </span>
                ) : null}
              </>
            ) : null}
          </li>
        ))}
      </ul>
    </div>
  );

  const renderHandle = isRangeLower => (
    <div
      className="slider__handle"
      style={{
        left: `${
          isRangeLower ? getLeftPct(min, max, currentValue[0]) : getLeftPct(min, max, value)
        }%`
      }}
      ref={!isRangeLower ? handleRef : handleLowerRef}
      role="presentation"
      onMouseDown={evt => handleMouseDown(evt, isRangeLower)}
      onMouseEnter={() => handleMouseEnter(isRangeLower)}
      onMouseLeave={handleMouseLeave}
    >
      <div className={renderClassName(handleCircleClassList)} />
      <div
        className={`slider__tooltip ${
          hover === (isRangeLower ? 'lower' : 'value') ||
          handleActive === (isRangeLower ? 'lower' : 'value')
            ? 'slider__tooltip--shown'
            : ''
        }`}
      >
        <Tooltip position="top" {...tooltip} text={isRangeLower ? currentValue[0] : value} />
      </div>
    </div>
  );

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={renderClassName(classList)}
      style={componentStyle}
      role="presentation"
      ref={sliderRef}
    >
      <div className={renderClassName(containerClassList)}>
        <div className={renderClassName(barClassList)} onClick={handleBarClick}>
          {progress ? (
            <div
              className={renderClassName(barProgressClassList)}
              style={{
                left: `${range ? getLeftPct(min, max, currentValue[0]) : 0}%`,
                width: `${getWidthPct(min, max, currentValue)}%`
              }}
            />
          ) : null}
          {graduated ? sliderGraduator : null}
        </div>
        {renderHandle()}
        {range ? renderHandle(true) : null}
      </div>
    </div>
  );
};

Object.assign(Slider, generateComponentProps(componentData));

export default Slider;
