/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { ChromePicker } from 'react-color';
import { generateComponentProps } from '../../util/component';
import Input from '../input/input';
import componentData from './color-selector.data.json';

const ColorSelector = ({ className, color, icon, id, inputProps, name, style, onChange }) => {
  const [showColorMenu, setShowColorMenu] = useState(false);
  const [selectedColor, setSelectedColor] = useState(color);

  const handleColorChange = newSelectedColor => {
    setSelectedColor(newSelectedColor);
    onChange(newSelectedColor);
  };

  const classList = ['d-flex align-items-center position-relative color-selector'];
  const componentStyle = { ...style };

  // custom className
  classList.push(className);

  return (
    <div
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      {showColorMenu ? (
        <div className="color-selector__overlay" onClick={() => setShowColorMenu(false)} />
      ) : null}
      <Input
        id={id || undefined}
        name={name || undefined}
        icon={icon}
        iconColor={selectedColor}
        iconOpacity={selectedColor ? 1 : 0.1}
        className="w-100"
        value={selectedColor}
        onClick={() => setShowColorMenu(true)}
        onChange={handleColorChange}
        {...inputProps}
      />
      {showColorMenu ? (
        <ChromePicker
          className={`${showColorMenu ? '' : 'hidden'}`}
          disableAlpha
          color={selectedColor || { hex: '#ffffff' }}
          onChangeComplete={val => handleColorChange(val.hex)}
        />
      ) : null}
    </div>
  );
};

Object.assign(ColorSelector, generateComponentProps(componentData));

export default ColorSelector;
