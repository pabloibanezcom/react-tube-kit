import { shallow } from 'enzyme';
import React from 'react';
import Input from '../input/input';
import ColorSelector from './color-selector';

describe('<ColorSelector />', () => {
  let wrapper;

  it('renders <ColorSelector /> component', () => {
    wrapper = shallow(<ColorSelector />);
    expect(wrapper.find(Input)).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<ColorSelector className="my-class" />);
      expect(wrapper.find('.color-selector.my-class')).toHaveLength(1);
    });
  });

  describe('color', () => {
    it('shows color string and icon when color is set', () => {
      wrapper = shallow(<ColorSelector color="#2b4c65" />);
      expect(wrapper.find(Input).prop('value')).toEqual('#2b4c65');
      expect(wrapper.find(Input).prop('icon')).toBeDefined();
      expect(wrapper.find(Input).prop('iconColor')).toEqual('#2b4c65');
    });
  });

  describe('icon', () => {
    it('set ion name if icon is defined', () => {
      wrapper = shallow(<ColorSelector color="#2b4c65" icon="circle" />);
      expect(wrapper.find(Input).prop('icon')).toEqual('circle');
    });
  });

  describe('inputProps', () => {
    it('spreads inputProps into Inut', () => {
      wrapper = shallow(
        <ColorSelector inputProps={{ placeholder: 'Mock placeholder', readOnly: true }} />
      );
      expect(wrapper.find(Input).prop('placeholder')).toEqual('Mock placeholder');
      expect(wrapper.find(Input).prop('readOnly')).toEqual(true);
    });
  });
});
