import { shallow } from 'enzyme';
import React from 'react';
import imgSample from '../../../test-resources/sample_img.jpg';
import Image from '../image/image';
import Card from './card';

describe('<Card />', () => {
  let wrapper;

  it('renders <Card /> component', () => {
    wrapper = shallow(<Card>Fake card text</Card>);
    expect(wrapper.find('.card__content').text()).toBe('Fake card text');
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Card className="my-class">Fake card text</Card>);
      expect(wrapper.find('.card.my-class')).toHaveLength(1);
    });
  });

  describe('color & fontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<Card>Fake card text</Card>);
      expect(wrapper.find('.bg--primary')).toHaveLength(1);

      wrapper = shallow(<Card color="secondary">Fake card text</Card>);
      expect(wrapper.find('.bg--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(
        <Card color="#1aaa55" fontColor="#222222">
          Fake card text
        </Card>
      );

      // expect(wrapper.find('.card').prop('backgroundColor')).toEqual('#1aaa55');
      // expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('image', () => {
    it('renders an Image with properties when defined', () => {
      wrapper = shallow(<Card image={{ src: imgSample, loading: 'lazy' }}>Fake card text</Card>);
      expect(wrapper.find(Image)).toHaveLength(1);
      expect(wrapper.find(Image).prop('src')).toEqual(imgSample);
      expect(wrapper.find(Image).prop('loading')).toEqual('lazy');
    });
  });

  describe('onImageClick', () => {
    it('invokes onClick when image is clicked', () => {
      const clickCallback = jest.fn();
      wrapper = shallow(
        <Card image={{ src: imgSample }} onImageClick={clickCallback}>
          Fake card text
        </Card>
      );
      expect(wrapper.find(Image).prop('onClick')).toEqual(clickCallback);
    });
  });

  describe('imagePosition', () => {
    it('renders image right position according to imagePosition', () => {
      wrapper = shallow(<Card image={{ src: imgSample }}>Fake card text</Card>);
      expect(wrapper.find('.card.flex-column')).toHaveLength(1);
      wrapper = shallow(
        <Card image={{ src: imgSample }} imagePosition="bottom">
          Fake card text
        </Card>
      );
      expect(wrapper.find('.card.flex-column-reverse')).toHaveLength(1);
      wrapper = shallow(
        <Card image={{ src: imgSample }} imagePosition="left">
          Fake card text
        </Card>
      );
      expect(wrapper.find('.card.flex-row')).toHaveLength(1);
      wrapper = shallow(
        <Card image={{ src: imgSample }} imagePosition="right">
          Fake card text
        </Card>
      );
      expect(wrapper.find('.card.flex-row-reverse')).toHaveLength(1);
    });
  });
});
