/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Link } from 'react-router-dom';
import { generateComponentProps, renderClassName } from '../../util/component';
import Image from '../image/image';
import componentData from './card.data.json';

const Card = ({
  children,
  className,
  color,
  fontColor,
  hoverColor,
  id,
  image,
  imagePosition,
  name,
  separatorColor,
  style,
  onImageClick,
  to
}) => {
  const classList = ['card', 'd-flex', 'rounded'];
  const componentStyle = { ...style };

  const imageClassList = ['rounded-top'];

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else {
    classList.push(`bg--${color}`);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    componentStyle.color = fontColor;
  } else if (fontColor) {
    classList.push(`text--${fontColor}`);
  }

  // to & hoverColor
  if (to) {
    classList.push(`bg-hover--${hoverColor}`);
  }

  // separatorColor
  if (separatorColor) {
    imageClassList.push(`border-bottom-shadow--${separatorColor}`);
  }

  // imagePosition
  if (imagePosition === 'top') {
    classList.push('flex-column');
  } else if (imagePosition === 'bottom') {
    classList.push('flex-column-reverse');
  } else if (imagePosition === 'left') {
    classList.push('flex-row');
  } else if (imagePosition === 'right') {
    classList.push('flex-row-reverse');
  }

  // custom className
  classList.push(className);

  // onImageClick
  const getOnImageClick = () => {
    if (onImageClick) {
      return onImageClick;
    }
    if (to) {
      return () => {};
    }
    return null;
  };

  const card = (
    <div
      id={id || undefined}
      name={name || undefined}
      className={renderClassName(classList)}
      style={componentStyle}
    >
      {image ? (
        <Image
          {...image}
          onClick={getOnImageClick()}
          className={renderClassName(imageClassList)}
          imgClassName="rounded-top"
        />
      ) : null}
      <div className="card__content">{children}</div>
    </div>
  );

  return <>{to ? <Link to={to}>{card}</Link> : card}</>;
};

Object.assign(Card, generateComponentProps(componentData));

export default Card;
