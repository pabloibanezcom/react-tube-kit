import React, { createRef, useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import getDisplayClass from '../../util/getDisplayClass';
import Icon from '../icon/icon';
import Panel from '../panel/panel';
import componentData from './tab-menu.data.json';

const TabMenu = ({
  activeTab,
  className,
  color,
  content,
  fontColor,
  id,
  name,
  panelColor,
  panelFontColor,
  selectedTabColor,
  tabs,
  showIcon,
  showTabText,
  style,
  onTabChange
}) => {
  const [currentTab, setCurrentTab] = useState(
    Number.isInteger(activeTab) ? activeTab : tabs.find(t => t.id === activeTab)
  );
  const [menuElementWidth, setMenuElementWidth] = useState(0);

  const classList = ['tab-menu'];
  const componentStyle = { ...style };

  const selectedClassList = ['tab-menu__header-indicator'];
  const selectedStyle = { left: currentTab * menuElementWidth, width: menuElementWidth };

  const menuHeader = createRef();

  useEffect(() => {
    const buildWidth = () => {
      if (menuHeader.current) {
        setMenuElementWidth(menuHeader.current.clientWidth / tabs.length);
      }
    };

    buildWidth();
    window.addEventListener('resize', buildWidth);
  }, [menuHeader, tabs.length]);

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else if (color) {
    classList.push(`bg--${color}`);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    componentStyle.color = fontColor;
  } else if (fontColor) {
    classList.push(`text--${fontColor}`);
  }

  // selectedTabColor
  if (selectedTabColor && selectedTabColor.startsWith('#')) {
    selectedStyle.backgroundColor = selectedTabColor;
  } else if (selectedTabColor) {
    selectedClassList.push(`bg--${selectedTabColor}`);
  } else {
    selectedClassList.push(`bg-alt--${color}`);
  }

  // custom className
  classList.push(className);

  const renderContent = () => {
    const MockContent = content || tabs[currentTab].content;
    return <MockContent />;
  };

  const changeTab = tab => {
    setCurrentTab(tab);
    onTabChange(tab);
  };

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <ul className="tab-menu__header" ref={menuHeader}>
        {tabs.map((tab, i) => {
          return (
            <li key={i} className={`flex-fill ${i === currentTab ? 'active' : ''}`}>
              <a
                className="d-flex justify-content-center align-items-center py-2 px-4"
                onClick={() => changeTab(i)}
              >
                {tab.icon ? (
                  <Icon
                    iconName={tab.icon}
                    size="sm"
                    className={`${getDisplayClass(showIcon)} mr-2`}
                  />
                ) : null}
                <span className={`${getDisplayClass(showTabText)}`}>{tab.name}</span>
              </a>
            </li>
          );
        })}
        <span
          className={selectedClassList.filter(c => c && !c.includes('null')).join(' ')}
          style={selectedStyle}
        />
      </ul>
      <Panel color={panelColor} fontColor={panelFontColor}>
        {renderContent()}
      </Panel>
    </div>
  );
};

Object.assign(TabMenu, generateComponentProps(componentData));

export default TabMenu;
