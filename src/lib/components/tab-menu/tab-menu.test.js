import { shallow } from 'enzyme';
import React from 'react';
import { mockTabs } from '../../../test-resources/mockTabs';
import Panel from '../panel/panel';
import TabMenu from './tab-menu';

describe('<TabMenu />', () => {
  let wrapper;

  it('renders <TabMenu /> component', () => {
    wrapper = shallow(<TabMenu tabs={mockTabs} />);
    expect(wrapper.find('.tab-menu__header li').length).toBe(4);
    expect(
      wrapper
        .find('.tab-menu__header li')
        .first()
        .text()
    ).toBe('Element A');
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<TabMenu className="my-class" tabs={mockTabs} />);
      expect(wrapper.find('.tab-menu.my-class')).toHaveLength(1);
    });
  });

  describe('color & fontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} />);
      expect(wrapper.find('.tab-menu.bg--primary')).toHaveLength(1);

      wrapper = shallow(<TabMenu tabs={mockTabs} color="secondary" />);
      expect(wrapper.find('.tab-menu.bg--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} color="#1aaa55" fontColor="#222222" />);
      expect(wrapper.find('.tab-menu').get(0).props.style).toHaveProperty(
        'backgroundColor',
        '#1aaa55'
      );
      expect(wrapper.find('.tab-menu').get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('panelColor & panelFontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} />);
      expect(wrapper.find(Panel).prop('color')).toBe('white');
      expect(wrapper.find(Panel).prop('fontColor')).toBe(null);

      wrapper = shallow(
        <TabMenu tabs={mockTabs} panelColor="secondary" panelFontColor="primary" />
      );
      expect(wrapper.find(Panel).prop('color')).toBe('secondary');
      expect(wrapper.find(Panel).prop('fontColor')).toBe('primary');
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} panelColor="#1aaa55" panelFontColor="#222222" />);
      expect(wrapper.find(Panel).prop('color')).toBe('#1aaa55');
      expect(wrapper.find(Panel).prop('fontColor')).toBe('#222222');
    });
  });

  describe('selectedTabColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} />);
      expect(wrapper.find('.tab-menu__header-indicator.bg-alt--primary')).toHaveLength(1);

      wrapper = shallow(<TabMenu tabs={mockTabs} selectedTabColor="secondary" />);
      expect(wrapper.find('.tab-menu__header-indicator.bg--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} selectedTabColor="#1aaa55" />);
      expect(wrapper.find('.tab-menu__header-indicator').get(0).props.style).toHaveProperty(
        'backgroundColor',
        '#1aaa55'
      );
    });
  });

  describe('activeTab', () => {
    it('set first tab as active when "activeTab" prop is not defined', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} />);
      expect(
        wrapper
          .find('.tab-menu__header li.active')
          .first()
          .text()
      ).toBe('Element A');
    });

    it('set active tab with index from "activeTab" prop when defined', () => {
      wrapper = shallow(<TabMenu tabs={mockTabs} activeTab={2} />);
      expect(
        wrapper
          .find('.tab-menu__header li.active')
          .first()
          .text()
      ).toBe('Element C');
    });
  });

  describe('onTabChange', () => {
    it('invokes onTabChange when tab is changed', () => {
      const tabChange = jest.fn();
      wrapper = shallow(<TabMenu tabs={mockTabs} onTabChange={tabChange} />);
      wrapper
        .find('.tab-menu__header a')
        .get(2)
        .props.onClick();
      expect(tabChange).toHaveBeenCalledWith(2);
    });
  });
});
