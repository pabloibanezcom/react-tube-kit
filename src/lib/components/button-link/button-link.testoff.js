// import { mount, shallow } from 'enzyme';
// import React from 'react';
// import Icon from '../icon/icon';
// import ButtonLink from './button-link';

// describe('<ButtonLink />', () => {
//   let wrapper;

//   it('renders <ButtonLink /> component', () => {
//     wrapper = shallow(<ButtonLink>Fake button</ButtonLink>);
//     expect(wrapper.text()).toBe('Fake button');
//     expect(wrapper).toMatchSnapshot();
//   });

//   describe('ariaExpanded', () => {
//     it('defines "aria-expanded" attr as true when ariaExpanded is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['aria-expanded']).toBe(undefined);

//       wrapper = shallow(<ButtonLink ariaExpanded>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['aria-expanded']).toBe(true);
//     });
//   });

//   describe('ariaHaspopup', () => {
//     it('defines "aria-haspopup" attr as true when ariaHaspopup is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['aria-haspopup']).toBe(undefined);

//       wrapper = shallow(<ButtonLink ariaHaspopup>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['aria-haspopup']).toBe(true);
//     });
//   });

//   describe('badge', () => {
//     it('creates a Badge element when "badge" is set', () => {
//       wrapper = mount(<ButtonLink badge>Fake button text</ButtonLink>);
//       expect(wrapper.find('a')).toHaveLength(1);
//       expect(wrapper.find('.badge')).toHaveLength(1);
//     });
//   });

//   describe('custom class', () => {
//     it('applies custom class when className is set', () => {
//       wrapper = shallow(<ButtonLink className="my-class">Fake button text</ButtonLink>);
//       expect(wrapper.find('.my-class')).toHaveLength(1);
//     });
//   });

//   describe('circle', () => {
//     it('applies "btn--circle" and size classes when circle is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--circle')).toHaveLength(0);

//       wrapper = shallow(<ButtonLink circle>Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--circle')).toHaveLength(1);
//       expect(wrapper.find('.btn--circle-md')).toHaveLength(1);

//       wrapper = shallow(
//         <ButtonLink circle size="sm">
//           Fake button text
//         </ButtonLink>
//       );
//       expect(wrapper.find('.btn--circle')).toHaveLength(1);
//       expect(wrapper.find('.btn--circle-md')).toHaveLength(0);
//       expect(wrapper.find('.btn--circle-sm')).toHaveLength(1);
//     });
//   });

//   describe('color & fontColor', () => {
//     it('applies color class when color is set as application as color string', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.bg--primary.bg-hover-darken--primary')).toHaveLength(1);

//       wrapper = shallow(<ButtonLink color="secondary">Fake button text</ButtonLink>);
//       expect(wrapper.find('.bg--secondary.bg-hover-darken--secondary')).toHaveLength(1);
//     });

//     it('applies color style when color is set as application as color hex', () => {
//       wrapper = shallow(
//         <ButtonLink color="#1aaa55" fontColor="#222222">
//           Fake button text
//         </ButtonLink>
//       );
//       expect(wrapper.get(0).props.style).toHaveProperty('backgroundColor', '#1aaa55');
//       expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
//     });
//   });

//   describe('dataToggle', () => {
//     it('defines "data-toggle" attr when dataToggle is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['data-toggle']).toBe(undefined);

//       wrapper = shallow(<ButtonLink dataToggle="otherElement">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props['data-toggle']).toBe('otherElement');
//     });
//   });

//   describe('disabled', () => {
//     it('defines "disabled" attr as true when disabled is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.disabled).toBe(undefined);

//       wrapper = shallow(<ButtonLink disabled>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.disabled).toBe(true);
//     });
//   });

//   describe('block', () => {
//     it('applies "btn--block" class when block is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--block')).toHaveLength(0);

//       wrapper = shallow(<ButtonLink block>Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--block')).toHaveLength(1);
//     });
//   });

//   describe('href', () => {
//     it('defines "href" attr when disabled is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.href).toBe(undefined);

//       wrapper = shallow(<ButtonLink href="www.fakesite.com">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.href).toBe('www.fakesite.com');
//     });
//   });

//   describe('icon', () => {
//     it('creates a Icon element in content when "icon" is set', () => {
//       wrapper = shallow(<ButtonLink icon="close">Fake button text</ButtonLink>);
//       expect(wrapper.find(Icon).prop('iconName')).toEqual('close');
//     });
//   });

//   describe('newPage', () => {
//     it('defines "target" attr as "_blank" when newPage is set', () => {
//       wrapper = shallow(<ButtonLink href="www.fakesite.com">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.target).toBe(undefined);

//       wrapper = shallow(
//         <ButtonLink href="www.fakesite.com" newPage>
//           Fake button text
//         </ButtonLink>
//       );
//       expect(wrapper.get(0).props.target).toBe('_blank');
//     });
//   });

//   describe('pulse', () => {
//     it('applies "w-100" class when fullWidth is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.pulse')).toHaveLength(0);

//       wrapper = shallow(<ButtonLink pulse>Fake button text</ButtonLink>);
//       expect(wrapper.find('.pulse')).toHaveLength(1);
//     });
//   });

//   describe('outline', () => {
//     it('applies "border" class when outline is set', () => {
//       wrapper = shallow(<ButtonLink outline>Fake button text</ButtonLink>);
//       expect(wrapper.find('.border--primary')).toHaveLength(1);
//       expect(wrapper.find('.text--primary')).toHaveLength(1);
//       expect(wrapper.find('.bg-hover--primary')).toHaveLength(1);
//     });
//   });

//   describe('size', () => {
//     it('applies right size class when size is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--md')).toHaveLength(1);

//       wrapper = shallow(<ButtonLink size="sm">Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--sm')).toHaveLength(1);
//       expect(wrapper.find('.btn--md')).toHaveLength(0);

//       wrapper = shallow(<ButtonLink size="lg">Fake button text</ButtonLink>);
//       expect(wrapper.find('.btn--lg')).toHaveLength(1);
//       expect(wrapper.find('.btn--md')).toHaveLength(0);
//     });
//   });

//   describe('submit', () => {
//     it('set button type to "submit" when submit is set', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.type).toBe('button');

//       wrapper = shallow(<ButtonLink submit>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).props.type).toBe('submit');
//     });
//   });

//   describe('textAlignment', () => {
//     it('applies textAlignment class', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.text-center')).toHaveLength(1);
//       expect(wrapper.find('.text-left')).toHaveLength(0);

//       wrapper = shallow(<ButtonLink type="link">Fake link text</ButtonLink>);
//       expect(wrapper.find('.text-center')).toHaveLength(0);
//       expect(wrapper.find('.text-left')).toHaveLength(1);

//       wrapper = shallow(<ButtonLink textAlignment="left">Fake button text</ButtonLink>);
//       expect(wrapper.find('.text-center')).toHaveLength(0);
//       expect(wrapper.find('.text-left')).toHaveLength(1);
//     });
//   });

//   describe('to', () => {
//     it('creates a Link element when "to" is set', () => {
//       wrapper = shallow(<ButtonLink to="/somepath">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).type.displayName).toBe('Link');
//     });
//   });

//   describe('type', () => {
//     it('creates a button element if type is not defined or type is "btn"', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.get(0).type).toBe('button');

//       wrapper = shallow(<ButtonLink type="btn">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).type).toBe('button');
//     });

//     it('creates a "a" element if type is is "link"', () => {
//       wrapper = shallow(<ButtonLink type="link">Fake button text</ButtonLink>);
//       expect(wrapper.get(0).type).toBe('a');
//     });
//   });

//   describe('uppercase', () => {
//     it('applies "text-uppercase" class by default', () => {
//       wrapper = shallow(<ButtonLink>Fake button text</ButtonLink>);
//       expect(wrapper.find('.text-uppercase')).toHaveLength(1);

//       wrapper = shallow(<ButtonLink uppercase={false}>Fake button text</ButtonLink>);
//       expect(wrapper.find('.text-uppercase')).toHaveLength(0);
//     });
//   });

//   describe('onClick', () => {
//     it('invokes onClick when button or link is clicked', () => {
//       const clickCallback = jest.fn();
//       wrapper = shallow(<ButtonLink onClick={clickCallback}>Fake button text</ButtonLink>);
//       wrapper.get(0).props.onClick();
//       expect(clickCallback).toHaveBeenCalled();
//     });
//   });
// });
