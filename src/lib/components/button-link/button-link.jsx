/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/button-has-type */
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
import { generateComponentProps } from '../../util/component';
import useOutsideClick from '../../util/useOutsideClick';
import { absolutePosition } from '../../util/viewport';
import Badge from '../badge/badge';
import Icon from '../icon/icon';
import { removeTooltip, renderTooltip } from '../tooltip/util';
import componentData from './button-link.data.json';

const ButtonLink = ({
  ariaExpanded,
  ariaHaspopup,
  badge,
  block,
  children,
  className,
  circle,
  color,
  dataToggle,
  disabled,
  fontColor,
  hoverColor,
  href,
  icon,
  iconColor,
  iconSize,
  id,
  name,
  newPage,
  pulse,
  outline,
  size,
  submit,
  style,
  textAlignment,
  to,
  tooltip,
  tooltipEvent,
  type,
  uppercase,
  onClick
}) => {
  const ref = useRef();

  const classList = [!badge ? type : 'd-inline-flex'];
  const componentStyle = { ...style };

  const _textAlginment = textAlignment || (type === 'btn' ? 'center' : 'left');

  // Tooltip events

  const handleMouseEnter = () => {
    renderTooltip(tooltip, absolutePosition(ref.current));
  };

  const handleMouseLeave = () => {
    removeTooltip();
  };

  const handleClick = () => {
    onClick();
    if (tooltipEvent === 'click') {
      renderTooltip(tooltip, absolutePosition(ref.current));
    }
  };

  useOutsideClick(
    ref,
    () => {
      if (tooltipEvent === 'click') {
        removeTooltip();
      }
    },
    tooltipEvent !== 'click'
  );

  //

  if (!badge) {
    // block
    classList.push(block ? 'btn--block' : null);

    // uppercase
    classList.push(uppercase || (uppercase === null && type === 'btn') ? 'text-uppercase' : null);

    // pulse
    classList.push(pulse ? 'pulse' : null);

    // circle
    classList.push(circle ? `btn--circle ${`btn--circle-${size}`} ` : null);

    // size
    if (!circle) {
      classList.push(type === 'btn' ? `btn--${size}` : `link--${size}`);
    }

    // textAlignment
    classList.push(`text-${_textAlginment}`);

    // outline
    classList.push(outline ? 'btn--outline' : null);

    // disabled
    classList.push(disabled ? 'btn--disabled' : null);

    // color
    if (color && color.startsWith('#')) {
      if (!outline) {
        componentStyle.backgroundColor = color;
      } else {
        componentStyle.borderColor = color;
        componentStyle.color = color;
      }
    } else if (!outline) {
      classList.push(type === 'btn' ? `bg--${color}` : `text--${color}`);
    } else {
      classList.push(`border--${color}`);
      classList.push(`text--${color}`);
      classList.push(`bg-hover--${color}`);
    }

    // hoverColor
    if (hoverColor) {
      classList.push(type === 'btn' ? `bg-hover--${hoverColor}` : `text-hover--${hoverColor}`);
    } else if (!outline) {
      classList.push(type === 'btn' ? `bg-hover-darken--${color}` : `text-hover-alt--${color}`);
    }

    // fontColor
    if (!outline) {
      if (fontColor && fontColor.startsWith('#')) {
        componentStyle.color = fontColor;
      }
    }
  }

  // custom className
  classList.push(className);

  const elProps = {
    id: id || undefined,
    name: name || undefined,
    className: classList.filter(c => c && !c.includes('null')).join(' '),
    'aria-expanded': ariaExpanded || undefined,
    'aria-haspopup': ariaHaspopup || undefined,
    'data-toggle': dataToggle || undefined,
    disabled: disabled || undefined,
    href: href || undefined,
    to,
    target: newPage ? '_blank' : undefined,
    style: componentStyle,
    onClick: handleClick,
    onMouseEnter: tooltip && tooltipEvent === 'hover' ? handleMouseEnter : null,
    onMouseLeave: tooltip && tooltipEvent === 'hover' ? handleMouseLeave : null
  };

  const getContent = () => {
    if (badge) {
      return (
        <Badge {...badge} color={color} fontColor={fontColor} fullWidth={block} size={size}>
          {children}
        </Badge>
      );
    }
    if (circle) {
      return (
        <div className={`d-flex justify-content-${_textAlginment} align-items-center`}>
          <Icon
            iconName={icon}
            color={iconColor}
            size={iconSize}
            className={`${!children ? 'm-0' : ''} ${type === 'link' ? 'mr-2' : ''}`}
          />
          {children}
        </div>
      );
    }
    if (icon) {
      return (
        <>
          <Icon
            iconName={icon}
            color={iconColor}
            size={iconSize}
            className={`${!children ? 'm-0' : ''} ${type === 'link' ? 'mr-2' : ''}`}
          />
          {children}
        </>
      );
    }
    return children;
  };

  if (to) {
    return <Link {...elProps}>{getContent()}</Link>;
  }
  if (type === 'link' || badge) {
    return <a {...elProps}>{getContent()}</a>;
  }
  if (type === 'btn') {
    return (
      <button ref={ref} type={submit ? 'submit' : 'button'} {...elProps}>
        {getContent()}
      </button>
    );
  }

  return null;
};

Object.assign(ButtonLink, generateComponentProps(componentData));

export default ButtonLink;
