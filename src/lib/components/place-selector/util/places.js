import iconTypes from './iconTypes.json';
import zoomTypes from './zoomTypes.json';

export const initPlacesService = () => {
  return new window.google.maps.places.PlacesService(document.createElement('div'));
};

export const searchPlace = async (searchServicestr, options) => {
  return new Promise((resolve, reject) => {
    new window.google.maps.places.PlacesService(document.createElement('div')).textSearch(
      {
        query: searchServicestr,
        location:
          options && options.location
            ? new window.google.maps.LatLng(options.location.lat, options.location.lng)
            : undefined,
        radius: options && options.radius ? options.radius : undefined,
        type: options && options.type ? options.type : undefined
      },
      (predictions, status) => {
        if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
          reject(status);
        }
        resolve(predictions);
      }
    );
  });
};

export const getPlaceById = async placeId => {
  return new Promise((resolve, reject) => {
    new window.google.maps.places.PlacesService(document.createElement('div')).getDetails(
      {
        placeId
      },
      (place, status) => {
        if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
          reject(status);
        }
        resolve(place);
      }
    );
  });
};

export const getSuggestionIcon = suggestion => {
  let result = 'marker';
  suggestion.types.forEach(t => {
    if (iconTypes[t]) {
      result = iconTypes[t];
    }
  });
  return result;
};

export const getPlaceZoom = place => {
  let zoom = 14;
  place.types.forEach(t => {
    if (zoomTypes[t]) {
      zoom = zoomTypes[t];
    }
  });
  return zoom;
};
