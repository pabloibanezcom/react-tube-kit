import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import ButtonLink from '../button-link/button-link';
import Icon from '../icon/icon';
import Map from '../map/map';
import Modal from '../modal/modal';
import Selector from '../selector/selector';
import componentData from './place-selector.data.json';
// eslint-disable-next-line import/named
import {
  getPlaceById,
  getPlaceZoom,
  getSuggestionIcon,
  initPlacesService,
  searchPlace
} from './util/places';

const PlaceSelector = ({
  className,
  enableChangePlace,
  enableModal,
  iconColor,
  id,
  inputClearable,
  mapBorderColor,
  location,
  name,
  radius,
  style,
  type,
  usePlaceIcon
}) => {
  // eslint-disable-next-line no-unused-vars
  const [suggestions, setSuggestions] = useState([]);
  const [selectedPlace, setSelectedPlace] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const classList = ['place-selector'];
  const componentStyle = { ...style };

  useEffect(() => {
    const refreshIntervalId = setInterval(() => {
      if (window.google && window.google.maps) {
        clearInterval(refreshIntervalId);
        initPlacesService();
      }
    }, 500);
  }, []);

  const updateSuggestions = _suggestions => {
    setSuggestions(
      _suggestions.map(sg => {
        return { ...sg, iconName: getSuggestionIcon(sg) };
      })
    );
  };

  const handleSearchPlace = async str => {
    if (str.length > 2) {
      const newSuggestions = await searchPlace(str, { location, radius, type });
      updateSuggestions(newSuggestions);
    }
  };

  const handlePlaceSelected = place => {
    setSelectedPlace(place);
    updateSuggestions([place]);
  };

  const handlePlaceChanged = async evt => {
    if (evt && evt.placeId) {
      const place = await getPlaceById(evt.placeId);
      setSelectedPlace(place);
      updateSuggestions([]);
    }
  };

  // custom className
  classList.push(className);

  const getPlaceMap = () => (
    <Map
      center={selectedPlace.geometry.location}
      zoom={getPlaceZoom(selectedPlace)}
      markers={[
        {
          position: selectedPlace.geometry.location,
          markerName: usePlaceIcon ? selectedPlace.iconName : undefined
        }
      ]}
      onClick={enableChangePlace ? handlePlaceChanged : null}
    />
  );

  const modalPlaceMap = () => (
    <div className="place-selector__modal-map-container">{getPlaceMap()}</div>
  );

  const PlaceSuggestion = ({ suggestion }) => (
    <div className="d-flex align-items-center">
      <div className="mr-2" style={{ width: 20 }}>
        {suggestion.iconName ? <Icon iconName={suggestion.iconName} color={iconColor} /> : null}
      </div>
      {suggestion.name}
    </div>
  );

  return (
    <div
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      id={id || undefined}
      name={name || undefined}
    >
      <Selector
        search
        disableInternalSearch
        native={null}
        options={suggestions}
        custom={PlaceSuggestion}
        inputClearable={inputClearable}
        valueProp="id"
        value={selectedPlace ? selectedPlace.id : null}
        customProp="suggestion"
        onChange={handlePlaceSelected}
        onSearchChange={handleSearchPlace}
      />
      {selectedPlace ? (
        <div className={`place-selector__map-container border-${mapBorderColor}`}>
          {getPlaceMap()}
          {enableModal ? (
            <>
              <ButtonLink
                outline
                circle
                icon="screen-full"
                color="secondary"
                size="sm"
                className="place-selector__modal-btn"
                onClick={() => setShowModal(true)}
              />
              <Modal
                fullScreen
                content={modalPlaceMap}
                isOpen={showModal}
                shouldCloseOnOverlayClick
                onClose={() => setShowModal(false)}
              />
            </>
          ) : null}
        </div>
      ) : null}
    </div>
  );
};

Object.assign(PlaceSelector, generateComponentProps(componentData));

export default PlaceSelector;
