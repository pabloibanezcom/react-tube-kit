import React from 'react';
import { generateComponentProps } from '../../util/component';
import componentData from './icon.data.json';
import getIconReactComponent from './util/getIconReactComponent';

const Icon = ({ color, className, iconName, id, name, opacity, prefix, size, style }) => {
  const classList = ['icon'];
  const componentStyle = { ...style };

  const IconSVG = getIconReactComponent(iconName);

  // iconName
  classList.push(`${prefix}-${iconName}`);

  // opacity
  componentStyle.opacity = opacity || undefined;

  // size
  classList.push(`icon--${size}`);

  // color
  if (color && color.startsWith('#')) {
    componentStyle.color = color;
  } else if (color) {
    classList.push(`text--${color}`);
  }

  // custom className
  classList.push(className);

  return (
    <>
      {IconSVG ? (
        <IconSVG
          id={id || undefined}
          name={name || undefined}
          style={componentStyle}
          className={classList.filter(c => c && !c.includes('null')).join(' ')}
        />
      ) : null}
    </>
  );
};

Object.assign(Icon, generateComponentProps(componentData));

export default Icon;
