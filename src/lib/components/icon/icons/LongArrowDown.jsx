import * as React from 'react';

export const LongArrowDown = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={12} height={28} viewBox="0 0 12 28" {...props}>
    <path d="M11.953 20.297a.534.534 0 01-.078.547l-5.469 6a.508.508 0 01-.359.156.549.549 0 01-.375-.156l-5.547-6a.534.534 0 01-.078-.547A.5.5 0 01.5 20H4V.5c0-.281.219-.5.5-.5h3c.281 0 .5.219.5.5V20h3.5c.203 0 .375.109.453.297z" />
  </svg>
);

export default LongArrowDown;
