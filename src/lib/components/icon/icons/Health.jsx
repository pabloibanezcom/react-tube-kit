import * as React from 'react';

export const Health = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M18 14.016V9.985h-3.984V6.001H9.985v3.984H6.001v4.031h3.984V18h4.031v-3.984H18zM18.984 3C20.062 3 21 3.938 21 5.016v13.969c0 1.078-.938 2.016-2.016 2.016H5.015c-1.078 0-2.016-.938-2.016-2.016V5.016C2.999 3.938 3.937 3 5.015 3h13.969z" />
  </svg>
);

export default Health;
