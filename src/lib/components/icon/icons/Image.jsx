import * as React from 'react';

export const Image = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={30} height={28} viewBox="0 0 30 28" {...props}>
    <path d="M10 9a3.001 3.001 0 01-6 0 3.001 3.001 0 016 0zm16 6v7H4v-3l5-5 2.5 2.5 8-8zm1.5-11h-25c-.266 0-.5.234-.5.5v19c0 .266.234.5.5.5h25c.266 0 .5-.234.5-.5v-19c0-.266-.234-.5-.5-.5zm2.5.5v19c0 1.375-1.125 2.5-2.5 2.5h-25A2.507 2.507 0 010 23.5v-19C0 3.125 1.125 2 2.5 2h25C28.875 2 30 3.125 30 4.5z" />
  </svg>
);

export default Image;
