import * as React from 'react';

export const Upload = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M14.016 12.984h3L12 8.015l-5.016 4.969h3v4.031h4.031v-4.031zm5.343-2.953C21.937 10.219 24 12.375 24 15a5.021 5.021 0 01-5.016 5.016H6c-3.328 0-6-2.672-6-6 0-3.094 2.344-5.625 5.344-5.953C6.61 5.672 9.094 3.985 12 3.985c3.656 0 6.656 2.578 7.359 6.047z" />
  </svg>
);

export default Upload;
