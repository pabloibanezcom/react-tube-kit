import * as React from 'react';

export const Square = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={28} viewBox="0 0 24 28" {...props}>
    <path d="M24 6.5v15c0 2.484-2.016 4.5-4.5 4.5h-15A4.502 4.502 0 010 21.5v-15C0 4.016 2.016 2 4.5 2h15C21.984 2 24 4.016 24 6.5z" />
  </svg>
);

export default Square;
