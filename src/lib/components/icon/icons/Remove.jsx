import * as React from 'react';

export const Remove = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M18.984 12.984H5.015v-1.969h13.969v1.969z" />
  </svg>
);

export default Remove;
