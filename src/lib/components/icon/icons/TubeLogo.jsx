import * as React from 'react';

export const TubeLogo = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={32} height={32} viewBox="0 0 32 32" {...props}>
    <path d="M16 6c-4.832 0-8.859 3.443-9.792 8H4.666c-.667 0-1.333.667-1.333 1.333v1.333c0 .667.667 1.333 1.333 1.333h1.542c.932 4.557 4.959 8 9.792 8s8.859-3.443 9.792-8h1.542c.667 0 1.333-.667 1.333-1.333v-1.333c0-.667-.667-1.333-1.333-1.333h-1.542C24.86 9.443 20.833 6 16 6zm0 3.333c2.742 0 5.158 2.21 6 4.667H10c.842-2.456 3.258-4.667 6-4.667zM10 18h12c-.842 2.456-3.258 4.667-6 4.667s-5.158-2.21-6-4.667z" />
  </svg>
);

export default TubeLogo;
