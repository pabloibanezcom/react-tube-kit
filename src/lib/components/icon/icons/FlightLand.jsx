import * as React from 'react';

export const FlightLand = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M14.016 14.438A804.972 804.972 0 004.36 11.86l-1.594-.469V6.235l1.453.375.938 2.344 4.969 1.313v-8.25l1.922.516 2.766 9 5.297 1.406c.797.234 1.266 1.078 1.078 1.875-.234.797-1.031 1.219-1.828 1.031zM2.484 18.984h19.031V21H2.484v-2.016z" />
  </svg>
);

export default FlightLand;
