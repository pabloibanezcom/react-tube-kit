import * as React from 'react';

export const Publish = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={32} height={32} viewBox="0 0 32 32" {...props}>
    <path d="M6.667 5.333V8h18.667V5.333H6.667zm0 13.334H12v8h8v-8h5.333L16 9.334l-9.333 9.333z" />
  </svg>
);

export default Publish;
