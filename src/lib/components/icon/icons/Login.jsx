import * as React from 'react';

export const Login = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={32} height={32} viewBox="0 0 32 32" {...props}>
    <path d="M27 3v26a1 1 0 01-1 1H6a1 1 0 01-1-1v-2h2v1h18V4H7v3H5V3a1 1 0 011-1h20a1 1 0 011 1zM12.29 20.29l1.42 1.42 5-5c.183-.181.296-.432.296-.71s-.113-.529-.296-.71l-5-5-1.42 1.42 3.3 3.29H5v2h10.59z" />
  </svg>
);

export default Login;
