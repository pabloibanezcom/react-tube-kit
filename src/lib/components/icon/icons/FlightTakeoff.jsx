import * as React from 'react';

export const FlightTakeoff = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M22.078 9.656A1.536 1.536 0 0121 11.484c-3.224.854-6.445 1.711-9.656 2.578l-5.297 1.406-1.594.469c-.879-1.496-1.751-2.999-2.625-4.5l1.453-.375 1.969 1.5 4.969-1.313-4.125-7.172 1.922-.516 6.891 6.422 5.344-1.406c.797-.234 1.641.281 1.828 1.078zM2.484 18.984h19.031V21H2.484v-2.016z" />
  </svg>
);

export default FlightTakeoff;
