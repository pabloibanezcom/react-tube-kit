import * as React from 'react';

export const Marker = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M12 11.484c1.359 0 2.484-1.125 2.484-2.484S13.359 6.516 12 6.516 9.516 7.641 9.516 9s1.125 2.484 2.484 2.484zm0-9.468A6.942 6.942 0 0118.984 9c0 5.25-6.984 12.984-6.984 12.984S5.016 14.25 5.016 9A6.942 6.942 0 0112 2.016z" />
  </svg>
);

export default Marker;
