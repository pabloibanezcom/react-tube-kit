import * as React from 'react';

export const Facebook = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={32} height={32} viewBox="0 0 32 32" {...props}>
    <path d="M22.82 12.27h-4.315V9.44c0-1.063.704-1.311 1.201-1.311h3.045V3.457l-4.194-.016c-4.656 0-5.715 3.485-5.715 5.715v3.115H10.15v4.815h2.692V30.71h5.663V17.086h3.821l.494-4.815z" />
  </svg>
);

export default Facebook;
