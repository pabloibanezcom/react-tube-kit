import * as React from 'react';

export const Connection = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={32} height={32} viewBox="0 0 32 32" {...props}>
    <path d="M2.105 11.896v1.958h22.129l.001 1.554 5.66-2.533-5.66-2.533-.001 1.554zM29.895 20.104v-1.958H7.766l-.001-1.554-5.66 2.533 5.66 2.533.001-1.554z" />
  </svg>
);

export default Connection;
