import * as React from 'react';

export const LongArrowUp = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={12} height={28} viewBox="0 0 12 28" {...props}>
    <path d="M11.953 7.703A.5.5 0 0111.5 8H8v19.5c0 .281-.219.5-.5.5h-3a.494.494 0 01-.5-.5V8H.5c-.203 0-.375-.109-.453-.297S0 7.312.125 7.156l5.469-6A.508.508 0 015.953 1c.141 0 .281.063.375.156l5.547 6a.534.534 0 01.078.547z" />
  </svg>
);

export default LongArrowUp;
