import * as React from 'react';

export const ScreenFull = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={28} height={32} viewBox="0 0 28 32" {...props}>
    <path d="M4 24h19.996V8H4v16zm3.998-12h12v8h-12v-8zM2 6.002h5.998v-2H0V12h2V6.002zM2 20H0v7.998h7.998V26H2v-6zM19.998 4.002v2h5.998V12h2V4.002h-7.998zM25.996 26h-5.998v1.998h7.998V20h-2v6z" />
  </svg>
);

export default ScreenFull;
