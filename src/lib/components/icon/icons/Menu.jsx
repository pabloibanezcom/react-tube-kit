import * as React from 'react';

export const Menu = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" {...props}>
    <path d="M2.016 5.484h19.969V7.5H2.016V5.484zm0 5.016h19.969v2.016H2.016V10.5zm0 5.016h19.969v1.969H2.016v-1.969z" />
  </svg>
);

export default Menu;
