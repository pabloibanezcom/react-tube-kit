import * as React from 'react';

export const FileO = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={28} viewBox="0 0 24 28" {...props}>
    <path d="M22.937 5.938c.578.578 1.062 1.734 1.062 2.562v18a1.5 1.5 0 01-1.5 1.5h-21a1.5 1.5 0 01-1.5-1.5v-25a1.5 1.5 0 011.5-1.5h14c.828 0 1.984.484 2.562 1.062zM16 2.125V8h5.875c-.094-.266-.234-.531-.344-.641L16.64 2.468c-.109-.109-.375-.25-.641-.344zM22 26V10h-6.5A1.5 1.5 0 0114 8.5V2H2v24h20z" />
  </svg>
);

export default FileO;
