import * as React from 'react';

export const SquareO = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width={22} height={28} viewBox="0 0 22 28" {...props}>
    <path d="M17.5 4h-13A2.507 2.507 0 002 6.5v13C2 20.875 3.125 22 4.5 22h13c1.375 0 2.5-1.125 2.5-2.5v-13C20 5.125 18.875 4 17.5 4zM22 6.5v13c0 2.484-2.016 4.5-4.5 4.5h-13A4.502 4.502 0 010 19.5v-13C0 4.016 2.016 2 4.5 2h13C19.984 2 22 4.016 22 6.5z" />
  </svg>
);

export default SquareO;
