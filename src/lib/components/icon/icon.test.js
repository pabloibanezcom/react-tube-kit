import { shallow } from 'enzyme';
import React from 'react';
import Icon from './icon';

describe('<Icon />', () => {
  let wrapper;

  it('renders <Icon /> component', () => {
    wrapper = shallow(<Icon iconName="user" />);
    expect(wrapper.find('.icon-user')).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  describe('color', () => {
    it('applies color class when color is set as color string', () => {
      wrapper = shallow(<Icon iconName="user" color="primary" />);
      expect(wrapper.find('.text--primary')).toHaveLength(1);
    });
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Icon iconName="user" className="my-class" />);
      expect(wrapper.find('.my-class')).toHaveLength(1);
    });
  });

  describe('prefix', () => {
    it('applies right prefix class when prefix is set', () => {
      wrapper = shallow(<Icon iconName="user" />);
      expect(wrapper.find('.icon-user')).toHaveLength(1);

      wrapper = shallow(<Icon prefix="customprefix" iconName="user" />);
      expect(wrapper.find('.icon-user')).toHaveLength(0);
      expect(wrapper.find('.customprefix-user')).toHaveLength(1);
    });
  });

  describe('size', () => {
    it('applies right size class when size is set', () => {
      wrapper = shallow(<Icon iconName="user" />);
      expect(wrapper.find('.icon--md')).toHaveLength(1);

      wrapper = shallow(<Icon iconName="user" size="sm" />);
      expect(wrapper.find('.icon--sm')).toHaveLength(1);
      expect(wrapper.find('.icon--md')).toHaveLength(0);

      wrapper = shallow(<Icon iconName="user" size="lg" />);
      expect(wrapper.find('.icon--lg')).toHaveLength(1);
      expect(wrapper.find('.icon--md')).toHaveLength(0);
    });
  });
});
