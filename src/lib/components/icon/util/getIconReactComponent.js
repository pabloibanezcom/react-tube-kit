import Add from '../icons/Add';
import AngleDown from '../icons/AngleDown';
import AngleLeft from '../icons/AngleLeft';
import AngleRight from '../icons/AngleRight';
import AngleUp from '../icons/AngleUp';
import At from '../icons/At';
import Atm from '../icons/Atm';
import Boat from '../icons/Boat';
import Bus from '../icons/Bus';
import Calendar from '../icons/Calendar';
import Cancel from '../icons/Cancel';
import Car from '../icons/Car';
import Center from '../icons/Center';
import CenterO from '../icons/CenterO';
import Check from '../icons/Check';
import ChevronDown from '../icons/ChevronDown';
import ChevronLeft from '../icons/ChevronLeft';
import ChevronRight from '../icons/ChevronRight';
import ChevronUp from '../icons/ChevronUp';
import Circle from '../icons/Circle';
import CircleO from '../icons/CircleO';
import City from '../icons/City';
import Close from '../icons/Close';
import Code from '../icons/Code';
import Connection from '../icons/Connection';
import Copy from '../icons/Copy';
import Download from '../icons/Download';
import Edit from '../icons/Edit';
import Error from '../icons/Error';
import Facebook from '../icons/Facebook';
import Female from '../icons/Female';
import FileArchiveO from '../icons/FileArchiveO';
import FileAudioO from '../icons/FileAudioO';
import FileExcelO from '../icons/FileExcelO';
import FileImageO from '../icons/FileImageO';
import FileMovieO from '../icons/FileMovieO';
import FileO from '../icons/FileO';
import FilePdfO from '../icons/FilePdfO';
import FilePowerpointO from '../icons/FilePowerpointO';
import FileTextO from '../icons/FileTextO';
import FileWordO from '../icons/FileWordO';
import Flight from '../icons/Flight';
import FlightLand from '../icons/FlightLand';
import FlightTakeoff from '../icons/FlightTakeoff';
import GasStation from '../icons/GasStation';
import Health from '../icons/Health';
import Hospital from '../icons/Hospital';
import Image from '../icons/Image';
import Info from '../icons/Info';
import Key from '../icons/Key';
import Lines from '../icons/Lines';
import Login from '../icons/Login';
import Logout from '../icons/Logout';
import LongArrowDown from '../icons/LongArrowDown';
import LongArrowLeft from '../icons/LongArrowLeft';
import LongArrowRight from '../icons/LongArrowRight';
import LongArrowUp from '../icons/LongArrowUp';
import Map from '../icons/Map';
import Marker from '../icons/Marker';
import Menu from '../icons/Menu';
import Nature from '../icons/Nature';
import Neighborhood from '../icons/Neighborhood';
import Pharmacy from '../icons/Pharmacy';
import Play from '../icons/Play';
import Public from '../icons/Public';
import Publish from '../icons/Publish';
import Railway from '../icons/Railway';
import Remove from '../icons/Remove';
import Restaurant from '../icons/Restaurant';
import School from '../icons/School';
import ScreenFull from '../icons/ScreenFull';
import Search from '../icons/Search';
import Settings from '../icons/Settings';
import Share from '../icons/Share';
import ShoppingBasket from '../icons/ShoppingBasket';
import ShoppingCart from '../icons/ShoppingCart';
import Square from '../icons/Square';
import SquareO from '../icons/SquareO';
import StoreMall from '../icons/StoreMall';
import Train from '../icons/Train';
import Tram from '../icons/Tram';
import TrashO from '../icons/TrashO';
import TubeLogo from '../icons/TubeLogo';
import Underground from '../icons/Underground';
import Upload from '../icons/Upload';
import User from '../icons/User';
import Warning from '../icons/Warning';
import ZoomIn from '../icons/ZoomIn';
import ZoomOut from '../icons/ZoomOut';

const getIconReactComponent = iconName => {
  switch (iconName) {
    case 'add':
      return Add;
    case 'angle-down':
      return AngleDown;
    case 'angle-left':
      return AngleLeft;
    case 'angle-right':
      return AngleRight;
    case 'angle-up':
      return AngleUp;
    case 'at':
      return At;
    case 'atm':
      return Atm;
    case 'boat':
      return Boat;
    case 'bus':
      return Bus;
    case 'calendar':
      return Calendar;
    case 'cancel':
      return Cancel;
    case 'car':
      return Car;
    case 'center-o':
      return CenterO;
    case 'center':
      return Center;
    case 'check':
      return Check;
    case 'chevron-down':
      return ChevronDown;
    case 'chevron-left':
      return ChevronLeft;
    case 'chevron-right':
      return ChevronRight;
    case 'chevron-up':
      return ChevronUp;
    case 'circle-o':
      return CircleO;
    case 'circle':
      return Circle;
    case 'city':
      return City;
    case 'close':
      return Close;
    case 'code':
      return Code;
    case 'connection':
      return Connection;
    case 'copy':
      return Copy;
    case 'download':
      return Download;
    case 'edit':
      return Edit;
    case 'error':
      return Error;
    case 'facebook':
      return Facebook;
    case 'female':
      return Female;
    case 'file-archive-o':
      return FileArchiveO;
    case 'file-audio-o':
      return FileAudioO;
    case 'file-excel-o':
      return FileExcelO;
    case 'file-image-o':
      return FileImageO;
    case 'file-movie-o':
      return FileMovieO;
    case 'file-o':
      return FileO;
    case 'file-pdf-o':
      return FilePdfO;
    case 'file-powerpoint-o':
      return FilePowerpointO;
    case 'file-text-o':
      return FileTextO;
    case 'file-word-o':
      return FileWordO;
    case 'flight-land':
      return FlightLand;
    case 'flight-takeoff':
      return FlightTakeoff;
    case 'flight':
      return Flight;
    case 'gas-station':
      return GasStation;
    case 'health':
      return Health;
    case 'hospital':
      return Hospital;
    case 'image':
      return Image;
    case 'info':
      return Info;
    case 'key':
      return Key;
    case 'lines':
      return Lines;
    case 'login':
      return Login;
    case 'logout':
      return Logout;
    case 'long-arrow-down':
      return LongArrowDown;
    case 'long-arrow-left':
      return LongArrowLeft;
    case 'long-arrow-right':
      return LongArrowRight;
    case 'long-arrow-up':
      return LongArrowUp;
    case 'map':
      return Map;
    case 'marker':
      return Marker;
    case 'menu':
      return Menu;
    case 'nature':
      return Nature;
    case 'neighborhood':
      return Neighborhood;
    case 'pharmacy':
      return Pharmacy;
    case 'play':
      return Play;
    case 'public':
      return Public;
    case 'publish':
      return Publish;
    case 'railway':
      return Railway;
    case 'remove':
      return Remove;
    case 'restaurant':
      return Restaurant;
    case 'school':
      return School;
    case 'screen-full':
      return ScreenFull;
    case 'search':
      return Search;
    case 'settings':
      return Settings;
    case 'share':
      return Share;
    case 'shopping-basket':
      return ShoppingBasket;
    case 'shopping-cart':
      return ShoppingCart;
    case 'square-o':
      return SquareO;
    case 'square':
      return Square;
    case 'store-mall':
      return StoreMall;
    case 'train':
      return Train;
    case 'tram':
      return Tram;
    case 'trash-o':
      return TrashO;
    case 'tube-logo':
      return TubeLogo;
    case 'underground':
      return Underground;
    case 'upload':
      return Upload;
    case 'user':
      return User;
    case 'warning':
      return Warning;
    case 'zoom-in':
      return ZoomIn;
    case 'zoom-out':
      return ZoomOut;

    default:
      return null;
  }
};

export default getIconReactComponent;
