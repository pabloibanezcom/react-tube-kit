/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { generateComponentProps, renderClassName } from '../../util/component';
import componentData from './image.data.json';

const Image = ({
  actionText,
  alt,
  className,
  id,
  imgClassName,
  loading,
  name,
  showHoverAnimation,
  showOverlay,
  src,
  style,
  onClick
}) => {
  const classList = ['image-wrapper', 'w-100', 'position-relative'];
  const componentStyle = { ...style };

  const imgClassList = ['w-100'];

  // imgClassName
  imgClassList.push(imgClassName);

  // showHoverAnimation
  classList.push(showHoverAnimation ? 'hover-zoom-img' : null);

  // custom className
  classList.push(className);

  const compProps = {
    id,
    name,
    className: classList.filter(c => c && !c.includes('null')).join(' '),
    style: componentStyle
  };

  const imageEl = (
    <img className={renderClassName(imgClassList)} src={src} alt={alt} loading={loading} />
  );

  const buttonElement = onClick ? (
    <button
      type="button"
      className={`btn--no-button-style position-absolute w-100 h-100 ${
        showOverlay ? 'overlay overlay--hover' : 'bg-transparent'
      } `}
      onClick={onClick}
    >
      {actionText}
    </button>
  ) : null;

  return (
    <div {...compProps}>
      {buttonElement}
      {imageEl}
    </div>
  );
};

Object.assign(Image, generateComponentProps(componentData));

export default Image;
