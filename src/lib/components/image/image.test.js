import { shallow } from 'enzyme';
import React from 'react';
import imgSample from '../../../test-resources/sample_img.jpg';
import Image from './image';

describe('<Image />', () => {
  let wrapper;

  it('renders <Image /> component', () => {
    wrapper = shallow(<Image src={imgSample} />);
    expect(wrapper).toMatchSnapshot();
  });

  describe('src', () => {
    it('set img src properly', () => {
      wrapper = shallow(<Image src={imgSample} />);
      expect(wrapper.find('img').prop('src')).toEqual(imgSample);
    });
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Image src={imgSample} className="my-class" />);
      expect(wrapper.find('.image-wrapper.my-class')).toHaveLength(1);
    });
  });

  describe('alt', () => {
    it('set img alt properly', () => {
      wrapper = shallow(<Image src={imgSample} alt="My image" />);
      expect(wrapper.find('img').prop('alt')).toBe('My image');
    });
  });

  describe('loading', () => {
    it('set img loading properly', () => {
      wrapper = shallow(<Image src={imgSample} loading="lazy" />);
      expect(wrapper.find('img').prop('loading')).toBe('lazy');
    });
  });

  describe('showHoverAnimation', () => {
    it('set hover-zoom-img class to image-wrapper', () => {
      wrapper = shallow(<Image src={imgSample} showHoverAnimation />);
      expect(wrapper.find('.image-wrapper.hover-zoom-img')).toHaveLength(1);
    });
  });

  describe('showOverlay', () => {
    it('set overlay class to button element', () => {
      wrapper = shallow(<Image src={imgSample} onClick={() => {}} showOverlay />);
      expect(wrapper.find('button.overlay')).toHaveLength(1);
    });
  });

  describe('actionText', () => {
    it('set button text', () => {
      wrapper = shallow(<Image src={imgSample} onClick={() => {}} actionText="Some action" />);
      expect(wrapper.find('button').text()).toBe('Some action');
    });
  });

  describe('onClick', () => {
    it('invokes onClick when button or link is clicked', () => {
      const clickCallback = jest.fn();
      wrapper = shallow(<Image src={imgSample} onClick={clickCallback} />);
      wrapper
        .find('button')
        .get(0)
        .props.onClick();
      expect(clickCallback).toHaveBeenCalled();
    });
  });
});
