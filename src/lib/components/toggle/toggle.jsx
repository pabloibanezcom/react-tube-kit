import React, { useEffect, useState } from 'react';
import { getAltHexColor } from '../../util/color';
import { generateComponentProps } from '../../util/component';
import Icon from '../icon/icon';
import componentData from './toggle.data.json';

const Toggle = ({
  className,
  color,
  disabled,
  fontColor,
  iconChecked,
  iconUnchecked,
  id,
  name,
  size,
  style,
  textChecked,
  textUnchecked,
  uncheckedColor,
  value,
  onChange
}) => {
  const [currentValue, setCurrentValue] = useState(value || '');
  const classList = ['toggle'];
  const componentStyle = { ...style };

  const innerClassList = ['toggle__inner'];
  const innerStyle = {};

  const circleClassList = ['toggle__circle'];
  const circleStyle = {};

  useEffect(() => {
    setCurrentValue(value);
  }, [value]);

  const handleChange = () => {
    if (disabled) {
      return;
    }
    setCurrentValue(!currentValue);
    onChange(!currentValue);
  };

  const getInner = () => {
    if (currentValue) {
      return (
        <>
          {getIconInner()}
          {currentValue ? textChecked : textUnchecked}
        </>
      );
    }
    return (
      <>
        {currentValue ? textChecked : textUnchecked}
        {getIconInner()}
      </>
    );
  };

  const getIconInner = () => {
    if (iconChecked || iconUnchecked) {
      return <Icon size={size} iconName={currentValue ? iconChecked : iconUnchecked} />;
    }
  };

  // checked
  innerClassList.push(currentValue ? 'toggle__inner--checked' : null);
  circleClassList.push(currentValue ? 'toggle__circle--checked' : null);

  // disabled
  classList.push(disabled ? 'toggle--disabled' : null);

  // size
  classList.push(`toggle--${size}`);

  // color
  classList.push(currentValue ? `bg--${color}` : null);
  circleStyle.backgroundColor = currentValue ? getAltHexColor(color) : undefined;
  innerStyle.color = !fontColor ? getAltHexColor(color) : undefined;

  // uncheckedColor
  if (uncheckedColor && !currentValue) {
    classList.push(`bg--${uncheckedColor}`);
    circleStyle.backgroundColor = getAltHexColor(uncheckedColor);
    innerStyle.color = !fontColor ? getAltHexColor(uncheckedColor) : undefined;
  }

  // fontColor
  innerClassList.push(`text--${fontColor}`);

  // custom className
  classList.push(className);

  return (
    <span
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      id={id || undefined}
      name={name || undefined}
      role="button"
      tabIndex="-1"
      aria-pressed={currentValue}
      onClick={handleChange}
    >
      <span
        className={innerClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={innerStyle}
      >
        {getInner()}
      </span>
      <span
        className={circleClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={circleStyle}
      />
    </span>
  );
};

Object.assign(Toggle, generateComponentProps(componentData));

export default Toggle;
