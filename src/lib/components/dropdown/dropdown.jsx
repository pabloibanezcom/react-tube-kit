/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import randomId from '../../util/randomId';
import ButtonLink from '../button-link/button-link';
import Icon from '../icon/icon';
import componentData from './dropdown.data.json';

const Dropdown = ({
  buttonProps,
  children,
  className,
  id,
  menuMarginTop,
  name,
  showArrow,
  style,
  text
}) => {
  const [expanded, setExpanded] = useState(false);
  const [dropdownRandomId] = useState(randomId());

  useEffect(() => {
    const checkIfClickOutside = evt => {
      if (
        !evt.path.some(p => p.className && p.className.includes(dropdownRandomId)) ||
        evt.path[0].tagName === 'A'
      ) {
        setExpanded(false);
      }
    };

    window.addEventListener('click', checkIfClickOutside);

    return () => {
      window.removeEventListener('click', checkIfClickOutside);
    };
  }, [dropdownRandomId]);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={`dropdown dropdown-${dropdownRandomId} ${expanded ? 'show' : ''} ${className ||
        ''}`}
      style={style}
    >
      <ButtonLink
        dataToggle="dropdown"
        ariaHaspopup
        ariaExpanded={expanded}
        size="sm"
        onClick={() => setExpanded(!expanded)}
        {...buttonProps}
      >
        <div className="d-flex align-items-center">
          {text}
          <Icon className="ml-2" iconName="angle-down" />
        </div>
      </ButtonLink>
      <div
        className={`dropdown-menu ${expanded ? 'shown' : ''} ${
          showArrow ? 'dropdown-menu--arrow' : ''
        }`}
        style={{ marginTop: menuMarginTop }}
        aria-labelledby="dropdownMenuButton"
      >
        {children.map((c, i) => (
          <React.Fragment key={i}>{c}</React.Fragment>
        ))}
      </div>
    </div>
  );
};

Object.assign(Dropdown, generateComponentProps(componentData));

export default Dropdown;
