/* eslint-disable react/jsx-props-no-spreading */
import { shallow } from 'enzyme';
import React from 'react';
import FakeContent from '../../../test-resources/fakeContent';
import FakeHeader from '../../../test-resources/fakeHeader';
import lines from '../../../test-resources/lines.json';
import CollapsibleList from './collapsible-list';

describe('<CollapsibleList />', () => {
  let wrapper;

  const requiredProps = {
    elements: lines.slice(0, 3),
    header: FakeHeader,
    content: FakeContent
  };

  it('renders <CollapsibleList /> component', () => {
    wrapper = shallow(<CollapsibleList {...requiredProps} />);
    expect(wrapper.find('.collapsible-list__element').length).toBe(3);
    expect(wrapper.find(FakeHeader)).toHaveLength(3);
    expect(wrapper.find(FakeContent)).toHaveLength(3);
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<CollapsibleList className="my-class" {...requiredProps} />);
      expect(wrapper.find('.collapsible-list.my-class')).toHaveLength(1);
    });
  });

  describe('color & fontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<CollapsibleList {...requiredProps} />);
      expect(wrapper.find('.collapsible-list__element__header.bg--primary')).toHaveLength(3);

      wrapper = shallow(<CollapsibleList {...requiredProps} color="secondary" />);
      expect(wrapper.find('.collapsible-list__element__header.bg--secondary')).toHaveLength(3);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<CollapsibleList {...requiredProps} color="#1aaa55" fontColor="#222222" />);
      expect(wrapper.find('.collapsible-list__element__header').get(0).props.style).toHaveProperty(
        'backgroundColor',
        '#1aaa55'
      );
      expect(wrapper.find('.collapsible-list__element__header').get(0).props.style).toHaveProperty(
        'color',
        '#222222'
      );
    });
  });

  describe('hoverColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<CollapsibleList {...requiredProps} />);
      expect(wrapper.find('.collapsible-list__element__header.bg-hover--secondary')).toHaveLength(
        3
      );

      wrapper = shallow(<CollapsibleList {...requiredProps} hoverColor="alternative" />);
      expect(wrapper.find('.collapsible-list__element__header.bg-hover--alternative')).toHaveLength(
        3
      );
    });
  });

  describe('Show/Hide elements', () => {
    wrapper = shallow(<CollapsibleList {...requiredProps} />);
    it('shows element content when clicking on it', () => {
      wrapper
        .find('.collapsible-list__element a')
        .get(0)
        .props.onClick();
      expect(
        wrapper
          .find('.collapsible-list__element')
          .first()
          .hasClass('shown')
      ).toBe(true);
    });
    it('hides element content when clicking on it again', () => {
      wrapper
        .find('.collapsible-list__element a')
        .get(0)
        .props.onClick();
      expect(
        wrapper
          .find('.collapsible-list__element')
          .first()
          .hasClass('shown')
      ).toBe(false);
    });
  });
});
