/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { generateComponentProps } from '../../util/component';
import Icon from '../icon/icon';
import componentData from './collapsible-list.data.json';

const CollapsibleList = ({
  bindings,
  className,
  color,
  content,
  elements,
  fontColor,
  header,
  hoverColor,
  id,
  name,
  style,
  onElementSelected
}) => {
  const [activeElementId, setActiveElementId] = useState(false);

  const setActiveElement = el => {
    setActiveElementId(activeElementId !== el._id ? el._id : null);
    onElementSelected(el);
  };

  const Header = header;
  const Content = content;

  const classList = ['collapsible-list'];
  const componentStyle = { ...style };

  const headerClassList = ['collapsible-list__element__header'];
  const headerStyle = {};

  const elementShownClassList = [];
  const elementShownStyle = {};

  // color
  if (color && color.startsWith('#')) {
    headerStyle.backgroundColor = color;
  } else if (color) {
    headerClassList.push(`bg--${color}`);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    headerStyle.color = fontColor;
  } else if (fontColor) {
    headerClassList.push(`text--${fontColor}`);
  }

  // hoverColor
  if (hoverColor && hoverColor.startsWith('#')) {
    headerStyle.backgroundColor = hoverColor;
    elementShownStyle.backgroundColor = hoverColor;
  } else if (hoverColor) {
    headerClassList.push(`bg-hover--${hoverColor}`);
    elementShownClassList.push(`bg--${hoverColor}`);
  } else {
    headerClassList.push(`bg-hover-darken--${color}`);
    elementShownClassList.push(`bg-darken--${color}`);
  }

  // custom className
  classList.push(className);

  return (
    <ul
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      {elements.map((el, i) => (
        <li key={i}>
          <div className={`collapsible-list__element ${activeElementId === el._id ? 'shown' : ''}`}>
            <div
              className={headerClassList
                .filter(c => c && !c.includes('null'))
                .concat(activeElementId === el._id ? elementShownClassList : [])
                .join(' ')}
              style={
                activeElementId === el._id
                  ? Object.assign({}, headerStyle, elementShownStyle)
                  : headerStyle
              }
            >
              <a onClick={() => setActiveElement(el)}>
                <div className="collapsible-list__element__header__container">
                  <Header element={el} />
                  <Icon iconName="angle-down" />
                </div>
              </a>
            </div>
            <div className="collapsible-list__element__content">
              <Content element={el} bindings={bindings} />
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};

Object.assign(CollapsibleList, generateComponentProps(componentData));

export default CollapsibleList;
