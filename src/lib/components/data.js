import Badge from './badge/badge.data.json';
import Breadcrumb from './breadcrumb/breadcrumb.data.json';
import ButtonLink from './button-link/button-link.data.json';
import Card from './card/card.data.json';
import Carousel from './carousel/carousel.data.json';
import CheckboxGroup from './checkbox-group/checkbox-group.data.json';
import Checkbox from './checkbox/checkbox.data.json';
import CollapsibleHeading from './collapsible-heading/collapsible-heading.data.json';
import CollapsibleList from './collapsible-list/collapsible-list.data.json';
import ColorLabel from './color-label/color-label.data.json';
import ColorSelector from './color-selector/color-selector.data.json';
import CountryLabel from './country-label/country-label.data.json';
import Dropdown from './dropdown/dropdown.data.json';
import FileUpload from './file-upload/file-upload.data.json';
import Form from './form/form.data.json';
import Icon from './icon/icon.data.json';
import Image from './image/image.data.json';
import Input from './input/input.data.json';
import Label from './label/label.data.json';
import BottomNavbar from './layout/bottom-navbar/bottom-navbar.data.json';
import LayoutWrapper from './layout/layout-wrapper/layout-wrapper.data.json';
import SideNavbar from './layout/side-navbar/side-navbar.data.json';
import TopNavbar from './layout/top-navbar/top-navbar.data.json';
import LoadingSpinner from './loading-spinner/loading-spinner.data.json';
import Map from './map/map.data.json';
import Modal from './modal/modal.data.json';
import Pagination from './pagination/pagination.data.json';
import Panel from './panel/panel.data.json';
import PlaceSelector from './place-selector/place-selector.data.json';
import RadioGroup from './radio-group/radio-group.data.json';
import Radio from './radio/radio.data.json';
import Selector from './selector/selector.data.json';
import Slider from './slider/slider.data.json';
import TabMenu from './tab-menu/tab-menu.data.json';
import Toastr from './toastr/toastr.data.json';
import Toggle from './toggle/toggle.data.json';
import Tooltip from './tooltip/tooltip.data.json';

export default {
  Badge,
  BottomNavbar,
  Breadcrumb,
  ButtonLink,
  Card,
  Carousel,
  Checkbox,
  CheckboxGroup,
  CollapsibleHeading,
  CollapsibleList,
  ColorLabel,
  ColorSelector,
  CountryLabel,
  Dropdown,
  FileUpload,
  Form,
  Icon,
  Image,
  Input,
  Label,
  LayoutWrapper,
  LoadingSpinner,
  Map,
  Modal,
  Pagination,
  Panel,
  PlaceSelector,
  Radio,
  RadioGroup,
  Selector,
  SideNavbar,
  Slider,
  TabMenu,
  Toastr,
  Toggle,
  Tooltip,
  TopNavbar
};
