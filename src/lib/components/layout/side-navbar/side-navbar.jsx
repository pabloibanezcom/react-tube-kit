/* eslint-disable react/jsx-props-no-spreading */
import React, { Fragment, useEffect, useRef, useState } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from 'react-router-dom';
import { generateComponentProps } from '../../../util/component';
import useOutsideClick from '../../../util/useOutsideClick';
import { isSize } from '../../../util/viewport';
import Icon from '../../icon/icon';
import componentData from './side-navbar.data.json';

const SideNavbar = ({
  activeColor,
  className,
  collapsed,
  collapsedEnabled,
  color,
  currentPage,
  elements,
  hidden,
  id,
  locked,
  name,
  position,
  style,
  onClose,
  onCollapse
}) => {
  const [activeSections, setActiveSections] = useState(currentPage ? currentPage.split('/') : null);
  const [isCollapsed, setIsCollapsed] = useState();
  const navbarRef = useRef();

  const classList = ['side-navbar'];
  const componentStyle = { ...style };

  useEffect(() => {
    setIsCollapsed(collapsed && isSize(collapsedEnabled));
  }, [collapsed, collapsedEnabled]);

  useOutsideClick(navbarRef, () => {
    if (!hidden) {
      close();
    }
  });

  const close = () => {
    if (!locked) {
      onClose();
    }
  };

  const handleCollapse = () => {
    setIsCollapsed(!isCollapsed);
    onCollapse(!isCollapsed);
  };

  const handleMenuElementClick = (urlElements, clickedElement) => {
    if (activeSections && activeSections.indexOf(clickedElement) > -1) {
      setActiveSections(activeSections.splice(0, activeSections.indexOf(clickedElement)));
    } else {
      setActiveSections([...urlElements, clickedElement]);
    }
  };

  const getLinkStatusStyle = (url, isParentActive) => {
    if (url && currentPage === url) {
      return `bg--${activeColor} font-weight-normal`;
    }
    return isParentActive
      ? `bg-darken--${color} bg-hover-lighten--${color}`
      : `bg--${color} bg-hover-lighten--${color}`;
  };

  const getGroupTitleStatusStyle = (el, urlElements) => {
    const url = el.url ? `/${[...urlElements, el.url].join('/')}` : null;
    if (
      url &&
      currentPage &&
      currentPage.includes(url) &&
      activeSections &&
      !activeSections.includes(el.url)
    ) {
      return `bg--${activeColor} font-weight-normal`;
    }
    return `bg--${color} bg-hover-lighten--${color}`;
  };

  const renderMenuElementTitle = (el, urlElements, level) => {
    return (
      <Fragment>
        <li className="side-navbar__group-title">
          <a
            className={`side-navbar__group-title-link ${getGroupTitleStatusStyle(el, urlElements)}`}
            role="button"
            tabIndex="-1"
            onClick={() => handleMenuElementClick(urlElements, el.url)}
          >
            <div
              className="d-flex align-items-center justify-content-between"
              style={{
                paddingLeft: 10 * level
              }}
            >
              <div className="d-flex align-items-center side-navbar__group-title-name">
                {el.icon ? (
                  <Icon iconName={el.icon} className="side-navbar__nav-icon mr-3" />
                ) : null}
                {!isCollapsed ? el.name : null}
              </div>
              <div className="d-flex align-items-center side-navbar__group-title-angle">
                <Icon
                  iconName="angle-left"
                  className={`side-navbar__group-title-link__arrow ${
                    activeSections && activeSections.includes(el.url)
                      ? 'side-navbar__group-title-link__arrow--open'
                      : ''
                  }`}
                />
              </div>
            </div>
          </a>
        </li>
        <li
          className={`side-navbar__submenu ${
            activeSections && activeSections.includes(el.url) ? 'side-navbar__submenu--shown' : ''
          }`}
        >
          <ul className="side-navbar__nav-list">
            {el.children.map((child, i) => (
              <Fragment key={i}>
                {renderMenuElement(
                  child,
                  [...urlElements, el.url],
                  activeSections ? 1 + activeSections.indexOf(el.url) : 0,
                  activeSections && activeSections.includes(el.url)
                )}
              </Fragment>
            ))}
          </ul>
        </li>
      </Fragment>
    );
  };

  const renderMenuElement = (el, urlElements, level, isParentActive) => {
    const url = el.url ? `/${[...urlElements, el.url].join('/')}` : null;

    const linkContent = !isCollapsed ? (
      <div
        className={`d-flex align-items-center side-navbar__nav-link__padding-container ${
          level > 0 ? 'side-navbar__nav-link__padding-container--open' : ''
        }`}
        style={{
          paddingLeft: 10 * level
        }}
      >
        {el.icon ? <Icon iconName={el.icon} className="side-navbar__nav-icon mr-3" /> : null}
        {el.name}
      </div>
    ) : (
      <div
        className={`d-flex align-items-center side-navbar__nav-link__padding-container ${
          level > 0 ? 'side-navbar__nav-link__padding-container--open' : ''
        }`}
      >
        {el.icon ? (
          <Icon
            iconName={el.icon}
            className={`side-navbar__nav-icon ${level === 0 ? 'mr-3' : ''}`}
          />
        ) : (
          <span className={level === 0 ? 'mr-3' : ''}>{el.name.slice(0, 2)}</span>
        )}
      </div>
    );

    const linkElement = url ? (
      <Link
        className={`side-navbar__nav-link ${getLinkStatusStyle(url, isParentActive)}`}
        to={url || ''}
        onClick={close}
      >
        {linkContent}
      </Link>
    ) : (
      <a
        className={`side-navbar__nav-link ${getLinkStatusStyle(url, isParentActive)}`}
        onClick={close}
      >
        {linkContent}
      </a>
    );

    return (
      <Fragment>
        {el.children ? renderMenuElementTitle(el, urlElements, level) : <li>{linkElement}</li>}
      </Fragment>
    );
  };

  // position
  classList.push(`side-navbar--${position}`);

  // hidden
  classList.push(hidden ? 'side-navbar--hidden' : null);

  // collapsed
  classList.push(isCollapsed ? 'side-navbar--collapsed' : 'side-navbar--full');

  // locked
  classList.push(locked ? 'side-navbar--locked' : null);

  // color
  classList.push(`bg--${color}`);

  // custom className
  classList.push(className);

  return (
    <div
      ref={navbarRef}
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <Scrollbars
        renderThumbVertical={({ scrollStyle, ...props }) => (
          <div
            {...props}
            style={{ ...scrollStyle, backgroundColor: '#fff', opacity: 0.2, width: 5 }}
          />
        )}
      >
        <ul className="side-navbar__nav-list--main">
          {elements.map((el, i) => (
            <Fragment key={i}>{renderMenuElement(el, [], 0)}</Fragment>
          ))}
          {!locked && collapsedEnabled ? (
            <a
              className={`side-navbar__collapse-toggle side-navbar__nav-link ${getLinkStatusStyle()}`}
              onClick={handleCollapse}
            >
              <Icon
                iconName="chevron-left"
                className={`side-navbar__nav-icon side-navbar__collapse-toggle-icon ${
                  isCollapsed ? 'side-navbar__nav-icon--collapsed' : ''
                } mr-3`}
                size="sm"
              />
              <span>Collapse sidebar</span>
            </a>
          ) : null}
        </ul>
      </Scrollbars>
    </div>
  );
};

Object.assign(SideNavbar, generateComponentProps(componentData));

export default SideNavbar;
