import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../../util/component';
import getDisplayClass from '../../../util/getDisplayClass';
import ButtonLink from '../../button-link/button-link';
import componentData from './top-navbar.data.json';

const TopNavbar = ({
  animated,
  borderColor,
  className,
  color,
  id,
  leftContent,
  logo,
  logoDisplay,
  menuButtonDisplay,
  menuButtonPosition,
  name,
  rightContent,
  scrollHide,
  style,
  title,
  titleDisplay,
  onMenuClick
}) => {
  const [prevScrollpos, setPrevScrollpos] = useState(window.pageYOffset);
  const [visible, setVisible] = useState(true);

  const handleScroll = () => {
    setPrevScrollpos(window.pageYOffset);
    setVisible(prevScrollpos > window.pageYOffset);
  };

  useEffect(() => {
    if (scrollHide) {
      window.addEventListener('scroll', handleScroll);

      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }
  });

  const classList = ['top-navbar'];
  const componentStyle = { ...style };

  // visibility
  classList.push(!visible ? 'top-navbar--hiden' : null);

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else if (color) {
    classList.push(`bg--${color}`);
  }

  // borderColor
  if (borderColor && borderColor.startsWith('#')) {
    componentStyle.borderColor = borderColor;
  } else if (borderColor) {
    classList.push(`border--${borderColor}`);
  }

  // custom className
  classList.push(className);

  const renderLogo = () => {
    return logo ? (
      <img
        className={`top-navbar__logo ${getDisplayClass(logoDisplay)} ${
          animated ? 'animated zoomInDown animation-delay-5' : null
        }`}
        src={logo}
        alt="Logo"
      />
    ) : null;
  };

  const renderTitle = () => {
    return title ? (
      <h1
        className={`top-navbar__title ${getDisplayClass(titleDisplay)} ${
          animated ? 'animated fadeInRight animation-delay-6' : ''
        }`}
      >
        {title}
      </h1>
    ) : null;
  };

  const renderMenuButton = () => (
    <div>
      <ButtonLink
        color="transparent"
        icon="menu"
        iconSize="lg"
        size="lg"
        className={`${getDisplayClass(menuButtonDisplay)} top-navbar__menu-buttom p-0`}
        onClick={onMenuClick}
      />
    </div>
  );

  return (
    <header
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <div className="top-navbar__container container d-flex align-items-center h-100 p-0">
        <div className="top-navbar__left flex-grow-1 pl-3">
          {menuButtonPosition === 'left' ? renderMenuButton() : null}
          {renderLogo()}
          {renderTitle()}
          {leftContent}
        </div>
        <div className="top-navbar__right pr-3">
          {rightContent}
          {menuButtonPosition === 'right' ? renderMenuButton() : null}
        </div>
      </div>
    </header>
  );
};

Object.assign(TopNavbar, generateComponentProps(componentData));

export default TopNavbar;
