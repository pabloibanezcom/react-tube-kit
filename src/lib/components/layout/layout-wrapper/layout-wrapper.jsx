/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../../util/component';
import { isSize } from '../../../util/viewport';
import LoadingSpinner from '../../loading-spinner/loading-spinner';
import BottomNavbar from '../bottom-navbar/bottom-navbar';
import SideNavbar from '../side-navbar/side-navbar';
import TopNavbar from '../top-navbar/top-navbar';
import componentData from './layout-wrapper.data.json';

const LayoutWrapper = ({
  bottomNavbar,
  children,
  className,
  currentPage,
  footer,
  id,
  loading,
  name,
  sideNavbarHidden,
  sideNavbarLocked,
  sideNavbarOverlay,
  sideNavbar,
  style,
  topNavbar
}) => {
  const [isSideNavbarHidden, setIsSideNavbarHidden] = useState(isSize(sideNavbarHidden));
  const [isSideNavbarOverlay, setIsSideNavbarOverlay] = useState(isSize(sideNavbarOverlay));
  const [isSideNavbarCollapsed, setIsSideNavbarCollapsed] = useState(false);
  const [isSideNavbarLocked, setIsSideNavbarLocked] = useState(isSize(sideNavbarLocked));

  useEffect(() => {
    const handleResize = () => {
      setIsSideNavbarHidden(isSize(sideNavbarHidden));
      setIsSideNavbarOverlay(isSize(sideNavbarOverlay));
      setIsSideNavbarLocked(isSize(sideNavbarLocked));
    };

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const toggleSideNavbar = () => {
    setIsSideNavbarHidden(!isSideNavbarHidden);
  };

  const classList = ['layout'];
  const componentStyle = { ...style };

  const pageLayoutClassList = ['layout__page'];

  // Side Navbar position
  if (sideNavbar) {
    pageLayoutClassList.push(
      sideNavbar.position === 'right' ? 'layout__page--right' : 'layout__page--left'
    );
  }

  // exists bottom navbar
  classList.push(bottomNavbar ? 'layout--with-bottom-navbar' : '');

  // collapsed
  classList.push(isSideNavbarCollapsed && !isSideNavbarLocked ? 'layout--collapsed' : null);

  // hidden
  classList.push(isSideNavbarHidden ? 'layout--hidden' : null);

  // overlay
  classList.push(isSideNavbarOverlay ? 'layout--overlay' : null);

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <LoadingSpinner loading={loading} background="dark" />
      {topNavbar ? <TopNavbar {...topNavbar} onMenuClick={toggleSideNavbar} /> : null}
      <div className={pageLayoutClassList.filter(c => c && !c.includes('null')).join(' ')}>
        {sideNavbar ? (
          <SideNavbar
            {...sideNavbar}
            collapsed={isSideNavbarCollapsed}
            currentPage={currentPage}
            hidden={isSideNavbarHidden}
            locked={isSideNavbarLocked}
            onClose={toggleSideNavbar}
            onCollapse={val => setIsSideNavbarCollapsed(val)}
          />
        ) : null}
        {isSideNavbarOverlay ? (
          <div className={`overlay bg--dark ${isSideNavbarHidden ? 'overlay--hidden' : ''}`} />
        ) : null}
        <div className="layout__content">{children}</div>
        {footer ? <footer>{footer}</footer> : null}
      </div>
      {bottomNavbar ? <BottomNavbar {...bottomNavbar} /> : null}
    </div>
  );
};

Object.assign(LayoutWrapper, generateComponentProps(componentData));

export default LayoutWrapper;
