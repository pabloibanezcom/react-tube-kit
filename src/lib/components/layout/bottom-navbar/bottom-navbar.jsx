import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../../util/component';
import getDisplayClass from '../../../util/getDisplayClass';
import ButtonLink from '../../button-link/button-link';
import Icon from '../../icon/icon';
import componentData from './bottom-navbar.data.json';

const BottomNavbar = ({
  buttonColor,
  className,
  color,
  display,
  elements,
  fontColor,
  iconColor,
  iconsOnly,
  id,
  name,
  scrollHide,
  style
}) => {
  const [prevScrollpos, setPrevScrollpos] = useState(window.pageYOffset);
  const [visible, setVisible] = useState(true);

  const handleScroll = () => {
    setPrevScrollpos(window.pageYOffset);
    setVisible(prevScrollpos > window.pageYOffset);
  };

  useEffect(() => {
    if (scrollHide) {
      window.addEventListener('scroll', handleScroll);

      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }
  });

  const classList = ['bottom-navbar'];
  const componentStyle = { ...style };

  // Display
  classList.push(getDisplayClass(display));

  // visibility
  classList.push(!visible ? 'bottom-navbar--hiden' : null);

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else if (color) {
    classList.push(`bg--${color}`);
  }

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <ul>
        {elements.map(mEl => (
          <li key={mEl.name}>
            {mEl.button ? (
              <ButtonLink circle icon={mEl.icon} size="lg" color={buttonColor} />
            ) : (
              <ButtonLink color={fontColor} type="link" textAlignment="center" to={mEl.url}>
                <Icon
                  color={iconColor}
                  iconName={mEl.icon}
                  className={iconsOnly ? 'bottom-navbar__icon-only' : ''}
                />
                {!iconsOnly && <span className="font-weight-light">{mEl.name}</span>}
              </ButtonLink>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

Object.assign(BottomNavbar, generateComponentProps(componentData));

export default BottomNavbar;
