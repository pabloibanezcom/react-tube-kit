import { shallow } from 'enzyme';
import React from 'react';
import ButtonLink from '../../button-link/button-link';
import Icon from '../../icon/icon';
import BottomNavbar from './bottom-navbar';

describe('<BottomNavbar />', () => {
  let wrapper;

  const menuElements = [
    {
      name: 'Map',
      icon: 'marker',
      url: 'map'
    },
    {
      name: 'Cities',
      icon: 'city',
      url: 'cities'
    },
    {
      name: 'My area',
      icon: 'user',
      url: 'user'
    },
    {
      name: 'Shop',
      icon: 'shopping-cart',
      url: 'shop'
    }
  ];

  it('renders <Badge /> component', () => {
    wrapper = shallow(<BottomNavbar elements={menuElements} />);
    expect(wrapper.find('.bottom-navbar')).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} className="my-class" />);
      expect(wrapper.find('.bottom-navbar.my-class')).toHaveLength(1);
    });
  });

  describe('color', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} />);
      expect(wrapper.find('.bg--secondary')).toHaveLength(1);

      wrapper = shallow(<BottomNavbar elements={menuElements} color="primary" />);
      expect(wrapper.find('.bg--primary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} color="#1aaa55" />);
      expect(wrapper.get(0).props.style).toHaveProperty('backgroundColor', '#1aaa55');
    });
  });

  describe('fontColor', () => {
    it('set ButtonLink color as fontColor when fontColor is set as application as color string', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} />);
      expect(
        wrapper
          .find(ButtonLink)
          .first()
          .prop('color')
      ).toEqual('white');

      wrapper = shallow(<BottomNavbar elements={menuElements} fontColor="primary" />);
      expect(
        wrapper
          .find(ButtonLink)
          .first()
          .prop('color')
      ).toEqual('primary');
    });

    it('set ButtonLink color as fontColor when fontColor is set as application as color hex', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} fontColor="#1aaa55" />);
      expect(
        wrapper
          .find(ButtonLink)
          .first()
          .prop('color')
      ).toEqual('#1aaa55');
    });
  });

  describe('iconColor', () => {
    it('set Icon color as iconColor when iconColor is set as application as color string', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} />);
      expect(
        wrapper
          .find(Icon)
          .first()
          .prop('color')
      ).toEqual(null);

      wrapper = shallow(<BottomNavbar elements={menuElements} iconColor="primary" />);
      expect(
        wrapper
          .find(Icon)
          .first()
          .prop('color')
      ).toEqual('primary');
    });

    it('set Icon color as iconColor when iconColor is set as application as color hex', () => {
      wrapper = shallow(<BottomNavbar elements={menuElements} iconColor="#1aaa55" />);
      expect(
        wrapper
          .find(Icon)
          .first()
          .prop('color')
      ).toEqual('#1aaa55');
    });
  });
});
