import { shallow } from 'enzyme';
import React from 'react';
import ColorLabel from './color-label';

describe('<ColorLabel />', () => {
  let wrapper;

  it('renders <ColorLabel /> component', () => {
    wrapper = shallow(<ColorLabel color="#ee2112" />);
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<ColorLabel color="#ee2112" className="my-class" />);
      expect(wrapper.find('.color-label.my-class')).toHaveLength(1);
    });
  });

  describe('color', () => {
    it('set name color succesfully', () => {
      wrapper = shallow(<ColorLabel color="red" className="my-class" />);
      expect(wrapper.text()).toBe('red');
    });

    it('set HEX color succesfully', () => {
      wrapper = shallow(<ColorLabel color="#ee2112" className="my-class" />);
      expect(wrapper.text()).toBe('#ee2112');
    });

    it('set RGB color succesfully', () => {
      wrapper = shallow(<ColorLabel color="255, 87, 51" className="my-class" />);
      expect(wrapper.text()).toBe('255, 87, 51');
    });
  });
});
