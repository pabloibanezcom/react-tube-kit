import React from 'react';
import { generateComponentProps } from '../../util/component';
import componentData from './color-label.data.json';

const ColorLabel = ({ bgColor, className, color, id, name, style }) => {
  const classList = ['d-flex align-items-center color-label'];
  const componentStyle = { ...style };

  const boxClassList = ['color-label__color-box'];
  const boxStyle = {};

  // bgColor
  if (bgColor && bgColor.startsWith('#')) {
    boxStyle.backgroundColor = color;
  } else {
    boxClassList.push(`bg--${bgColor}`);
  }

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <div
        className={boxClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={boxStyle}
      >
        <div style={{ backgroundColor: color }} />
      </div>
      {color}
    </div>
  );
};

Object.assign(ColorLabel, generateComponentProps(componentData));

export default ColorLabel;
