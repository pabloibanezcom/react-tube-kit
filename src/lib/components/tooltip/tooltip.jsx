/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useRef } from 'react';
import { getHexColor } from '../../util/color';
import { generateComponentProps } from '../../util/component';
import componentData from './tooltip.data.json';

const Tooltip = ({ className, color, fontColor, id, name, style, position, text, onRendered }) => {
  const ref = useRef();

  useEffect(() => {
    onRendered(ref.current.getBoundingClientRect());
  }, []);

  const classList = ['tooltip'];
  const componentStyle = { ...style };

  const arrowClassList = ['tooltip__arrow'];
  const arrowStyle = { ...style };

  const borderPropName = `border${position.charAt(0).toUpperCase() + position.slice(1)}Color`;

  // position
  classList.push(`tooltip--${position}`);
  arrowClassList.push(`tooltip__arrow--${position}`);

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
    arrowStyle[borderPropName] = color;
  } else {
    classList.push(`bg--${color}`);
    arrowStyle[borderPropName] = getHexColor(color);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    componentStyle.color = fontColor;
  } else if (fontColor) {
    classList.push(`text--${fontColor}`);
  }

  // custom className
  classList.push(className);
  return (
    <Fragment>
      <div
        ref={ref}
        id={id || undefined}
        name={name || undefined}
        className={classList.filter(c => c && !c.includes('null')).join(' ')}
        style={componentStyle}
        role="tooltip"
      >
        {text}
      </div>
      <div
        className={arrowClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={arrowStyle}
      />
    </Fragment>
  );
};

Object.assign(Tooltip, generateComponentProps(componentData));

export default Tooltip;
