/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import ReactDOM from 'react-dom';
import Tooltip from '../tooltip';

const tooltipContainerName = 'tooltip-container';
const tooltipContainerNameAnimation = 'tooltip-container--in';

// eslint-disable-next-line no-unused-vars
export const renderTooltip = (tooltip, boundings) => {
  let htmlElement;

  const setTooltipBoundings = tooltipBoundings => {
    htmlElement.style.cssText = getTopLeft(boundings, tooltipBoundings, tooltip.position);
    htmlElement.className = `${tooltipContainerName} ${tooltipContainerNameAnimation}`;
  };

  const renderHtml = () => {
    htmlElement = document.createElement('div');
    htmlElement.id = tooltipContainerName;
    htmlElement.className = tooltipContainerName;
    ReactDOM.render(<Tooltip {...tooltip} onRendered={setTooltipBoundings} />, htmlElement);
    document.body.appendChild(htmlElement);
  };

  if (!boundings) {
    return;
  }

  const tooltipContainer = document.getElementById(tooltipContainerName);

  if (tooltipContainer) {
    document.body.removeChild(document.getElementById(tooltipContainerName));
  }

  renderHtml();
};

export const removeTooltip = () => {
  const tooltipContainer = document.getElementById(tooltipContainerName);
  if (tooltipContainer) {
    const htmlElement = document.getElementById(tooltipContainerName);
    htmlElement.classList.remove(tooltipContainerNameAnimation);
  }
};

const getTopLeft = (boundings, tooltipBoundings, position = 'right') => {
  const padding = 2;
  const tooltipArrow = 6;
  const topLeft = {};

  if (position === 'right') {
    topLeft.top = boundings.top - (tooltipBoundings.height - boundings.height) / 2;
    topLeft.left = boundings.left + boundings.width + tooltipArrow + padding;
  }
  if (position === 'left') {
    topLeft.top = boundings.top - (tooltipBoundings.height - boundings.height) / 2;
    topLeft.left = boundings.left - (tooltipBoundings.width + tooltipArrow) - padding;
  }
  if (position === 'top') {
    topLeft.top = boundings.top - (tooltipBoundings.height + tooltipArrow) - padding;
    topLeft.left = boundings.left - (tooltipBoundings.width - boundings.width) / 2;
  }
  if (position === 'bottom') {
    topLeft.top = boundings.top + boundings.height + tooltipArrow + padding;
    topLeft.left = boundings.left - (tooltipBoundings.width - boundings.width) / 2;
  }
  return `top:${topLeft.top}px;left:${topLeft.left}px`;
};
