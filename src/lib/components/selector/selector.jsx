/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { generateComponentProps } from '../../util/component';
import useOutsideClick from '../../util/useOutsideClick';
import ButtonLink from '../button-link/button-link';
import Icon from '../icon/icon';
import Input from '../input/input';
import componentData from './selector.data.json';

const Selector = ({
  className,
  custom,
  customProp,
  disableInternalSearch,
  error,
  id,
  inputClearable,
  minLengthSearch,
  maxOptions,
  name,
  nameProp,
  // eslint-disable-next-line no-unused-vars
  native,
  nullOption,
  optionClassName,
  options,
  placeholder,
  returnOnlyValue,
  search,
  showScroll,
  style,
  value,
  valueProp,
  onChange,
  onSearchChange
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [optionsWithNull, setOptionsWithNull] = useState([]);
  const [selectedOption, setSelectedOption] = useState(() => {
    return nullOption ? { ...nullOption, isNullOption: true } : null;
  });
  const [focusedIndex, setFocusedIndex] = useState(-1);
  const [searchStr, setSearchStr] = useState('');
  // const windowSize = useWindowSize();
  const menuRef = useRef();
  const Custom = custom;

  useEffect(() => {
    setOptionsWithNull(nullOption ? [{ ...nullOption, isNullOption: true }, ...options] : options);
  }, [options]);

  useEffect(() => {
    const checkIfOpen = evt => {
      if (isOpen) {
        handleKeyDown(evt);
      }
    };
    window.addEventListener('keydown', checkIfOpen);
    return () => {
      window.removeEventListener('keydown', checkIfOpen);
    };
  }, [isOpen, focusedIndex]);

  useEffect(() => {
    if (value !== undefined && value !== null) {
      setSelectedOption(
        value[valueProp] ? value : options.find(option => option[valueProp] === value)
      );
    }
  }, [value]);

  useOutsideClick(menuRef, () => {
    if (isOpen) {
      setIsOpen(false);
    }
  });

  const handleOptionSelected = option => {
    setSelectedOption(option);
    setSearchStr('');
    setTimeout(() => setIsOpen(false), 0);
    onChange(returnOnlyValue ? option[valueProp] : option);
  };

  const handleSearchChange = str => {
    if (str.length >= minLengthSearch) {
      onSearchChange(str);
    }
    if (!disableInternalSearch) {
      setSearchStr(str.toLowerCase());
    }

    // setOptionsWithNull(nullOption ? [{ ...nullOption, isNullOption: true }] : []);
  };

  const handleKeyDown = evt => {
    if (evt.key === 'Tab') {
      setIsOpen(false);
    } else if (evt.key === 'ArrowUp') {
      if (focusedIndex > 0) {
        setFocusedIndex(focusedIndex - 1);
      }
      evt.preventDefault();
    } else if (evt.key === 'ArrowDown') {
      if (focusedIndex < optionsWithNull.length - 1) {
        setFocusedIndex(focusedIndex + 1);
      }
      evt.preventDefault();
    } else if (evt.key === 'Enter') {
      handleOptionSelected(optionsWithNull.filter(opt => filterOption(opt))[focusedIndex]);
    }
  };

  const filterOption = opt => {
    if (
      searchStr &&
      searchStr.length > minLengthSearch - 1 &&
      !opt[nameProp].toLowerCase().includes(searchStr)
    ) {
      return false;
    }
    return true;
  };

  const getIndexFirstToShow = () => {
    return focusedIndex - (maxOptions - 1) < 0 ? 0 : focusedIndex - (maxOptions - 1);
  };

  const optionHtml = (opt, isOptionSelected) => {
    if (custom && customProp) {
      const dynamicProps = { [customProp]: opt };
      return (
        <Custom
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...dynamicProps}
          className={`${isOptionSelected ? 'selector-option-selected' : ''}`}
        />
      );
    }
    if (custom) {
      return (
        <Custom className={`${isOptionSelected ? 'selector-option-selected' : ''}`}>opt</Custom>
      );
    }
    return opt[nameProp];
  };

  const optionsList = (
    <ul>
      {optionsWithNull
        .filter(opt => filterOption(opt))
        .slice(getIndexFirstToShow(), getIndexFirstToShow() + maxOptions)
        .map((opt, i) => {
          return (
            <li key={opt[valueProp] || i + 1}>
              <a
                role="option"
                aria-disabled="false"
                aria-selected="false"
                className={`${optionClassName} ${
                  selectedOption && opt && opt[valueProp] === selectedOption[valueProp]
                    ? 'selected'
                    : ''
                } 
                ${
                  focusedIndex > -1 &&
                  optionsWithNull &&
                  optionsWithNull[focusedIndex] &&
                  opt &&
                  optionsWithNull[focusedIndex][valueProp] === opt[valueProp]
                    ? 'focused'
                    : ''
                }`}
                onMouseEnter={() => setFocusedIndex(i)}
                onClick={() => handleOptionSelected(opt)}
              >
                {optionHtml(opt)}
              </a>
            </li>
          );
        })}
    </ul>
  );

  // eslint-disable-next-line no-unused-vars
  const customSelect = (
    <>
      <ButtonLink
        className={`d-flex align-items-center selector__button ${
          error ? 'selector__button--error' : ''
        } underline--light-primary`}
        color="transparent"
        uppercase={false}
        size="lg"
        onClick={() => setIsOpen(true)}
      >
        {!selectedOption ? placeholder : optionHtml(selectedOption, true)}
        <Icon className="selector-icon" iconName="angle-down" size="sm" />
      </ButtonLink>
      <input type="hidden" value={(selectedOption && selectedOption[valueProp]) || ''} />
      <div className="position-relative">
        <div className={`selector-menu ${isOpen ? 'show' : ''}`} ref={menuRef} tabIndex="0">
          {search ? (
            <div className="selector-searchbox">
              <Input value={searchStr} onChange={handleSearchChange} clearable={inputClearable} />
            </div>
          ) : null}
          <div role="listbox" aria-expanded={isOpen} tabIndex="-1">
            {showScroll ? <Scrollbars autoHeight>{optionsList}</Scrollbars> : optionsList}
          </div>
        </div>
      </div>
    </>
  );

  // eslint-disable-next-line no-unused-vars
  const nativeSelect = (
    <div className="select-wrapper d-flex align-items-center">
      <select>
        <option>{placeholder}</option>
        {optionsWithNull.map(opt => (
          <option key={opt[valueProp]} value={opt[valueProp]}>
            {opt[nameProp]}
          </option>
        ))}
      </select>
      <Icon className="selector-icon" iconName="angle-down" size="sm" />
    </div>
  );

  return (
    <>
      <div
        id={id || undefined}
        name={name || undefined}
        className={`selector d-flex flex-column w-100 ${className || ''}`}
        style={style}
      >
        {/* {isSize(windowSize, native) ? <>{nativeSelect}</> : <>{customSelect}</>} */}
        {customSelect}
      </div>
    </>
  );
};

Object.assign(Selector, generateComponentProps(componentData));

export default Selector;
