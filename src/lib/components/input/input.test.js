import { shallow } from 'enzyme';
import React from 'react';
import Input from './input';

describe('<Input />', () => {
  let wrapper;

  it('renders <Input /> component', () => {
    wrapper = shallow(<Input />);
    expect(wrapper.find('.input')).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Input className="my-class" />);
      expect(wrapper.find('.input-container.my-class')).toHaveLength(1);
    });
  });

  describe('placeholder', () => {
    it('applies placeholder to input when set', () => {
      wrapper = shallow(<Input placeholder="This is a placeholder" />);
      expect(wrapper.find('.input').get(0).props.placeholder).toBe('This is a placeholder');
    });
  });

  describe('disabled', () => {
    it('set input as disabled when set property', () => {
      wrapper = shallow(<Input disabled />);
      expect(wrapper.find('.input').get(0).props.disabled).toBe(true);
    });
  });

  describe('readonly', () => {
    it('set input as readonly when set property', () => {
      wrapper = shallow(<Input readOnly />);
      expect(wrapper.find('.input').get(0).props.readOnly).toBe(true);
    });
  });

  describe('multiple', () => {
    it('set input as multiple when set property', () => {
      wrapper = shallow(<Input multiple />);
      expect(wrapper.find('.input').get(0).props.multiple).toBe(true);
    });
  });

  describe('accept', () => {
    it('set input as accept when set property', () => {
      wrapper = shallow(<Input accept="image/png, image/jpeg" />);
      expect(wrapper.find('.input').get(0).props.accept).toBe('image/png, image/jpeg');
    });
  });

  describe('type', () => {
    it('set input as type when set property', () => {
      wrapper = shallow(<Input type="number" />);
      expect(wrapper.find('.input').get(0).props.type).toBe('number');
    });
  });

  describe('color & hoverColor', () => {
    it('applies color and hover class when color is set as application as color string', () => {
      wrapper = shallow(<Input />);
      expect(wrapper.find('.underline--light-primary')).toHaveLength(1);

      wrapper = shallow(<Input color="primary" />);
      expect(wrapper.find('.underline--primary-primary')).toHaveLength(1);

      wrapper = shallow(<Input hoverColor="secondary" />);
      expect(wrapper.find('.underline--light-secondary')).toHaveLength(1);

      wrapper = shallow(<Input color="secondary" hoverColor="alternative" />);
      expect(wrapper.find('.underline--secondary-alternative')).toHaveLength(1);
    });
  });

  describe('fontColor', () => {
    it('applies fontColor class when color is set as application as color string', () => {
      wrapper = shallow(<Input fontColor="secondary" />);
      expect(wrapper.find('.text--secondary')).toHaveLength(1);
    });

    it('applies fontColor style when color is set as application as color hex', () => {
      wrapper = shallow(<Input fontColor="#222222" />);
      expect(wrapper.find('.input').get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('onChange', () => {
    it('invokes onChange when value changes', () => {
      const onChangeCallback = jest.fn();
      wrapper = shallow(<Input onChange={onChangeCallback} />);
      wrapper.find('.input').simulate('change', { target: { value: 'Some text' } });
      expect(onChangeCallback).toHaveBeenCalled();
    });
  });
});
