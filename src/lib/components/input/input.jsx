/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import useOutsideClick from '../../util/useOutsideClick';
import ButtonLink from '../button-link/button-link';
import Icon from '../icon/icon';
import componentData from './input.data.json';

const Input = React.forwardRef(
  (
    {
      accept,
      className,
      clearable,
      color,
      disabled,
      error,
      fontColor,
      hoverColor,
      icon,
      iconColor,
      iconExtendible,
      iconOpacity,
      iconPosition,
      iconSize,
      id,
      multiple,
      name,
      placeholder,
      readOnly,
      style,
      value,
      type,
      onChange,
      onClick,
      extensionProps
    },
    ref
  ) => {
    const [currentValue, setCurrentValue] = useState(value || '');
    const [extendibleShown, setExtendibleShown] = useState(false);

    const inputRef = ref || React.createRef();

    useEffect(() => {
      setCurrentValue(value || '');
    }, [value]);

    const handleOnChange = newValue => {
      setCurrentValue(newValue);
      onChange(newValue);
      if (type === 'file') {
        setCurrentValue('');
      }
    };

    const handleToggleExtendible = () => {
      if (!extendibleShown) {
        inputRef.current.focus();
      }
      setExtendibleShown(!extendibleShown);
    };

    useOutsideClick(inputRef, () => {
      if (extendibleShown && currentValue === '') {
        setExtendibleShown(false);
      }
    });

    const clearValue = () => {
      handleOnChange('');
    };

    const containerClassList = ['input-container', 'position-relative'];
    const containerStyle = { ...style };

    const classList = ['input', 'px-1'];
    const inputStyle = {};

    // error
    classList.push(error ? `input--error` : null);

    // color & hoverColor
    classList.push(`underline--${color}-${error ? 'danger' : hoverColor}`);

    // fontColor
    if (fontColor && fontColor.startsWith('#')) {
      inputStyle.color = fontColor;
    } else if (fontColor) {
      classList.push(`text--${fontColor}`);
    } else {
      inputStyle.color = 'inherit';
    }

    // icon
    if (icon && iconPosition === 'left') {
      classList.push('input--with-icon-left');
    }
    if (icon && iconPosition === 'right') {
      classList.push('input--with-icon-right');
    }

    // iconExtendible
    classList.push(iconExtendible ? 'input__extendible' : null);
    if (iconExtendible && extendibleShown) {
      classList.push('input__extendible--shown');
      classList.push(
        iconPosition === 'left' ? 'input__extendible--shown-right' : 'input__extendible--shown-left'
      );
    }

    // custom className
    containerClassList.push(className);

    const renderIcon = () => {
      if (iconExtendible) {
        return (
          <ButtonLink
            type="link"
            className={`input__icon input__icon--${iconPosition}`}
            icon={icon}
            onClick={handleToggleExtendible}
          />
        );
      }
      if (icon) {
        return (
          <Icon
            className={`input__icon input__icon--${iconPosition}`}
            iconName={icon}
            color={iconColor}
            size={iconSize}
            opacity={iconOpacity}
          />
        );
      }
      return null;
    };

    return (
      <div
        className={containerClassList.filter(c => c && !c.includes('null')).join(' ')}
        style={containerStyle}
      >
        <input
          ref={inputRef}
          id={id || undefined}
          name={name || undefined}
          className={classList.filter(c => c && !c.includes('null')).join(' ')}
          style={inputStyle}
          type={type}
          accept={accept}
          value={currentValue}
          placeholder={placeholder}
          disabled={disabled}
          readOnly={readOnly}
          multiple={multiple}
          onChange={evt => handleOnChange(evt.target.value)}
          onClick={onClick}
          {...extensionProps}
        />
        {renderIcon()}
        {clearable && currentValue ? (
          <button className="input__clearable-btn" type="button" onClick={clearValue}>
            <Icon iconName="close" size="sm" />
          </button>
        ) : null}
      </div>
    );
  }
);

Object.assign(Input, generateComponentProps(componentData));

export default Input;
