// import { mount, shallow } from 'enzyme';
// import React from 'react';
// import ButtonLink from '../button-link/button-link';
// import Panel from '../panel/panel';
// import Modal from './modal';

// describe('<Modal />', () => {
//   let wrapper;

//   it('renders <Modal /> component', () => {
//     wrapper = shallow(<Modal content="This is a basic modal content" isOpen />);
//     expect(wrapper.find('.modal')).toHaveLength(1);
//     expect(wrapper).toMatchSnapshot();
//   });

//   describe('custom class', () => {
//     it('applies custom class when className is set', () => {
//       wrapper = shallow(
//         <Modal content="This is a basic modal content" isOpen className="my-class" />
//       );
//       expect(wrapper.find('.modal.my-class')).toHaveLength(1);
//     });
//   });

//   describe('content', () => {
//     it('renders a plain HTML string as content', () => {
//       wrapper = mount(<Modal content="This is a html content" isOpen />);a
//       expect(wrapper.find('.panel-content').text()).toBe('This is a html content');
//     });

//     it('renders a component function as content', () => {
//       const mockContent = () => (
//         <div>
//           <span>This content is a rendered component</span>
//         </div>
//       );
//       wrapper = mount(<Modal content={mockContent} isOpen />);
//       expect(wrapper.find(mockContent)).toHaveLength(1);
//       expect(wrapper.find('.panel-content').text()).toBe('This content is a rendered component');
//     });
//   });

//   describe('color', () => {
//     it('set Panel color when color prop is defined', () => {
//       wrapper = shallow(<Modal content="This is a html content" color="secondary" isOpen />);
//       expect(wrapper.find(Panel).prop('color')).toEqual('secondary');
//     });
//   });

//   describe('closeButtonColor', () => {
//     it('set close link color when closeButtonColor prop is defined', () => {
//       wrapper = shallow(
//         <Modal content="This is a html content" closeButtonColor="secondary" isOpen />
//       );
//       expect(wrapper.find(ButtonLink).prop('color')).toEqual('secondary');
//     });
//   });

//   describe('showCloseButton', () => {
//     it('shows close button when showCloseButton is set to true', () => {
//       wrapper = shallow(<Modal content="This is a html content" isOpen showCloseButton />);
//       expect(wrapper.find(ButtonLink)).toHaveLength(1);
//     });

//     it('does not show close button when showCloseButton is set to false', () => {
//       wrapper = shallow(<Modal content="This is a html content" isOpen showCloseButton={false} />);
//       expect(wrapper.find(ButtonLink)).toHaveLength(0);
//     });
//   });

//   describe('onClose', () => {
//     it('invokes onClose when modal is closed', () => {
//       const closeCallback = jest.fn();
//       wrapper = shallow(<Modal content="This is a html content" isOpen onClose={closeCallback} />);
//       wrapper
//         .find('.modal__close-button')
//         .get(0)
//         .props.onClick();
//       expect(closeCallback).toHaveBeenCalled();
//     });
//   });
// });
