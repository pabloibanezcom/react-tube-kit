/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { default as ReactModal } from 'react-modal';
import { generateComponentProps } from '../../util/component';
import ButtonLink from '../button-link/button-link';
import Panel from '../panel/panel';
import componentData from './modal.data.json';

const Modal = ({
  animation,
  animationSpeed,
  className,
  closeButtonColor,
  color,
  content,
  fullScreen,
  id,
  isOpen,
  name,
  panelProps,
  shouldCloseOnOverlayClick,
  showCloseButton,
  style,
  onClose
}) => {
  const [isShown, setIsShown] = useState(isOpen);
  const Content = typeof content === 'function' ? content : null;

  useEffect(() => {
    setIsShown(isOpen);
  }, [isOpen]);

  const handleOnClose = () => {
    setIsShown(false);
    onClose();
  };

  const classList = ['modal'];
  const componentStyle = { ...style };

  // fullScreen
  classList.push(fullScreen ? `modal__fullscreen` : null);

  // animation
  classList.push(animation ? `animated ${animation} ${animationSpeed}` : null);

  // custom className
  classList.push(className);

  return (
    <ReactModal
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      overlayClassName="modal__overlay"
      onRequestClose={shouldCloseOnOverlayClick ? handleOnClose : undefined}
      isOpen={isShown}
      ariaHideApp={false}
    >
      <Panel color={color} {...panelProps}>
        {Content ? <Content /> : <div dangerouslySetInnerHTML={{ __html: content }} />}
      </Panel>
      {showCloseButton ? (
        <ButtonLink
          type="link"
          text=""
          icon="close"
          color={closeButtonColor}
          className="modal__close-button"
          onClick={handleOnClose}
        />
      ) : null}
    </ReactModal>
  );
};

Object.assign(Modal, generateComponentProps(componentData));

export default Modal;
