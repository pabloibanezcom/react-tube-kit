/* eslint-disable react/jsx-props-no-spreading */
import React, { Fragment, useState } from 'react';
import { generateComponentProps } from '../../util/component';
import randomId from '../../util/randomId';
import Checkbox from '../checkbox/checkbox';
import componentData from './checkbox-group.data.json';

const CheckboxGroup = ({
  checkAll,
  className,
  color,
  direction,
  fontColor,
  id,
  max,
  name,
  options,
  style,
  value,
  onChange
}) => {
  const [currentValue, setCurrentValue] = useState(value || []);

  const classList = ['checkbox-group'];
  const componentStyle = { ...style };

  const checkBoxclassList = [];

  // direction
  classList.push('d-flex', direction === 'horizontal' ? 'flex-row' : 'flex-column');
  checkBoxclassList.push(direction === 'horizontal' ? 'mr-4' : null);

  // custom className
  classList.push(className);

  const handleOptionChanged = (option, checked) => {
    let newValue;
    if (checked) {
      newValue = [...currentValue, option];
    } else {
      newValue = currentValue.filter(o => o !== option);
    }
    setCurrentValue(newValue);
    onChange(newValue);
  };

  const handleChangeAll = () => {
    let newValue;
    if (currentValue.length === options.length) {
      newValue = [];
    } else {
      newValue = options.map(opt => opt.value);
    }
    setCurrentValue(newValue);
    onChange(newValue);
  };

  const groupName = name || `checkbox-group-${randomId()}`;

  const optionCheckbox = option => (
    <Checkbox
      checked={currentValue.includes(option.value)}
      color={color}
      disabled={max && !currentValue.includes(option.value) && currentValue.length >= max}
      fontColor={fontColor}
      name={groupName}
      label={option.label}
      className={checkBoxclassList.filter(c => c && !c.includes('null')).join(' ')}
      onChange={checked => handleOptionChanged(option.value, checked)}
      {...option.checkbox}
    />
  );

  const checkAllCheckBox = (
    <Checkbox
      {...checkAll}
      checked={currentValue.length === options.length}
      indeterminate={currentValue.length > 0 && currentValue.length < options.length}
      onChange={handleChangeAll}
    />
  );

  const checkGroup = (
    <div
      id={id || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      role="group"
    >
      {options.map(opt => (
        <Fragment key={opt.value}>{optionCheckbox(opt)}</Fragment>
      ))}
    </div>
  );

  return (
    <Fragment>
      {checkAll ? (
        <div>
          {checkAllCheckBox}
          {checkGroup}
        </div>
      ) : (
        checkGroup
      )}
    </Fragment>
  );
};

Object.assign(CheckboxGroup, generateComponentProps(componentData));

export default CheckboxGroup;
