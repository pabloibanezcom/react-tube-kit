import React, { useEffect, useState } from 'react';
import { getAltHexColor } from '../../util/color';
import { generateComponentProps } from '../../util/component';
import componentData from './checkbox.data.json';

const Checkbox = ({
  className,
  checked,
  color,
  disabled,
  fontColor,
  id,
  indeterminate,
  label,
  name,
  style,
  onChange
}) => {
  const [currentChecked, setCurrentChecked] = useState(checked || '');

  useEffect(() => {
    setCurrentChecked(checked);
  }, [checked]);

  const handleChange = () => {
    if (disabled) {
      return;
    }
    setCurrentChecked(!currentChecked);
    onChange(!currentChecked);
  };

  const classList = ['checkbox'];
  const componentStyle = { ...style };

  const labelClassList = ['checkbox__label'];

  const checkerClassList = ['checkbox__checker'];

  const tickClassList = [];
  const tickStyle = {};

  // currentChecked
  tickClassList.push(
    currentChecked ? 'checkbox__checker-checked-tick checkbox__checker-checked-tick--shown' : null
  );

  // disabled
  labelClassList.push(disabled ? 'checkbox__label--disabled' : null);

  // indeterminate
  tickClassList.push(
    indeterminate
      ? 'checkbox__checker-checked-indeterminate checkbox__checker-checked-indeterminate--shown'
      : null
  );

  // color
  checkerClassList.push(currentChecked || indeterminate ? `bg--${color} border--${color}` : null);
  tickStyle.borderColor = getAltHexColor(color);

  // fontColor
  classList.push(`text--${fontColor}`);

  // custom className
  classList.push(className);

  return (
    <div
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <div
        className={labelClassList.filter(c => c && !c.includes('null')).join(' ')}
        onClick={handleChange}
      >
        <span className="checkbox__wrapper" aria-disabled={disabled}>
          <input
            id={id || undefined}
            name={name || undefined}
            type="checkbox"
            tabIndex="0"
            disabled={disabled}
            defaultChecked={currentChecked}
          />
          <div className={checkerClassList.filter(c => c && !c.includes('null')).join(' ')}>
            <span
              className={tickClassList.filter(c => c && !c.includes('null')).join(' ')}
              style={tickStyle}
            />
          </div>
        </span>
        {label}
      </div>
    </div>
  );
};

Object.assign(Checkbox, generateComponentProps(componentData));

export default Checkbox;
