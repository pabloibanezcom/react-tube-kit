// import { mount, shallow } from 'enzyme';
// import React from 'react';
// import ReactPaginate from 'react-paginate';
// import Pagination from './pagination';

// const mockPagination = {
//   page: 1,
//   pages: 10
// };

// describe('<Pagination />', () => {
//   let wrapper;

//   it('renders <Pagination /> component', () => {
//     wrapper = shallow(<Pagination pagination={mockPagination} />);
//     expect(wrapper.find(ReactPaginate)).toHaveLength(1);
//     expect(wrapper.find(ReactPaginate).prop('pageCount')).toEqual(mockPagination.pages);
//     expect(wrapper.find(ReactPaginate).prop('forcePage')).toEqual(mockPagination.page - 1);
//     expect(wrapper.find(ReactPaginate).prop('containerClassName')).toMatch('pagination');
//     expect(wrapper).toMatchSnapshot();
//   });

//   describe('custom class', () => {
//     it('applies custom class when className is set', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} className="my-class" />);
//       expect(wrapper.find('.text-center.my-class')).toHaveLength(1);
//     });
//   });

//   describe('pageRangeDisplayed', () => {
//     it('applies pageRangeDisplayed to pagination', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} />);
//       expect(wrapper.find(ReactPaginate).prop('pageRangeDisplayed')).toEqual(4);

//       wrapper = shallow(<Pagination pagination={mockPagination} pageRangeDisplayed={3} />);
//       expect(wrapper.find(ReactPaginate).prop('pageRangeDisplayed')).toEqual(3);
//     });
//   });

//   describe('color & fontColor', () => {
//     it('applies color class when color is set as application as color string', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} />);
//       expect(wrapper.find('.text-center.bg--primary')).toHaveLength(1);

//       wrapper = shallow(<Pagination pagination={mockPagination} color="secondary" />);
//       expect(wrapper.find('.text-center.bg--secondary')).toHaveLength(1);
//     });

//     it('applies color style when color is set as application as color hex', () => {
//       wrapper = shallow(
//         <Pagination pagination={mockPagination} color="#1aaa55" fontColor="#222222" />
//       );
//       expect(wrapper.find('.text-center').get(0).props.style).toHaveProperty(
//         'backgroundColor',
//         '#1aaa55'
//       );
//       expect(wrapper.find('.text-center').get(0).props.style).toHaveProperty('color', '#222222');
//     });
//   });

//   describe('hoverColor', () => {
//     it('applies hoverColor class when hoverColor is set as application as color string', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} />);
//       expect(wrapper.find(ReactPaginate).prop('pageLinkClassName')).toMatch('bg-hover--secondary');
//       expect(wrapper.find(ReactPaginate).prop('previousLinkClassName')).toMatch(
//         'bg-hover--secondary'
//       );
//       expect(wrapper.find(ReactPaginate).prop('nextLinkClassName')).toMatch('bg-hover--secondary');

//       wrapper = shallow(<Pagination pagination={mockPagination} hoverColor="alternative" />);
//       expect(wrapper.find(ReactPaginate).prop('pageLinkClassName')).toMatch(
//         'bg-hover--alternative'
//       );
//       expect(wrapper.find(ReactPaginate).prop('previousLinkClassName')).toMatch(
//         'bg-hover--alternative'
//       );
//       expect(wrapper.find(ReactPaginate).prop('nextLinkClassName')).toMatch(
//         'bg-hover--alternative'
//       );
//     });
//   });

//   describe('selectedColor', () => {
//     it('applies selectedColor class when selectedColor is set as application as color string', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} selectedColor="alternative" />);
//       expect(wrapper.find(ReactPaginate).prop('activeLinkClassName')).toMatch('bg--alternative');
//     });

//     it('applies hoverColor class when selectedColor is not set', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} />);
//       expect(wrapper.find(ReactPaginate).prop('activeLinkClassName')).toMatch('bg--secondary');
//     });
//   });

//   describe('size', () => {
//     it('applies right size class when size is set', () => {
//       wrapper = shallow(<Pagination pagination={mockPagination} />);
//       expect(wrapper.find(ReactPaginate).prop('containerClassName')).toMatch('pagination--md');

//       wrapper = shallow(<Pagination pagination={mockPagination} size="sm" />);
//       expect(wrapper.find(ReactPaginate).prop('containerClassName')).toMatch('pagination--sm');

//       wrapper = shallow(<Pagination pagination={mockPagination} size="lg" />);
//       expect(wrapper.find(ReactPaginate).prop('containerClassName')).toMatch('pagination--lg');
//     });
//   });

//   describe('onPageChange', () => {
//     it('invokes onPageChange when page is changed', () => {
//       const pageChange = jest.fn();
//       wrapper = mount(<Pagination pagination={mockPagination} onPageChange={pageChange} />);
//       wrapper
//         .find('.pagination a')
//         .at(3)
//         .simulate('click');
//       expect(pageChange).toHaveBeenCalledWith(3);
//     });
//   });
// });
