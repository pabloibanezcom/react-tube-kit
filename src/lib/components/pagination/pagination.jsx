import React from 'react';
import ReactPaginate from 'react-paginate';
import { generateComponentProps } from '../../util/component';
import componentData from './pagination.data.json';

const Pagination = ({
  className,
  color,
  fontColor,
  hoverColor,
  id,
  name,
  pagination,
  pageRangeDisplayed,
  selectedColor,
  size,
  style,
  onPageChange
}) => {
  const handlePageChange = evt => {
    onPageChange(evt.selected + 1);
  };

  const classList = ['text-center'];
  const componentStyle = { ...style };

  const buttonClassList = ['pagination__page-link'];
  const selectedButtonClassList = [];

  // color
  if (color && color.startsWith('#')) {
    componentStyle.backgroundColor = color;
  } else if (color) {
    classList.push(`bg--${color}`);
  }

  // fontColor
  if (fontColor && fontColor.startsWith('#')) {
    componentStyle.color = fontColor;
  } else if (fontColor) {
    classList.push(`text--${fontColor}`);
  }

  // hoverColor
  if (hoverColor) {
    buttonClassList.push(`bg-hover--${hoverColor}`);
  }

  // selectedColor
  if (selectedColor) {
    selectedButtonClassList.push(`bg--${selectedColor}`);
  } else {
    selectedButtonClassList.push(`bg--${hoverColor}`);
  }

  // custom className
  classList.push(className);

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      <nav className="d-inline-block" aria-label="Page navigation">
        {pagination ? (
          <ReactPaginate
            pageCount={pagination.pages}
            pageRangeDisplayed={pageRangeDisplayed}
            marginPagesDisplayed={1}
            forcePage={pagination.page - 1}
            onPageChange={handlePageChange}
            containerClassName={`pagination pagination--${size}`}
            activeLinkClassName={selectedButtonClassList
              .filter(c => c && !c.includes('null'))
              .join(' ')}
            pageLinkClassName={buttonClassList.filter(c => c && !c.includes('null')).join(' ')}
            previousLinkClassName={buttonClassList.filter(c => c && !c.includes('null')).join(' ')}
            nextLinkClassName={buttonClassList.filter(c => c && !c.includes('null')).join(' ')}
            previousLabel="«"
            nextLabel="»"
          />
        ) : null}
      </nav>
    </div>
  );
};

Object.assign(Pagination, generateComponentProps(componentData));

export default Pagination;
