import { shallow } from 'enzyme';
import React from 'react';
import Badge from './badge';

describe('<Badge />', () => {
  let wrapper;

  it('renders <Badge /> component', () => {
    wrapper = shallow(<Badge>Fake badge text</Badge>);
    expect(wrapper.text()).toBe('Fake badge text');
    expect(wrapper).toMatchSnapshot();
  });

  describe('fullWidth', () => {
    it('applies "w-100" class when fullWidth is set', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.w-100')).toHaveLength(0);

      wrapper = shallow(<Badge fullWidth>Fake badge text</Badge>);
      expect(wrapper.find('.w-100')).toHaveLength(1);
    });
  });

  describe('border', () => {
    it('applies "badge--with-border" class when border is set', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.badge--with-border')).toHaveLength(0);

      wrapper = shallow(<Badge border>Fake badge text</Badge>);
      expect(wrapper.find('.badge--with-border')).toHaveLength(1);
    });
  });

  describe('outline', () => {
    it('applies "border" class when outline is set', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.border')).toHaveLength(0);

      wrapper = shallow(<Badge outline>Fake badge text</Badge>);
      expect(wrapper.find('.border')).toHaveLength(1);
    });
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<Badge className="my-class">Fake badge text</Badge>);
      expect(wrapper.find('.badge.my-class')).toHaveLength(1);
    });
  });

  describe('size', () => {
    it('applies right size class when size is set', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.badge--md')).toHaveLength(1);

      wrapper = shallow(<Badge size="sm">Fake badge text</Badge>);
      expect(wrapper.find('.badge--sm')).toHaveLength(1);
      expect(wrapper.find('.badge--md')).toHaveLength(0);

      wrapper = shallow(<Badge size="lg">Fake badge text</Badge>);
      expect(wrapper.find('.badge--lg')).toHaveLength(1);
      expect(wrapper.find('.badge--md')).toHaveLength(0);
    });
  });

  describe('weight', () => {
    it('applies right weight class when weight is set', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(1);

      wrapper = shallow(<Badge weight="light">Fake badge text</Badge>);
      expect(wrapper.find('.font-weight-light')).toHaveLength(1);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(0);

      wrapper = shallow(<Badge weight="bold">Fake badge text</Badge>);
      expect(wrapper.find('.font-weight-bold')).toHaveLength(1);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(0);
    });
  });

  describe('color & fontColor', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<Badge>Fake badge text</Badge>);
      expect(wrapper.find('.bg--primary')).toHaveLength(1);

      wrapper = shallow(<Badge color="secondary">Fake badge text</Badge>);
      expect(wrapper.find('.bg--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(
        <Badge color="#1aaa55" fontColor="#222222">
          Fake badge text
        </Badge>
      );
      expect(wrapper.get(0).props.style).toHaveProperty('backgroundColor', '#1aaa55');
      expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
    });
  });
});
