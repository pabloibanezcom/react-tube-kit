/* eslint-disable no-nested-ternary */
import React from 'react';
import { generateComponentProps } from '../../util/component';
import componentData from './badge.data.json';

const Badge = ({
  border,
  children,
  className,
  color,
  fontColor,
  fullWidth,
  id,
  name,
  outline,
  size,
  style,
  weight
}) => {
  const classList = ['badge'];
  const componentStyle = { ...style };

  // fullWidth
  classList.push(fullWidth ? 'w-100' : null);

  // border
  classList.push(border ? 'badge--with-border' : null);

  // size
  classList.push(`badge--${size}`);

  // weight
  classList.push(`font-weight-${weight}`);

  // color
  if (color && color.startsWith('#')) {
    if (!outline) {
      componentStyle.backgroundColor = color;
    } else {
      componentStyle.borderColor = color;
      componentStyle.color = color;
    }
  } else if (!outline) {
    classList.push(`bg--${color}`);
  } else {
    classList.push(`border--${color}`);
    classList.push(`text--${color}`);
  }

  // fontColor
  if (!outline) {
    if (fontColor && fontColor.startsWith('#')) {
      componentStyle.color = fontColor;
    } else if (fontColor) {
      classList.push(`text--${fontColor}`);
    }
  }

  // outline
  classList.push(outline ? `border` : null);

  // custom className
  classList.push(className);

  return (
    <span
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      {children}
    </span>
  );
};

Object.assign(Badge, generateComponentProps(componentData));

export default Badge;
