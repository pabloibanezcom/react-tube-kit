/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';
import { generateComponentProps } from '../../util/component';
import ButtonLink from '../button-link/button-link';
import componentData from './breadcrumb.data.json';

const Breadcrumb = ({
  activeColor,
  className,
  id,
  items,
  linkColor,
  linkHoverColor,
  name,
  size,
  style
}) => {
  const classList = ['breadcrumb', 'bg-transparent', 'p-0'];
  const componentStyle = { ...style };

  const activeClassList = ['font-weight-normal'];
  const activeStyle = {};

  // size
  classList.push(`breadcrumb--${size}`);

  // active color
  activeClassList.push(`text-${activeColor}`);

  // custom className
  classList.push(className);

  const renderItem = item => {
    const itemLink = () => (
      <li className="breadcrumb-item">
        <ButtonLink
          type="link"
          color={linkColor}
          hoverColor={linkHoverColor}
          size={size}
          to={item.url}
        >
          {item.text}
        </ButtonLink>
      </li>
    );

    const itemActive = () => (
      <li
        className={`breadcrumb-item active ${activeClassList
          .filter(c => c && !c.includes('null'))
          .join(' ')}`}
        style={activeStyle}
        aria-current="page"
      >
        {item.text}
      </li>
    );

    return item.url ? itemLink() : itemActive();
  };

  return (
    <nav aria-label="breadcrumb" id={id || undefined} name={name || undefined}>
      <ol
        className={classList.filter(c => c && !c.includes('null')).join(' ')}
        style={componentStyle}
      >
        {items.map(item => (
          <Fragment key={item.text}>{renderItem(item)}</Fragment>
        ))}
      </ol>
    </nav>
  );
};

Object.assign(Breadcrumb, generateComponentProps(componentData));

export default Breadcrumb;
