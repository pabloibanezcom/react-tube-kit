/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { generateComponentProps } from '../../util/component';
import Badge from '../badge/badge';
import componentData from './country-label.data.json';

const CountryLabel = ({
  badge,
  className,
  color,
  country,
  customName,
  id,
  name,
  size,
  style,
  weight
}) => {
  const classList = ['country-label'];
  const componentStyle = { ...style };

  // color
  if (!badge) {
    if (color && color.startsWith('#')) {
      componentStyle.color = color;
    } else if (color) {
      classList.push(`text--${color}`);
    }
  }

  // size
  classList.push(`country-label--${size}`);

  // weight
  classList.push(weight ? `font-weight-${weight}` : null);

  // custom className
  classList.push(className);

  const basicLabel = (
    <div className="d-flex align-items-center">
      <div className={`country-flag flag-${country.code.toLowerCase()}`} />
      <span className="country-name">{customName || country.name}</span>
    </div>
  );

  return (
    <div
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
    >
      {!badge ? (
        basicLabel
      ) : (
        <Badge {...badge} color={color || undefined} size={size || undefined}>
          {basicLabel}
        </Badge>
      )}
    </div>
  );
};

Object.assign(CountryLabel, generateComponentProps(componentData));

export default CountryLabel;
