import { shallow } from 'enzyme';
import React from 'react';
import Badge from '../badge/badge';
import CountryLabel from './country-label';

describe('<CountryLabel />', () => {
  let wrapper;

  const country = { code: 'GB', name: 'United Kingdom' };

  it('renders <CountryLabel /> component', () => {
    wrapper = shallow(<CountryLabel country={country} />);
    expect(wrapper.find('.country-flag.flag-gb')).toHaveLength(1);
    expect(wrapper.find('.country-name').text()).toBe('United Kingdom');
    expect(wrapper).toMatchSnapshot();
  });

  describe('customName', () => {
    it('uses custonName instead of country name when defined', () => {
      wrapper = shallow(<CountryLabel country={country} customName="London" />);
      expect(wrapper.find('.country-name').text()).toBe('London');
    });
  });

  describe('custom class', () => {
    it('applies custom class when className is set', () => {
      wrapper = shallow(<CountryLabel country={country} className="my-class" />);
      expect(wrapper.find('.country-label.my-class')).toHaveLength(1);
    });
  });

  describe('size', () => {
    it('applies right size class when size is set', () => {
      wrapper = shallow(<CountryLabel country={country} />);
      expect(wrapper.find('.country-label--md')).toHaveLength(1);

      wrapper = shallow(<CountryLabel size="sm" country={country} />);
      expect(wrapper.find('.country-label--sm')).toHaveLength(1);
      expect(wrapper.find('.country-label--md')).toHaveLength(0);

      wrapper = shallow(<CountryLabel size="lg" country={country} />);
      expect(wrapper.find('.country-label--lg')).toHaveLength(1);
      expect(wrapper.find('.country-label--md')).toHaveLength(0);
    });
  });

  describe('weight', () => {
    it('applies right weight class when weight is set', () => {
      wrapper = shallow(<CountryLabel country={country} />);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(1);

      wrapper = shallow(<CountryLabel weight="light" country={country} />);
      expect(wrapper.find('.font-weight-light')).toHaveLength(1);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(0);

      wrapper = shallow(<CountryLabel weight="bold" country={country} />);
      expect(wrapper.find('.font-weight-bold')).toHaveLength(1);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(0);
    });
  });

  describe('color', () => {
    it('applies color class when color is set as application as color string', () => {
      wrapper = shallow(<CountryLabel country={country} color="secondary" />);
      expect(wrapper.find('.text--secondary')).toHaveLength(1);
    });

    it('applies color style when color is set as application as color hex', () => {
      wrapper = shallow(<CountryLabel country={country} color="#222222" />);
      expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('badge', () => {
    it('doesn not use Badge when is not defined', () => {
      wrapper = shallow(<CountryLabel country={country} />);
      expect(wrapper.find(Badge)).toHaveLength(0);
    });

    it('shows country label wrapped in a badge when badge is set to true', () => {
      wrapper = shallow(<CountryLabel country={country} badge />);
      expect(wrapper.find(Badge)).toHaveLength(1);
    });

    it('pass color as Badge prop when color is defined', () => {
      wrapper = shallow(<CountryLabel country={country} badge color="primary" />);
      expect(wrapper.find(Badge).prop('color')).toEqual('primary');
    });

    it('pass size as Badge prop when size is defined', () => {
      wrapper = shallow(<CountryLabel country={country} badge size="sm" />);
      expect(wrapper.find(Badge).prop('size')).toEqual('sm');
    });

    it('spread properties into Badge when Badge is an object', () => {
      wrapper = shallow(
        <CountryLabel
          country={country}
          badge={{ fullWidth: true, className: 'custom-badge-classname' }}
        />
      );
      expect(wrapper.find(Badge).prop('fullWidth')).toEqual(true);
      expect(wrapper.find(Badge).prop('className')).toEqual('custom-badge-classname');
    });
  });
});
