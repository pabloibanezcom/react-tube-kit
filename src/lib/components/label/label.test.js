import { shallow } from 'enzyme';
import React from 'react';
import Label from './label';

describe('<Label />', () => {
  let wrapper;

  it('renders <Label /> component', () => {
    wrapper = shallow(<Label>Fake label text</Label>);
    expect(wrapper.find('label').text()).toBe('Fake label text');
    expect(wrapper).toMatchSnapshot();
  });

  describe('color', () => {
    it('applies color class when color is set as color string', () => {
      wrapper = shallow(<Label color="primary">Fake label text</Label>);
      expect(wrapper.find('.text-primary')).toHaveLength(1);
    });

    it('applies color style when color is set as color hex', () => {
      wrapper = shallow(<Label color="#222222">Fake label text</Label>);
      expect(wrapper.get(0).props.style).toHaveProperty('color', '#222222');
    });
  });

  describe('forhtml', () => {
    it('applies "forhtml" attr when forhtml is set', () => {
      wrapper = shallow(<Label forhtml="fakename">Fake label text</Label>);
      expect(wrapper.get(0).props.forhtml).toBe('fakename');
    });
  });

  describe('weight', () => {
    it('applies right weight class when weight is set', () => {
      wrapper = shallow(<Label>Fake label text</Label>);
      expect(wrapper.find('.font-weight-light')).toHaveLength(0);
      expect(wrapper.find('.font-weight-normal')).toHaveLength(0);
      expect(wrapper.find('.font-weight-bold')).toHaveLength(0);

      wrapper = shallow(<Label weight="light">Fake label text</Label>);
      expect(wrapper.find('.font-weight-light')).toHaveLength(1);

      wrapper = shallow(<Label weight="light">Fake label text</Label>);
      expect(wrapper.find('.font-weight-light')).toHaveLength(1);

      wrapper = shallow(<Label weight="bold">Fake label text</Label>);
      expect(wrapper.find('.font-weight-bold')).toHaveLength(1);
    });
  });
});
