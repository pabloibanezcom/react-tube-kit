import React from 'react';
import { generateComponentProps } from '../../util/component';
import componentData from './label.data.json';

const Label = ({ children, className, color, forhtml, id, name, style, weight }) => {
  const classList = ['label'];
  const componentStyle = { ...style };

  // color
  if (color && color.startsWith('#')) {
    componentStyle.color = color;
  } else if (color) {
    classList.push(`text-${color}`);
  }

  // weight
  classList.push(weight ? `font-weight-${weight}` : null);

  // custom className
  classList.push(className);

  return (
    <label
      id={id || undefined}
      name={name || undefined}
      className={classList.filter(c => c && !c.includes('null')).join(' ')}
      style={componentStyle}
      forhtml={forhtml}
    >
      {children}
    </label>
  );
};

Object.assign(Label, generateComponentProps(componentData));

export default Label;
