import Add from './svg/add.svg';
import AngleDown from './svg/angle-down.svg';
import AngleLeft from './svg/angle-left.svg';
import AngleRight from './svg/angle-right.svg';
import AngleUp from './svg/angle-up.svg';
import At from './svg/at.svg';
import Atm from './svg/atm.svg';
import Boat from './svg/boat.svg';
import Bus from './svg/bus.svg';
import Calendar from './svg/calendar.svg';
import Cancel from './svg/cancel.svg';
import Car from './svg/car.svg';
import CenterO from './svg/center-o.svg';
import Center from './svg/center.svg';
import Check from './svg/check.svg';
import ChevronDown from './svg/chevron-down.svg';
import ChevronLeft from './svg/chevron-left.svg';
import ChevronRight from './svg/chevron-right.svg';
import ChevronUp from './svg/chevron-up.svg';
import CircleO from './svg/circle-o.svg';
import Circle from './svg/circle.svg';
import City from './svg/city.svg';
import Close from './svg/close.svg';
import Code from './svg/code.svg';
import Connection from './svg/connection.svg';
import Copy from './svg/copy.svg';
import Download from './svg/download.svg';
import Edit from './svg/edit.svg';
import Error from './svg/error.svg';
import Facebook from './svg/facebook.svg';
import Female from './svg/female.svg';
import FileArchiveO from './svg/file-archive-o.svg';
import FileAudioO from './svg/file-audio-o.svg';
import FileExcelO from './svg/file-excel-o.svg';
import FileImageO from './svg/file-image-o.svg';
import FileMovieO from './svg/file-movie-o.svg';
import FileO from './svg/file-o.svg';
import FilePdfO from './svg/file-pdf-o.svg';
import FilePowerpointO from './svg/file-powerpoint-o.svg';
import FileTextO from './svg/file-text-o.svg';
import FileWordO from './svg/file-word-o.svg';
import FlightLand from './svg/flight-land.svg';
import FlightTakeoff from './svg/flight-takeoff.svg';
import Flight from './svg/flight.svg';
import GasStation from './svg/gas-station.svg';
import Health from './svg/health.svg';
import Hospital from './svg/hospital.svg';
import Image from './svg/image.svg';
import Info from './svg/info.svg';
import Key from './svg/key.svg';
import Lines from './svg/lines.svg';
import Login from './svg/login.svg';
import Logout from './svg/logout.svg';
import LongArrowDown from './svg/long-arrow-down.svg';
import LongArrowLeft from './svg/long-arrow-left.svg';
import LongArrowRight from './svg/long-arrow-right.svg';
import LongArrowUp from './svg/long-arrow-up.svg';
import Map from './svg/map.svg';
import Marker from './svg/marker.svg';
import Menu from './svg/menu.svg';
import Nature from './svg/nature.svg';
import Neighborhood from './svg/neighborhood.svg';
import Pharmacy from './svg/pharmacy.svg';
import Play from './svg/play.svg';
import Public from './svg/public.svg';
import Publish from './svg/publish.svg';
import Railway from './svg/railway.svg';
import Remove from './svg/remove.svg';
import Restaurant from './svg/restaurant.svg';
import School from './svg/school.svg';
import ScreenFull from './svg/screen-full.svg';
import Search from './svg/search.svg';
import Settings from './svg/settings.svg';
import Share from './svg/share.svg';
import ShoppingBasket from './svg/shopping-basket.svg';
import ShoppingCart from './svg/shopping-cart.svg';
import SquareO from './svg/square-o.svg';
import Square from './svg/square.svg';
import StoreMall from './svg/store-mall.svg';
import Train from './svg/train.svg';
import Tram from './svg/tram.svg';
import TrashO from './svg/trash-o.svg';
import TubeLogo from './svg/tube-logo.svg';
import Underground from './svg/underground.svg';
import Upload from './svg/upload.svg';
import User from './svg/user.svg';
import Warning from './svg/warning.svg';
import ZoomIn from './svg/zoom-in.svg';
import ZoomOut from './svg/zoom-out.svg';

export default {
  add: Add,
  'angle-down': AngleDown,
  'angle-left': AngleLeft,
  'angle-right': AngleRight,
  'angle-up': AngleUp,
  at: At,
  atm: Atm,
  boat: Boat,
  bus: Bus,
  calendar: Calendar,
  cancel: Cancel,
  car: Car,
  'center-o': CenterO,
  center: Center,
  check: Check,
  'chevron-down': ChevronDown,
  'chevron-left': ChevronLeft,
  'chevron-right': ChevronRight,
  'chevron-up': ChevronUp,
  'circle-o': CircleO,
  circle: Circle,
  city: City,
  close: Close,
  code: Code,
  connection: Connection,
  copy: Copy,
  download: Download,
  edit: Edit,
  error: Error,
  facebook: Facebook,
  female: Female,
  'file-archive-o': FileArchiveO,
  'file-audio-o': FileAudioO,
  'file-excel-o': FileExcelO,
  'file-image-o': FileImageO,
  'file-movie-o': FileMovieO,
  'file-o': FileO,
  'file-pdf-o': FilePdfO,
  'file-powerpoint-o': FilePowerpointO,
  'file-text-o': FileTextO,
  'file-word-o': FileWordO,
  'flight-land': FlightLand,
  'flight-takeoff': FlightTakeoff,
  flight: Flight,
  'gas-station': GasStation,
  health: Health,
  hospital: Hospital,
  image: Image,
  info: Info,
  key: Key,
  lines: Lines,
  login: Login,
  logout: Logout,
  'long-arrow-down': LongArrowDown,
  'long-arrow-left': LongArrowLeft,
  'long-arrow-right': LongArrowRight,
  'long-arrow-up': LongArrowUp,
  map: Map,
  marker: Marker,
  menu: Menu,
  nature: Nature,
  neighborhood: Neighborhood,
  pharmacy: Pharmacy,
  play: Play,
  public: Public,
  publish: Publish,
  railway: Railway,
  remove: Remove,
  restaurant: Restaurant,
  school: School,
  'screen-full': ScreenFull,
  search: Search,
  settings: Settings,
  share: Share,
  'shopping-basket': ShoppingBasket,
  'shopping-cart': ShoppingCart,
  'square-o': SquareO,
  square: Square,
  'store-mall': StoreMall,
  train: Train,
  tram: Tram,
  'trash-o': TrashO,
  'tube-logo': TubeLogo,
  underground: Underground,
  upload: Upload,
  user: User,
  warning: Warning,
  'zoom-in': ZoomIn,
  'zoom-out': ZoomOut
};
