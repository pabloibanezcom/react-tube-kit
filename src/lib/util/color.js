import defaultColors from './colors.json';

export const getContrastColor = hexcolor => {
  hexcolor = hexcolor.replace('#', '');
  const r = parseInt(hexcolor.substr(0, 2), 16);
  const g = parseInt(hexcolor.substr(2, 2), 16);
  const b = parseInt(hexcolor.substr(4, 2), 16);
  const yiq = (r * 299 + g * 587 + b * 114) / 1000;
  return yiq >= 128 ? '#000000' : '#ffffff';
};

export const getHexColor = color => {
  return getColors()[color] ? getColors()[color].base : null;
};

export const getAltHexColor = color => {
  return getColors()[color] ? getColors()[color].alt : null;
};

const getCustomizeColors = () => {
  const customVars = localStorage.getItem('customVars');
  return customVars ? JSON.parse(customVars).colors : {};
};

const getColors = () => {
  return Object.assign({}, defaultColors, getCustomizeColors());
};
