export const isSize = (sizeStr, currentSize) => {
  if (!sizeStr) {
    return false;
  }
  const sizesArray = sizeStr.split('-').map(str => getNumberFromSizeStr(str));
  const currentSizeNumber = getNumberFromSizeStr(currentSize || getWindowSize());
  if (sizesArray.length === 1 && sizesArray[0] === currentSizeNumber) {
    return true;
  }
  if (sizesArray[0] === '') {
    return currentSizeNumber <= sizesArray[1];
  }
  if (sizesArray[1] === '') {
    return currentSizeNumber >= sizesArray[0];
  }
  return currentSizeNumber >= sizesArray[0] && currentSizeNumber <= sizesArray[1];
};

export const getNumberFromSizeStr = sizeStr => {
  switch (sizeStr) {
    case 'xs':
      return 1;
    case 'sm':
      return 2;
    case 'md':
      return 3;
    case 'lg':
      return 4;
    case 'xl':
      return 5;
    default:
      return sizeStr;
  }
};

export const getWindowSize = () => {
  const { outerWidth: width } = window;
  if (width >= 1200) {
    return 'xl';
  }
  if (width >= 992) {
    return 'lg';
  }
  if (width >= 768) {
    return 'md';
  }
  if (width >= 576) {
    return 'sm';
  }
  if (width < 576) {
    return 'xs';
  }
  return null;
};

export const absolutePosition = el => {
  let found;
  let left = 0;
  let top = 0;
  let width = 0;
  let height = 0;
  let offsetBase = absolutePosition.offsetBase;
  if (!offsetBase && document.body) {
    // eslint-disable-next-line no-multi-assign
    offsetBase = absolutePosition.offsetBase = document.createElement('div');
    offsetBase.style.cssText = 'position:absolute;left:0;top:0';
    document.body.appendChild(offsetBase);
  }
  if (el && el.ownerDocument === document && 'getBoundingClientRect' in el && offsetBase) {
    const boundingRect = el.getBoundingClientRect();
    const baseRect = offsetBase.getBoundingClientRect();
    found = true;
    left = boundingRect.left - baseRect.left;
    top = boundingRect.top - baseRect.top;
    width = boundingRect.right - boundingRect.left;
    height = boundingRect.bottom - boundingRect.top;
  }
  return {
    found,
    left,
    top,
    width,
    height,
    right: left + width,
    bottom: top + height
  };
};
