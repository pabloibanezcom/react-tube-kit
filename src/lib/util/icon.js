import icons from '../assets/icons/icons.json';
import { getHexColor } from './color';

export const getColoredIconUrl = (name, fillColor, strokeColor, rotation) => {
  let svgStr = icons[name];
  if (!svgStr) {
    svgStr = icons.marker;
  }
  return `data:image/svg+xml;charset=UTF-8;base64,${btoa(
    svgStr.replace(
      'width=',
      `fill="${getHexColor(fillColor)}" stroke="${getHexColor(strokeColor)}" ${
        rotation ? `transform="rotate(${rotation})"` : ''
      } width=`
    )
  )}`;
};
