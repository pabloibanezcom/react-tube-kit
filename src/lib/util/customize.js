export const setCustomVariables = customVars => {
  localStorage.setItem('customVars', JSON.stringify(customVars));
};
