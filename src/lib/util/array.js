export const chunkArray = (arr, size) => {
  const result = [];
  let index = 0;
  while (index < arr.length) {
    result.push(arr.slice(index, size + index));
    index += size;
  }
  return result;
};

export const randomPick = arr => {
  return arr[Math.floor(Math.random() * arr.length)];
};

export const randomPickAndRemove = arr => {
  const pickIndex = Math.floor(Math.random() * arr.length);
  const pickEl = arr[pickIndex];
  arr.splice(pickIndex, 1);
  return pickEl;
};

export const removeWithIds = (arr, idArray) => {
  return arr.filter(el => !idArray.includes(el.id));
};
