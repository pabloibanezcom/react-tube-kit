import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import environment from './environment.json';
import './index.scss';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

const loadGoogleMaps = () => {
  const script = document.createElement(`script`);
  script.src = `https://maps.googleapis.com/maps/api/js?key=${environment.GOOGLE_MAPS_API_KEY}&libraries=places`;
  document.head.append(script);
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
loadGoogleMaps();
