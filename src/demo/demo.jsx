import ButtonLink from 'lib/ButtonLink';
import { LayoutWrapper } from 'lib/Layout';
import Panel from 'lib/Panel';
import React from 'react';
import { withRouter } from 'react-router-dom';
import logo from './assets/img/logo192.png';
import components from './components.json';
import menuElements from './demo-menu.data.json';
import releases from './releases.json';
import routes from './routes';

menuElements.find(mEl => mEl.url === 'components').children = components.filter(
  el => !el.namespace
);

const Demo = ({ history }) => {
  const rightContent = (
    <>
      <div className="d-none d-md-block">
        <ButtonLink type="link" to="/releases" color="white" hoverColor="primary" className="mr-4">
          v.{releases[0].version}
        </ButtonLink>
      </div>
      <div className="d-none d-md-block">
        <ButtonLink
          type="link"
          color="white"
          hoverColor="primary"
          href="https://gitlab.com/pabloibanezcom/react-tube-kit"
          newPage
        >
          GitLab
        </ButtonLink>
      </div>
    </>
  );

  const footer = (
    <Panel color="secondary" contentClassName="py-3 text-center">
      Development & Design by Pablo Ibanez - 2020
    </Panel>
  );

  const getLayoutWrapperClass = () => {
    if (history.location.pathname.includes('layout-full-screen')) {
      return 'w-100 layout-full-screen';
    }
    return 'container container-full pt-7 pb-7';
  };

  return (
    <LayoutWrapper
      currentPage={history.location.pathname.replace('/demo/', '')}
      className="demo"
      topNavbar={{
        title: 'React Tube Kit',
        titleDisplay: null,
        logo,
        animated: true,
        rightContent
      }}
      sideNavbar={{ elements: menuElements }}
      footer={footer}
    >
      <div className={getLayoutWrapperClass()}>{routes}</div>
    </LayoutWrapper>
  );
};

export default withRouter(Demo);
