import ButtonLink from 'lib/ButtonLink';
import React from 'react';

const Overview = () => (
  <div className="row">
    <div className="col-lg-12">
      <h1 className="right-line mb-4">Overview</h1>
      <ButtonLink type="link" color="secondary" hoverColor="primary" to="/layout/playground">
        Playground
      </ButtonLink>
    </div>
  </div>
);

export default Overview;
