import componentsData from 'lib/components/data';
import { mergeCommonProps } from 'lib/util/component';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import ComponentPage from '../../components/component-page/component-page';
import menuData from '../../demo-menu.data.json';
// import examples from './examples';
import Overview from './overview/overview';

const routes = path => (
  <Switch>
    <Route path={`${path}/overview`} component={Overview} />
    {menuData
      .find(mEl => mEl.url === 'layout')
      .children.filter(cEl => cEl.component)
      .map(cEl => (
        <Route
          key={cEl.url}
          path={`${path}/${cEl.url}`}
          component={() => (
            <ComponentPage
              componentData={mergeCommonProps(componentsData[cEl.component])}
              examples={[]}
              // examples={[examples[cEl.component]]}
            />
          )}
        />
      ))}
    <Redirect to={`${path}/overview`} />
  </Switch>
);

export default routes;
