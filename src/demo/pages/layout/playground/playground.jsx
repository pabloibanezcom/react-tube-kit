import CollapsibleHeading from 'lib/CollapsibleHeading';
import { LayoutWrapper } from 'lib/Layout';
import React, { useState } from 'react';
import logo from '../../../assets/img/logo192.png';
import PlaygroundComponentConfigurator from '../../../components/playground-component-configurator/playground-component-configurator';
import bottomNavbarElements from './bottom-navbar-elements.json';
import sideNavbarElements from './side-navbar-elements.json';

const defaultTopNavbar = {
  title: 'React Tube Kit',
  titleDisplay: null,
  logo,
  animated: true
};

const defaultSideNavbar = { elements: sideNavbarElements };

const defaultBottomNavbar = { elements: bottomNavbarElements };

const LayoutPlayground = ({ history }) => {
  const [topNavbar, setTopNavbar] = useState(defaultTopNavbar);
  const [sideNavbar, setSideNavbar] = useState(defaultSideNavbar);
  const [bottomNavbar, setBottomNavbar] = useState(defaultBottomNavbar);

  const handleTopNavbarChange = data => {
    setTopNavbar(Object.assign({}, topNavbar, data));
  };

  const handleSideNavbarChange = data => {
    setSideNavbar(Object.assign({}, sideNavbar, data));
  };

  const handleBottomNavbarChange = data => {
    setBottomNavbar(Object.assign({}, bottomNavbar, data));
  };

  return (
    <LayoutWrapper
      currentPage={history.location.pathname}
      topNavbar={topNavbar}
      sideNavbar={sideNavbar}
      bottomNavbar={bottomNavbar}
    >
      <div className="container container-full pt-7 pb-7">
        <div className="row">
          <div className="col-lg-12">
            <CollapsibleHeading headingLevel="2" headingContent="Top Navbar" className="mb-4">
              <PlaygroundComponentConfigurator
                componentName="TopNavbar"
                custom={{ title: 'React Tube Kit' }}
                resetText="Reset Top Navbar"
                onChange={handleTopNavbarChange}
              />
            </CollapsibleHeading>

            <CollapsibleHeading headingLevel="2" headingContent="Side Navbar" className="mb-4 mt-8">
              <PlaygroundComponentConfigurator
                componentName="SideNavbar"
                resetText="Reset Side Navbar"
                onChange={handleSideNavbarChange}
              />
            </CollapsibleHeading>
            <CollapsibleHeading
              headingLevel="2"
              headingContent="Bottom Navbar"
              className="mb-4 mt-8"
            >
              <PlaygroundComponentConfigurator
                componentName="BottomNavbar"
                resetText="Reset Bottom Navbar"
                onChange={handleBottomNavbarChange}
              />
            </CollapsibleHeading>
          </div>
        </div>
      </div>
    </LayoutWrapper>
  );
};

export default LayoutPlayground;
