const html = `<div>
  <TopNavbar
    title="Mock application title"
    scrollHide
  />
</div>`;

const topNavbarScrollHide = {
  id: 'topNavbarScrollHide',
  name: 'Scroll hide',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/top-navbar/scroll-hide' }
  ]
};

export default topNavbarScrollHide;
