const html = `<div>
  <TopNavbar
    title="Mock application title"
    menuButtonPosition="left"
  />
</div>`;

const topNavbarMenuButtonPosition = {
  id: 'topNavbarMenuButtonPosition',
  name: 'Menu button position',
  disableExample: true,
  html,
  actions: [
    {
      text: 'See full screen',
      icon: 'screen-full',
      to: '/components/top-navbar/menu-button-position'
    }
  ]
};

export default topNavbarMenuButtonPosition;
