const html = `<div>
  <TopNavbar
    title="Mock application title"
    color="primary"
  />
</div>`;

const topNavbarColors = {
  id: 'topNavbarColors',
  name: 'Colors',
  disableExample: true,
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/top-navbar/colors' }]
};

export default topNavbarColors;
