const html = `<div>
  <TopNavbar
    title="Mock application title"
  />
</div>`;

const topNavbarRightContent = {
  id: 'topNavbarRightContent',
  name: 'Right content',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/top-navbar/right-content' }
  ]
};

export default topNavbarRightContent;
