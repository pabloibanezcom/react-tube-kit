const html = `<div>
  <TopNavbar
    title="Mock application title"
  />
</div>`;

const topNavbarBasic = {
  id: 'topNavbarBasic',
  name: 'Basic',
  disableExample: true,
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/top-navbar/basic' }]
};

export default topNavbarBasic;
