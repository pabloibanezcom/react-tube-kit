import topNavbarBasic from './top-navbar-basic';
import topNavbarColors from './top-navbar-colors';
import topNavbarMenuButtonPosition from './top-navbar-menu-button-position';
import topNavbarRightContent from './top-navbar-right-content';
import topNavbarScrollHide from './top-navbar-scroll-hide';

export default [
  topNavbarBasic,
  topNavbarRightContent,
  topNavbarMenuButtonPosition,
  topNavbarScrollHide,
  topNavbarColors
];
