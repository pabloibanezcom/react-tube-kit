const html = `<div>
  <BottomNavbar elements={menuElements} />
</div>`;

const bottomNavbarButtonElement = {
  id: 'bottomNavbarButtonElement',
  name: 'Button element',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/bottom-navbar/button-element' }
  ]
};

export default bottomNavbarButtonElement;
