const html = `<div>
  <BottomNavbar scrollHide elements={menuElements} />
</div>`;

const bottomNavbarScrollHide = {
  id: 'bottomNavbarScrollHide',
  name: 'Scroll hide',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/bottom-navbar/scroll-hide' }
  ]
};

export default bottomNavbarScrollHide;
