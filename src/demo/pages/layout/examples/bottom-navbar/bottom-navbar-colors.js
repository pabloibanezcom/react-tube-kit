const html = `<div>
  <BottomNavbar elements={menuElements} color="#60ac3a" fontColor="secondary" iconColor="light" />
</div>`;

const bottomNavbarColors = {
  id: 'bottomNavbarColors',
  name: 'Colors',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/bottom-navbar/colors' }
  ]
};

export default bottomNavbarColors;
