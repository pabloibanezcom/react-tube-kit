import bottomNavbarBasic from './bottom-navbar-basic';
import bottomNavbarButtonElement from './bottom-navbar-button-element';
import bottomNavbarColors from './bottom-navbar-colors';
import bottomNavbarIconsOnly from './bottom-navbar-icons-only';
import bottomNavbarScrollHide from './bottom-navbar-scroll-hide';

export default [
  bottomNavbarBasic,
  bottomNavbarScrollHide,
  bottomNavbarColors,
  bottomNavbarIconsOnly,
  bottomNavbarButtonElement
];
