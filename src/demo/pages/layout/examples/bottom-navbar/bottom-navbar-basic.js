const html = `<div>
  <BottomNavbar elements={menuElements} />
</div>`;

const bottomNavbarBasic = {
  id: 'bottomNavbarBasic',
  name: 'Basic',
  disableExample: true,
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/bottom-navbar/basic' }]
};

export default bottomNavbarBasic;
