const html = `<div>
  <BottomNavbar elements={menuElements} iconsOnly />
</div>`;

const bottomNavbarIconsOnly = {
  id: 'bottomNavbarIconsOnly',
  name: 'Icons only',
  disableExample: true,
  html,
  actions: [
    { text: 'See full screen', icon: 'screen-full', to: '/components/bottom-navbar/icons-only' }
  ]
};

export default bottomNavbarIconsOnly;
