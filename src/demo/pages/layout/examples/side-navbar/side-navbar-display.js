const html = `<div>
  <SideNavbar elements={menuElements} display="full" />
</div>`;

const sideNavbarDisplay = {
  id: 'sideNavbarDisplay',
  name: 'Display',
  disableExample: true,
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/side-navbar/display' }]
};

export default sideNavbarDisplay;
