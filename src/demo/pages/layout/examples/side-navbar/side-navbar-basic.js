const html = `<div>
  <SideNavbar elements={menuElements} />
</div>`;

const sideNavbarBasic = {
  id: 'sideNavbarBasic',
  name: 'Basic',
  disableExample: true,
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/side-navbar/basic' }]
};

export default sideNavbarBasic;
