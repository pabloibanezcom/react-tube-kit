import sideNavbarBasic from './side-navbar-basic';
import sideNavbarDisplay from './side-navbar-display';

export default [sideNavbarBasic, sideNavbarDisplay];
