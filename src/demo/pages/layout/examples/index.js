import BottomNavbar from './bottom-navbar';
import SideNavbar from './side-navbar';
import TopNavbar from './top-navbar';

export default {
  BottomNavbar,
  SideNavbar,
  TopNavbar
};
