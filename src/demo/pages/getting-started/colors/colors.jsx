/* eslint-disable jsx-a11y/control-has-associated-label */
import ColorLabel from 'lib/ColorLabel';
import React from 'react';
import colors from './colors.data.json';

const colorsTable = () => (
  <div style={{ overflowX: 'auto' }}>
    <div style={{ display: 'table-header-group' }}>
      <table>
        <thead>
          <tr>
            <th>Color</th>
            <th />
            <th>Alt color</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {colors.map(c => (
            <tr key={c.color}>
              <td>{c.color}</td>
              <td>
                <ColorLabel color={c.colorCode} />
              </td>
              <td>{c.colorAlt}</td>
              <td>
                <ColorLabel color={c.colorAltCode} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
);

const Colors = () => (
  <div className="row">
    <div className="col-lg-12">
      <h1 className="right-line mb-4">Colors</h1>
      <div className="row justify-content-end">
        <div className="col-lg-9">
          <div className="mt-8">{colorsTable()}</div>
        </div>
        <div className="col-lg-3" />
      </div>
    </div>
  </div>
);

export default Colors;
