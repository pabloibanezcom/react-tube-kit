import ButtonLink from 'lib/ButtonLink';
import { SideNavbar, TopNavbar } from 'lib/Layout';
import React from 'react';

const menuElements = [
  {
    name: 'Spain',
    icon: 'shopping-cart',
    url: 'spain',
    children: [
      {
        name: 'Madrid'
      },
      {
        name: 'Barcelona'
      },
      {
        name: 'Zaragoza'
      }
    ]
  },
  {
    name: 'Germany',
    icon: 'marker'
  },
  {
    name: 'United Kingdom',
    icon: 'user',
    url: 'components',
    children: [
      {
        name: 'London',
        url: 'side-navbar/basic'
      },
      {
        name: 'Birmingham'
      },
      {
        name: 'Manchester'
      },
      {
        name: 'Liverpool'
      }
    ]
  },
  {
    name: 'France',
    icon: 'city'
  }
];

const SideNavbarBasic = ({ history }) => {
  return (
    <>
      <TopNavbar title="Mock application title" />
      <div className="center-screen p-2">
        <div className="mb-4">
          <ButtonLink to="/components/side-navbar">Go back to Side Navbar page</ButtonLink>
        </div>
      </div>
      <div className="layout__main">
        <SideNavbar
          elements={menuElements}
          currentPage={history.location.pathname.replace('/demo/', '')}
        />
      </div>
    </>
  );
};

export default SideNavbarBasic;
