import ButtonLink from 'lib/ButtonLink';
import { SideNavbar, TopNavbar } from 'lib/Layout';
import React, { useState } from 'react';

const menuElements = [
  {
    name: 'Spain',
    icon: 'shopping-cart',
    url: 'spain',
    children: [
      {
        name: 'Madrid'
      },
      {
        name: 'Barcelona'
      },
      {
        name: 'Zaragoza'
      }
    ]
  },
  {
    name: 'Germany',
    icon: 'marker'
  },
  {
    name: 'United Kingdom',
    icon: 'user',
    url: 'components',
    children: [
      {
        name: 'London',
        url: 'side-navbar/display'
      },
      {
        name: 'Birmingham'
      },
      {
        name: 'Manchester'
      },
      {
        name: 'Liverpool'
      }
    ]
  },
  {
    name: 'France',
    icon: 'city'
  },
  {
    name: 'USA'
  }
];

const SideNavbarDisplay = ({ history }) => {
  const [hidden, setHidden] = useState(false);
  const [collapsed, setCollapsed] = useState(false);

  const handleHidden = () => {
    setHidden(true);
  };

  const handleCollapsed = () => {
    setHidden(false);
    setCollapsed(true);
  };

  const handleFull = () => {
    setHidden(false);
    setCollapsed(false);
  };

  return (
    <>
      <TopNavbar title="Mock application title" />
      <div className="center-screen p-2">
        <div className="mb-4">
          <div className="mb-8">
            <ButtonLink
              outline={!hidden}
              color="secondary"
              hoverColor="primary"
              className="mr-4"
              onClick={handleHidden}
            >
              Hidden
            </ButtonLink>
            <ButtonLink
              outline={!collapsed || hidden}
              color="secondary"
              hoverColor="primary"
              className="mr-4"
              onClick={handleCollapsed}
            >
              Collapsed
            </ButtonLink>
            <ButtonLink
              outline={hidden || collapsed}
              color="secondary"
              hoverColor="primary"
              onClick={handleFull}
            >
              Full
            </ButtonLink>
          </div>
          <ButtonLink to="/components/side-navbar">Go back to Side Navbar page</ButtonLink>
        </div>
      </div>
      <div className="layout__main">
        <SideNavbar
          elements={menuElements}
          hidden={hidden}
          collapsed={collapsed}
          showCollapsedToggle
          currentPage={history.location.pathname.replace('/demo/', '')}
        />
      </div>
    </>
  );
};

export default SideNavbarDisplay;
