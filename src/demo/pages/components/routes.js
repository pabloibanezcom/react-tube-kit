import componentsData from 'lib/components/data';
import { mergeCommonProps } from 'lib/util/component';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import ComponentPage from '../../components/component-page/component-page';
import menuData from '../../demo-menu.data.json';
import examples from './examples';
import MapFullScreen from './map-full-screen/map-full-screen';

const routes = path => (
  <Switch>
    <Route path="/components/map/full-screen" component={MapFullScreen} />
    {menuData
      .find(mEl => mEl.url === 'components')
      .children.filter(cEl => cEl.component)
      .map(cEl => (
        <Route
          key={cEl.url}
          path={`${path}/${cEl.url}`}
          component={() => (
            <ComponentPage
              componentData={mergeCommonProps(componentsData[cEl.component])}
              examples={examples[cEl.component]}
            />
          )}
        />
      ))}
    <Redirect to="/" />
  </Switch>
);

export default routes;
