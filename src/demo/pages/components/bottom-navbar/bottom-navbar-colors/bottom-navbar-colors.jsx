import ButtonLink from 'lib/ButtonLink';
import { BottomNavbar, TopNavbar } from 'lib/Layout';
import React from 'react';

const menuElements = [
  {
    name: 'Map',
    icon: 'marker',
    url: 'map'
  },
  {
    name: 'Cities',
    icon: 'city',
    url: 'cities'
  },
  {
    name: 'My area',
    icon: 'user',
    url: 'user'
  },
  {
    name: 'Shop',
    icon: 'shopping-cart',
    url: 'shop'
  }
];

const BottomNavbarColors = () => {
  return (
    <>
      <TopNavbar title="Mock application title" />
      <div className="center-screen p-2" style={{ height: 900 }}>
        <div className="mb-4">
          <ButtonLink to="/components/bottom-navbar">Go back to Bottom Navbar page</ButtonLink>
        </div>
        <p className="d-none d-md-block">
          Note that <b>bottom navbar</b> may be only visible in mobile view
        </p>
      </div>
      <BottomNavbar
        elements={menuElements}
        color="#60ac3a"
        fontColor="secondary"
        iconColor="light"
      />
    </>
  );
};

export default BottomNavbarColors;
