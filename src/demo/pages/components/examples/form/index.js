import formAllFields from './form-all-fields';
import formBasic from './form-basic';
import formHorizontal from './form-horizontal';
import formNoFields from './form-no-fields';
import formWithInitialValues from './form-with-initial-values';
import formWithNoButton from './form-with-no-button';

export default [
  formBasic,
  formNoFields,
  formAllFields,
  formWithInitialValues,
  formHorizontal,
  formWithNoButton
];
