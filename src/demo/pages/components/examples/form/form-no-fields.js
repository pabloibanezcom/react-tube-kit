import { info } from 'lib/Notification';

const html = `<div style={{width: 250}}>
  <Form
    value={value}
    submitText="Submit"
    submitBtnProps={{block: true}}
    onSubmit={ handleSubmit }
  />
</div>`;

const value = {
  firstName: 'Peter',
  lastName: 'Jones',
  age: 33,
  isRegistered: true
};

const handleSubmit = fData => {
  info(`<div>Form submitted with following data:</div></br><div>${JSON.stringify(fData)}</div>`);
};

const formNoFields = {
  id: 'noFields',
  name: 'Non-defined fields',
  html,
  bindings: { value, handleSubmit }
};

export default formNoFields;
