import { info } from 'lib/Notification';
import SelectorLine from '../../../../components/custom-components/selector-line/selector-line';
import lines from './lines.json';
import towns from './towns.json';

const html = `<div style={{width: 320}}>
  <Form
    fields={formFields}
    submitText="Submit form"
    submitBtnProps={{block: true}}
    labelClassName="text-secondary font-weight-normal"
    onSubmit={ handleSubmit }
  />
</div>`;

const formFields = [
  {
    name: 'firstName',
    label: 'First name',
    type: 'input',
    validation: {
      required: true
    }
  },
  {
    name: 'lastName',
    label: 'Last name',
    type: 'input',
    validation: {
      required: true
    }
  },
  {
    name: 'genre',
    label: 'Genre',
    type: 'radio',
    validation: {
      required: 'Genre is mandatory'
    },
    fieldProps: {
      options: [
        {
          label: 'Male',
          value: 'male'
        },
        {
          label: 'Female',
          value: 'female'
        }
      ]
    }
  },
  {
    name: 'town',
    label: 'Town',
    type: 'selector',
    validation: {
      required: true
    },
    fieldProps: {
      options: towns
    }
  },
  {
    name: 'line',
    label: 'Lines',
    type: 'selector',
    fieldProps: {
      options: lines,
      optionClassName: 'p-0',
      custom: SelectorLine,
      customProp: 'line',
      maxOptions: 20
    }
  },
  {
    name: 'availability',
    label: 'Availability',
    type: 'checkbox',
    fieldProps: {
      options: [
        {
          label: 'Morning',
          value: 'morning'
        },
        {
          label: 'Afternoon',
          value: 'afternoon'
        },
        {
          label: 'Evening',
          value: 'evening'
        }
      ]
    }
  },
  {
    name: 'price',
    label: 'Price',
    type: 'slider',
    fieldProps: {
      graduated: true,
      progress: true,
      showMarks: true,
      range: true,
      min: 10,
      max: 50,
      step: 10,
      defaultValue: [10, 50],
      renderMark: mark => {
        return `£${mark}`;
      }
    }
  },
  {
    name: 'color',
    label: 'Color',
    type: 'colorSelector'
  }
];

const handleSubmit = fData => {
  info(`<div>Form submitted with following data:</div></br><div>${JSON.stringify(fData)}</div>`);
};

const formAllFields = {
  id: 'allFields',
  name: 'All fields',
  html,
  bindings: { formFields, handleSubmit }
};

export default formAllFields;
