const html = `<div className="d-flex flex-column">
  <ButtonLink size="sm" className="mb-3" >
    Continue
  </ButtonLink>
  <ButtonLink className="mb-3">
    Continue
  </ButtonLink>
  <ButtonLink className="mb-6" size="lg" >
    Continue
  </ButtonLink>
  <ButtonLink type="link" size="sm" className="mb-3" >
    Link small
  </ButtonLink>
  <ButtonLink type="link" className="mb-3">
    Link medium
  </ButtonLink>
  <ButtonLink type="link" size="lg" >
    Link large
  </ButtonLink>
</div>`;

const buttonSize = {
  id: 'buttonSize',
  name: 'Size',
  html
};

export default buttonSize;
