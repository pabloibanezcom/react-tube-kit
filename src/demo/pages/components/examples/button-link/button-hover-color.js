import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(
      color =>
        `<ButtonLink color="${color}" hoverColor="${
          color !== 'primary' ? 'primary' : 'secondary'
        }">Continue</ButtonLink>`
    )
    .join('\n  ')}
</div>
`;

const buttonHoverColor = {
  id: 'buttonHoverColor',
  name: 'HoverColor',
  html,
  multi: true
};

export default buttonHoverColor;
