const html = `<ButtonLink
  fullWidth
>
  Continue
</ButtonLink>`;

const buttonFullWidth = {
  id: 'buttonFullWidth',
  name: 'Fullwidth',
  html
};

export default buttonFullWidth;
