import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(
      color =>
        `<div><ButtonLink className="mb-3" color="${color}">ButtonLink with ${color} color</ButtonLink></div>`
    )
    .join('\n  ')}
</div>`;

const buttonColors = {
  id: 'buttonColors',
  name: 'Colors',
  html
};

export default buttonColors;
