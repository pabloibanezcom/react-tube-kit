const html = `<ButtonLink
  uppercase={false}
>
  Continue
</ButtonLink>`;

const buttonNoTextTransformation = {
  id: 'no-text-transformation',
  name: 'No text transformation',
  html
};

export default buttonNoTextTransformation;
