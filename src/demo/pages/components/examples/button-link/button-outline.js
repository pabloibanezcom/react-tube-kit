import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors.map(color => `<ButtonLink outline color="${color}">Continue</ButtonLink>`).join('\n  ')}
</div>
`;

const buttonOutline = {
  id: 'outline',
  name: 'Outline',
  html,
  multi: true
};

export default buttonOutline;
