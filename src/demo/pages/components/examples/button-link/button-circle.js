const html = `<div className="d-flex flex-column">
  <ButtonLink
    circle
    size="sm"
    icon="search"
  />
  <ButtonLink
    circle
    icon="search"
  />
  <ButtonLink
    circle
    size="lg"
    icon="search"
  />
</div>`;

const buttonCircle = {
  id: 'buttonCircle',
  name: 'Circle',
  html,
  multi: true
};

export default buttonCircle;
