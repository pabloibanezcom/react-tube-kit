const html = `<ButtonLink
  disabled
>
  Some info
</ButtonLink>`;

const buttonDisabled = {
  id: 'buttonDisabled',
  name: 'Disabled',
  html
};

export default buttonDisabled;
