const html = `<div className="d-flex flex-column">
  <ButtonLink
    circle
    pulse
    icon="long-arrow-up"
  />
</div>`;

const buttonPulse = {
  id: 'buttonPulse',
  name: 'Pulse',
  html,
  multi: true
};

export default buttonPulse;
