const html = `<div>
  <div className="mb-4">
    <ButtonLink
      icon="search"
    >
      Continue
    </ButtonLink>
  </div>
  <div>
    <ButtonLink
      type="link"
      icon="search"
    />
  </div>
</div>`;

const buttonWithIcon = {
  id: 'with-icon',
  name: 'With icon',
  html
};

export default buttonWithIcon;
