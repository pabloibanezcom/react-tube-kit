import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(color => `<ButtonLink type="link" color="${color}">Link ${color}</ButtonLink>`)
    .join('\n  ')}
  ${colors
    .map(
      color =>
        `<ButtonLink type="link" color="${color}" hoverColor="${
          color !== 'primary' ? 'primary' : 'secondary'
        }">Link ${color} with hover</ButtonLink>`
    )
    .join('\n  ')}
</div>
`;

const buttonLinkColors = {
  id: 'buttonLinkColors',
  name: 'Link colors',
  html
};

export default buttonLinkColors;
