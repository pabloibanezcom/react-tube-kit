const html = `<ButtonLink
  badge={{}}
>
  Some info
</ButtonLink>`;

const buttonBadge = {
  id: 'badge',
  name: 'With badge',
  html
};

export default buttonBadge;
