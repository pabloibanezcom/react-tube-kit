import menuContent from './menu-content';

const html = `<Dropdown
  buttonProps={{ type: 'link' }}
  text="Link dropdown"
>
  {menuContent}
</Dropdown>`;

const dropdownAnchorLink = {
  id: 'dropdownAnchorLink',
  name: 'Anchor link',
  html,
  bindings: { menuContent }
};

export default dropdownAnchorLink;
