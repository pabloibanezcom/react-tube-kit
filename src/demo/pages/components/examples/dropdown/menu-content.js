import ButtonLink from 'lib/ButtonLink';
import React from 'react';

const menuContent = (
  <ul>
    {[1, 2, 3, 4].map(number => (
      <li style={{ lineHeight: 'initial' }} key={number}>
        <ButtonLink className="rounded-0" color="light" size="sm" block>
          Option {number}
        </ButtonLink>
      </li>
    ))}
  </ul>
);

export default menuContent;
