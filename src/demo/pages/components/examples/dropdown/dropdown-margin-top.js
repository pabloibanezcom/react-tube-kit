import menuContent from './menu-content';

const html = `<Dropdown
  text="Dropdown with menu margin top"
  menuMarginTop={20}
>
  {menuContent}
</Dropdown>`;

const dropdownMarginTop = {
  id: 'dropdownMarginTop',
  name: 'With menu margin top',
  html,
  bindings: { menuContent }
};

export default dropdownMarginTop;
