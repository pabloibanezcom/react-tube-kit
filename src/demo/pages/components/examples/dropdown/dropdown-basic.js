import menuContent from './menu-content';

const html = `<Dropdown
  text="Basic dropdown"
>
  {menuContent}
</Dropdown>`;

const dropdownBasic = {
  id: 'dropdownBasic',
  name: 'Basic',
  html,
  bindings: { menuContent }
};

export default dropdownBasic;
