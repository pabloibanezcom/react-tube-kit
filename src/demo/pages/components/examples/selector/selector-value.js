import towns from './towns.json';

const html = `<div className="width-200">
  <Selector
    options={towns}
    value="madrid"
  />
</div>`;

const selectorValue = {
  id: 'selectorValue',
  name: 'With value',
  html,
  bindings: { towns }
};

export default selectorValue;
