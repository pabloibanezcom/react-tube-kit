const html = `<div className="width-200">
  <Selector
    search
    disableInternalSearch
    onSearchChange={mockSearchChange}
  />
</div>`;

// const allOptions = [
//   'Arsenal',
//   'Charlton',
//   'Chelsea',
//   'Crystal Palace',
//   'Fulham',
//   'Tottenham',
//   'Watford',
//   'West Ham'
// ];

const mockSearchChange = str => {
  console.log(str);
};

const selectorExternalSearch = {
  id: 'selectorExternalSearch',
  name: 'External search',
  html,
  bindings: { mockSearchChange }
};

export default selectorExternalSearch;
