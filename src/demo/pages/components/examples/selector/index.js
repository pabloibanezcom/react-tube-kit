import selectorBasic from './selector-basic';
import selectorCustom from './selector-custom';
import selectorExternalSearch from './selector-external-search';
import selectorInputClearable from './selector-input-clearable';
import selectorManyOptions from './selector-many-options';
import selectorNullOption from './selector-null-option';
import selectorOptionCustomClass from './selector-option-custom-class';
import selectorSearch from './selector-search';
import selectorValue from './selector-value';

export default [
  selectorBasic,
  selectorValue,
  selectorNullOption,
  selectorSearch,
  selectorInputClearable,
  selectorExternalSearch,
  selectorCustom,
  selectorOptionCustomClass,
  selectorManyOptions
];
