import towns from './towns.json';

const html = `<div className="width-200">
  <Selector
    options={towns}
    search
    inputClearable
  />
</div>`;

const selectorInputClearable = {
  id: 'selectorInputClearable',
  name: 'Input clearable',
  html,
  bindings: { towns }
};

export default selectorInputClearable;
