import { info } from 'lib/Notification';
import imgSample from './sample_img.jpg';

const html = `<div className="d-flex flex-column" className="width-300">
  <Image
    src={imgSample}
    alt="example"
    onClick={handleClick}
  />
</div>`;

const handleClick = () => {
  info('Image was clicked');
};

const imageClickable = {
  id: 'imageClickable',
  name: 'Clickable',
  html,
  bindings: { imgSample, handleClick }
};

export default imageClickable;
