import imageBasic from './image-basic';
import imageClickable from './image-clickable';
import imageHoverAnimation from './image-hover-animation';
import imageOverlay from './image-overlay';

export default [imageBasic, imageClickable, imageHoverAnimation, imageOverlay];
