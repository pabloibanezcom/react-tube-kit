import { info } from 'lib/Notification';
import imgSample from './sample_img.jpg';

const html = `<div className="d-flex flex-column" className="width-300">
  <Image
    src={imgSample}
    alt="example"
    onClick={handleClick}
    showHoverAnimation
  />
</div>`;

const handleClick = () => {
  info('Image was clicked');
};

const imageHoverAnimation = {
  id: 'imageHoverAnimation',
  name: 'Hover animation',
  html,
  bindings: { imgSample, handleClick }
};

export default imageHoverAnimation;
