import imgSample from './sample_img.jpg';

const html = `<div className="d-flex flex-column" className="width-300">
  <Image
    src={imgSample}
    alt="example"
  />
</div>`;

const imageBasic = {
  id: 'imageBasic',
  name: 'Basic',
  html,
  bindings: { imgSample }
};

export default imageBasic;
