import colors from '../../../../colors.json';

const html = `<div style={{width: 300}}>
  ${colors
    .filter(color => color !== 'transparent')
    .map(
      color => `<Input hoverColor="${color}" placeholder="Hover color ${color}"className="mb-3" />`
    )
    .join('\n  ')}
</div>`;

const inputHoverColors = {
  id: 'inputHoverColors',
  name: 'Hover colors',
  html
};

export default inputHoverColors;
