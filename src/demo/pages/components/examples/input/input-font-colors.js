import colors from '../../../../colors.json';

const html = `<div style={{width: 300}}>
  ${colors
    .filter(color => color !== 'transparent')
    .map(
      color => `<Input fontColor="${color}" placeholder="Font color ${color}"className="mb-3" />`
    )
    .join('\n  ')}
</div>`;

const inputFontColors = {
  id: 'inputFontColors',
  name: 'Font colors',
  html
};

export default inputFontColors;
