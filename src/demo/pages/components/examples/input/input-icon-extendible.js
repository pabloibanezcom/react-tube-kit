const html = `<Input
  icon="search"
  iconExtendible
/>`;

const inputIconExtendible = {
  id: 'inputIconExtendible',
  name: 'Icon extendible',
  html
};

export default inputIconExtendible;
