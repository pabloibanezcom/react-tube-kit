const html = `<div>
  <Input
    icon="user"
    placeholder="Input with icon left"
    className="mb-4"
  />
  <Input
    icon="user"
    iconPosition="right"
    placeholder="Input with icon right"
  />
</div>`;

const inputWithIcon = {
  id: 'inputWithIcon',
  name: 'With icon',
  html
};

export default inputWithIcon;
