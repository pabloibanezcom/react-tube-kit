import colors from '../../../../colors.json';

const html = `<div style={{width: 300}}>
  ${colors
    .filter(color => color !== 'transparent')
    .map(color => `<Input color="${color}" placeholder="Color ${color}"className="mb-3" />`)
    .join('\n  ')}
</div>`;

const inputColors = {
  id: 'inputColors',
  name: 'Colors',
  html
};

export default inputColors;
