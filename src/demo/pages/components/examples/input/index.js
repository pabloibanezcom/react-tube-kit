import inputClearable from './input-clearable';
import inputColors from './input-colors';
import inputDefaultValue from './input-default-value';
import inputDisabled from './input-disabled';
import inputFontColors from './input-font-colors';
import inputHoverColors from './input-hover-colors';
import inputIconExtendible from './input-icon-extendible';
import inputPlaceholder from './input-placeholder';
import inputReadOnly from './input-readonly';
import inputTypes from './input-types';
import inputWithIcon from './input-with-icon';

export default [
  inputColors,
  inputHoverColors,
  inputFontColors,
  inputPlaceholder,
  inputDefaultValue,
  inputClearable,
  inputDisabled,
  inputReadOnly,
  inputTypes,
  inputWithIcon,
  inputIconExtendible
];
