import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column mb-2">
  ${colors
    .filter(c => c !== 'transparent')
    .map(
      color =>
        `<div className="d-flex flex-row"><Radio checked className="mr-4" color="${color}" label="${color}" /><Radio fontColor="${color}" label="${color} label" /></div>`
    )
    .join('\n  ')}
</div>`;

const radioColors = {
  id: 'colors',
  name: 'Colors',
  html
};

export default radioColors;
