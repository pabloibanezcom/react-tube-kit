const html = `<div className="mr-2">
  <Radio
    label="Default"
  />
</div>
<div>
  <Radio
    label="Checked"
    checked
  />
</div>`;

const radioBasic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default radioBasic;
