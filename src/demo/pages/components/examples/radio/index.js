import radioBasic from './radio-basic';
import radioColors from './radio-colors';

export default [radioBasic, radioColors];
