import { mockTabs } from './mockTabs';

const html = `<div className="w-100">
  <TabMenu
    tabs={mockTabs}
  />
</div>`;

const tabMenuBasic = {
  id: 'tabMenuBasic',
  name: 'Basic',
  html,
  bindings: { mockTabs }
};

export default tabMenuBasic;
