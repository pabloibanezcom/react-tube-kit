import colors from '../../../../colors.json';
import { mockTabs } from './mockTabs';

const html = `<div className="w-100">
  ${colors
    .filter(color => color !== 'transparent')
    .map(
      color =>
        `<TabMenu color="${color}" selectedTabColor="${
          color !== 'secondary' ? 'secondary' : 'primary'
        }" tabs={mockTabs} className="mb-2" />`
    )
    .join('\n  ')}
</div>`;

const tabMenuSelectedTabColors = {
  id: 'tabMenuSelectedTabColors',
  name: 'SelectedTab colors',
  html,
  bindings: { mockTabs }
};

export default tabMenuSelectedTabColors;
