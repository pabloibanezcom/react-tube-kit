import colors from '../../../../colors.json';
import { mockTabs } from './mockTabs';

const html = `<div className="w-100">
  ${colors
    .filter(color => color !== 'transparent')
    .map(color => `<TabMenu color="${color}" tabs={mockTabs} className="mb-2" />`)
    .join('\n  ')}
</div>`;

const tabMenuColors = {
  id: 'tabMenuColors',
  name: 'Colors',
  html,
  bindings: { mockTabs }
};

export default tabMenuColors;
