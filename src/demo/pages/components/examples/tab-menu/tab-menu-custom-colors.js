import { mockTabs } from './mockTabs';

const html = `<div className="d-flex flex-column" className="w-100">
  <div>
    <TabMenu
      tabs={mockTabs}
      color="#8a1717"
      fontColor="#95d08f"
      panelColor="#111111"
      panelFontColor="#98c9f5"
      selectedTabColor="#ff9800"
    />
  </div>
</div>`;

const tabMenuCustomColors = {
  id: 'tabMenuCustomColors',
  name: 'Custom colors',
  html,
  bindings: { mockTabs }
};

export default tabMenuCustomColors;
