import tabMenuActiveTab from './tab-menu-active-tab';
import tabMenuBasic from './tab-menu-basic';
import tabMenuColors from './tab-menu-colors';
import tabMenuCustomColors from './tab-menu-custom-colors';
import tabMenuSelectedTabColors from './tab-menu-selectedtab-colors';
import tabMenuSingleContent from './tab-menu-single-content';
import tabMenuWithIcons from './tab-menu-with-icons';

export default [
  tabMenuBasic,
  tabMenuColors,
  tabMenuSelectedTabColors,
  tabMenuCustomColors,
  tabMenuWithIcons,
  tabMenuActiveTab,
  tabMenuSingleContent
];
