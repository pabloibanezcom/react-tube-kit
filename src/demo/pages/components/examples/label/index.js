import labelColors from './label-colors';
import labelWeight from './label-weight';

export default [labelColors, labelWeight];
