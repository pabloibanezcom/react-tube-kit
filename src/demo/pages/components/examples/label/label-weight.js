const html = `<div className="d-flex flex-column">
  <Label weight="light">Label with light weight</Label>
  <Label weight="normal">Label with normal weight</Label>
  <Label weight="bold">Label with bold weight</Label>
</div>`;

const labelWeight = {
  id: 'labelWeight',
  name: 'Weight',
  html
};

export default labelWeight;
