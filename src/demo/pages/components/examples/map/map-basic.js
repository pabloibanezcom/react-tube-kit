const html = `<div className="w-100" style={{height: 200}}>
  <Map
    center={
      { 
        lat: 51.5099695, 
        lng: -0.1371599 
      }
    }
  />
</div>`;

const mapBasic = {
  id: 'mapBasic',
  name: 'Basic',
  html,
  actions: [{ text: 'See full screen', icon: 'screen-full', to: '/components/map/full-screen' }]
};

export default mapBasic;
