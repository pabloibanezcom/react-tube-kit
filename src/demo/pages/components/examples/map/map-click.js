import { info } from 'lib/Notification';

const html = `<div className="w-100" style={{height: 200}}>
  <Map
    center={
      { 
        lat: 51.5099695, 
        lng: -0.1371599 
      }
    }
    onClick={handleOnClick}
  />
</div>`;

const handleOnClick = evt => {
  info(
    `<div>Position clicked:</div></br><div>${JSON.stringify({
      lat: evt.latLng.lat(),
      lng: evt.latLng.lng()
    })}</div>`
  );
};

const mapClick = {
  id: 'mapClick',
  name: 'Click',
  html,
  bindings: { handleOnClick }
};

export default mapClick;
