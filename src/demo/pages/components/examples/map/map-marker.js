const html = `<div className="w-100" style={{height: 200}}>
  <Map
    center={
      { 
        lat: 51.5077563,
        lng: -0.1279291
      }
    }
    zoom={17}
    markers={[{
      position: {
        lat: 51.5077563,
        lng: -0.1279291
      }
    }]}
  />
</div>`;

const mapMarker = {
  id: 'mapMarker',
  name: 'Marker',
  html
};

export default mapMarker;
