import mapBasic from './map-basic';
import mapClick from './map-click';
import mapMarker from './map-marker';

export default [mapBasic, mapMarker, mapClick];
