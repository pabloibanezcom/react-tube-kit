import { info } from 'lib/Notification';

const html = `<RadioGroup
  options={radioOptions}
  value={'B'}
  onChange={handleChange}
/>`;

const radioOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C'
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const radioGroupBasic = {
  id: 'basic',
  name: 'Basic',
  html,
  bindings: { radioOptions, handleChange }
};

export default radioGroupBasic;
