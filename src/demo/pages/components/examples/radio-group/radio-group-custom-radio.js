import { info } from 'lib/Notification';

const html = `<RadioGroup
  options={radioOptions}
  value={'C'}
  onChange={handleChange}
/>`;

const radioOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C',
    radio: {
      color: 'secondary'
    }
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const radioGroupCustomRadio = {
  id: 'customRadio',
  name: 'Custom radio',
  html,
  bindings: { radioOptions, handleChange }
};

export default radioGroupCustomRadio;
