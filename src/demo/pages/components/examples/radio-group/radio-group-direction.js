import { info } from 'lib/Notification';

const html = `<div>
  <h5>Horizontal</h5>
  <RadioGroup
    options={radioOptions}
    value={'C'}
    onChange={handleChange}
    className="mb-4"
  />
  <h5>Vertical</h5>
  <RadioGroup
    direction="vertical"
    options={radioOptions}
    value={'C'}
    onChange={handleChange}
  />
</div>`;

const radioOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C'
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const radioGroupDirection = {
  id: 'direction',
  name: 'Direction',
  html,
  bindings: { radioOptions, handleChange }
};

export default radioGroupDirection;
