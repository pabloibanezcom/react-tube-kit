const html = `<div className="width-300">
  <FileUpload 
    showPreview
  />
</div>`;

const fileUploadPreview = {
  id: 'fileUploadPreview',
  name: 'With preview',
  html
};

export default fileUploadPreview;
