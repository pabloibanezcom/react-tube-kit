const html = `<div className="width-300">
  <FileUpload
    filesLimit={3}
  />
</div>`;

const fileUploadMultiple = {
  id: 'fileUploadMultiple',
  name: 'Multiple files',
  html
};

export default fileUploadMultiple;
