const html = `<div className="width-300">
  <FileUpload 
    showPreview
    filesLimit={6}
    previewsPerRow={2}
  />
</div>`;

const fileUploadPreviewMulti = {
  id: 'fileUploadPreviewMulti',
  name: 'With preview multi',
  html
};

export default fileUploadPreviewMulti;
