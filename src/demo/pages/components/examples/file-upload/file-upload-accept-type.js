const html = `<div className="width-300">
  <FileUpload
    accept="image/*,.pdf"
  />
</div>`;

const fileUploadAcceptType = {
  id: 'fileUploadAcceptType',
  name: 'Accepted Type',
  html
};

export default fileUploadAcceptType;
