const html = `<PlaceSelector 
  className="width-250"
  enableModal
  enableChangePlace
/>`;

const placeSelectorChangePlaceOnMap = {
  id: 'placeSelectorChangePlaceOnMap',
  name: 'Change place on map',
  html
};

export default placeSelectorChangePlaceOnMap;
