const html = `<PlaceSelector
  className="width-250"
  usePlaceIcon
/>`;

const placeSelectorPlaceIcon = {
  id: 'placeSelectorPlaceIcon',
  name: 'Place icon',
  html
};

export default placeSelectorPlaceIcon;
