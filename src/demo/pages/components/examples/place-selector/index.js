import placeSelectorBasic from './place-selector-basic';
import placeSelectorChangePlace from './place-selector-change-place';
import placeSelectorLocation from './place-selector-location';
import placeSelectorModal from './place-selector-modal';
import placeSelectorPlaceIcon from './place-selector-place-icon';
import placeSelectorType from './place-selector-type';

export default [
  placeSelectorBasic,
  placeSelectorType,
  placeSelectorPlaceIcon,
  placeSelectorLocation,
  placeSelectorModal,
  placeSelectorChangePlace
];
