const html = `<PlaceSelector
  className="width-250"
  type="transit_station"
/>`;

const placeSelectorType = {
  id: 'placeSelectorType',
  name: 'Type',
  html
};

export default placeSelectorType;
