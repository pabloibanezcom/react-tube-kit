const html = `<PlaceSelector 
  className="width-250"
  enableModal
/>`;

const placeSelectorModal = {
  id: 'placeSelectorModal',
  name: 'With modal',
  html
};

export default placeSelectorModal;
