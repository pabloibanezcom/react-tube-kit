const html = `<PlaceSelector
  className="width-250"
  location={{
    lat: 51.5099695,
    lng: -0.1371599
  }}
  radius="3000"
/>`;

const placeSelectorLocation = {
  id: 'placeSelectorLocation',
  name: 'Location & radius',
  html
};

export default placeSelectorLocation;
