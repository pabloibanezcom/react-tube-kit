const html = `<div className="w-100">
  <Slider
    defaultValue={40}
  />
</div>`;

const sliderBasic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default sliderBasic;
