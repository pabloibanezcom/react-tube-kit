import sliderBasic from './slider-basic';
import sliderColors from './slider-colors';
import sliderGraduated from './slider-graduated';
import sliderMinMax from './slider-min-max';
import sliderProgress from './slider-progress';
import sliderRange from './slider-range';

export default [
  sliderBasic,
  sliderProgress,
  sliderColors,
  sliderMinMax,
  sliderGraduated,
  sliderRange
];
