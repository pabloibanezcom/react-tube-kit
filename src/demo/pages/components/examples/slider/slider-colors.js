import colors from '../../../../colors.json';

const html = `<div className="w-100">
${colors
  .filter(c => !['light', 'transparent'].includes(c))
  .map(
    color => `<div className="mb-6"><Slider progress color="${color}" defaultValue={45} /></div>`
  )
  .join('\n  ')}
</div>`;

const sliderColors = {
  id: 'sliderColors',
  name: 'Colors',
  html
};

export default sliderColors;
