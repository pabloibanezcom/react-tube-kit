import { info } from 'lib/Notification';

const customMark = mark => {
  return mark < 6 ? mark : '6+';
};

const handleChange = value => {
  info(`<div>New slider value: ${value}</div>`);
};

const html = `<div className="w-100">
  <div className="mb-6">
    <Slider
      progress
      range
      defaultValue={[10,60]}
      onChange={ handleChange }
    />
  </div>
  <div>
    <Slider
      progress
      range
      graduated
      showMarks
      min={1}
      max={6}
      defaultValue={[2,4]}
      renderMark={customMark}
      onChange={ handleChange }
    />
  </div>
</div>`;

const sliderRange = {
  id: 'sliderRange',
  name: 'Range',
  html,
  bindings: { customMark, handleChange }
};

export default sliderRange;
