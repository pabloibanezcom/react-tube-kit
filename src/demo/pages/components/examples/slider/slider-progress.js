import { info } from 'lib/Notification';

const html = `<div className="w-100">
  <Slider
    progress
    defaultValue={60}
    onChange={ handleChange }
  />
</div>`;

const handleChange = value => {
  info(`<div>New slider value: ${value}</div>`);
};

const sliderProgress = {
  id: 'sliderProgress',
  name: 'Progress',
  html,
  bindings: { handleChange }
};

export default sliderProgress;
