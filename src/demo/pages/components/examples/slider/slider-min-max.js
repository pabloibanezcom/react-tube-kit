const html = `<div className="w-100 mb-6">
  <div className="mb-6">
    <Slider
      progress
      min={-100}
      max={100}
      defaultValue={0}
    />
  </div>
  <div className="mb-6">
    <Slider
      progress
      min={-100}
      max={100}
      defaultValue={50}
    />
  </div>
  <div>
    <Slider
      progress
      min={40}
      max={80}
      defaultValue={50}
    />
  </div>
</div>`;

const sliderMinMax = {
  id: 'sliderMinMax',
  name: 'Min & Max',
  html
};

export default sliderMinMax;
