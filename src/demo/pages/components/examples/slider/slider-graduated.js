const customMark = mark => {
  return `${mark} °C`;
};

const html = `<div className="w-100">
  <div className="mb-6">
    <Slider
      graduated
      defaultValue={20}
      step={10}
    />
  </div>
  <div className="mb-10">
    <Slider
      progress
      graduated
      showMarks
      defaultValue={20}
      step={10}
      handleColor="primary"
    />
  </div>
  <div className="mb-10">
    <Slider
      progress
      graduated
      showMarks
      min={30}
      max={60}
      step={5}
      defaultValue={60}
      fontColor="primary"
    />
  </div>
  <div>
    <Slider
      graduated
      showMarks
      min={0}
      max={100}
      step={10}
      renderMark={customMark}
    />
  </div>
</div>`;

const sliderGraduated = {
  id: 'sliderGraduated',
  name: 'Graduated',
  html,
  bindings: { customMark }
};

export default sliderGraduated;
