import badgeBorder from './badge-border';
import badgeColors from './badge-colors';
import badgeCustom from './badge-custom';
import badgeFullWidth from './badge-fullwidth';
import badgeOutline from './badge-outline';
import badgeSize from './badge-size';
import badgeWeight from './badge-weight';

export default [
  badgeColors,
  badgeOutline,
  badgeCustom,
  badgeBorder,
  badgeFullWidth,
  badgeSize,
  badgeWeight
];
