const html = `<Badge
  fullWidth
>
  Some info
</Badge>`;

const badgeFullWidth = {
  id: 'badgeFullWidth',
  name: 'Fullwidth',
  html
};

export default badgeFullWidth;
