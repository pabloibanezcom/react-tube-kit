const html = `<div className="d-flex flex-column">
  <div><Badge size="sm" className="mb-3">Badge small</Badge></div>
  <div><Badge className="mb-3">Badge medium</Badge></div>
  <div><Badge size="lg" className="mb-3">Badge large</Badge></div>
</div>`;

const badgeSize = {
  id: 'badgeSize',
  name: 'Size',
  html
};

export default badgeSize;
