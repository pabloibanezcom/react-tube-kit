import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(
      color =>
        `<div><Badge className="mb-2" color="${color}">Badge with ${color} color</Badge></div>`
    )
    .join('\n  ')}
</div>`;

const badgeColors = {
  id: 'badgeColors',
  name: 'Colors',
  html
};

export default badgeColors;
