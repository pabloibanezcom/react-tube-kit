const html = `<Badge
  color="#8a1717"
  fontColor="#95d08f"
>
  Some info
</Badge>`;

const badgeCustom = {
  id: 'custom',
  name: 'Custom colors',
  html
};

export default badgeCustom;
