const html = `<div className="d-flex flex-column">
  <div><Badge weight="light" className="mb-3">Badge light</Badge></div>
  <div><Badge className="mb-3">Badge normal</Badge></div>
  <div><Badge weight="bold" className="mb-3">Badge bold</Badge></div>
</div>`;

const badgeWeight = {
  id: 'badgeWeight',
  name: 'Weight',
  html
};

export default badgeWeight;
