import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(
      color =>
        `<div><Badge outline className="mb-2" color="${color}">Badge with ${color} color</Badge></div>`
    )
    .join('\n  ')}
</div>`;

const badgeOutline = {
  id: 'badgeOutline',
  name: 'Outline',
  html
};

export default badgeOutline;
