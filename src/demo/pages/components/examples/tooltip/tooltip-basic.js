const html = `<ButtonLink 
  color="light"
  className="mr-4"
  tooltip={{ text: 'This is a tooltip' }}
>
  Hover me
</ButtonLink>`;

const tooltipBasic = {
  id: 'basic',
  name: 'Basic',
  html,
  extraComponents: ['ButtonLink']
};

export default tooltipBasic;
