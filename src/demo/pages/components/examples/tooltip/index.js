import tooltipBasic from './tooltip-basic';
import tooltipColors from './tooltip-colors';
import tooltipEvent from './tooltip-event';
import tooltipPosition from './tooltip-position';

export default [tooltipBasic, tooltipPosition, tooltipColors, tooltipEvent];
