const html = `
  <ButtonLink 
    color="light"
    className="mr-4"
    tooltip={{ text: 'This is a tooltip', position: 'top'}}
  >
    Top
  </ButtonLink>
  <ButtonLink 
    color="light"
    className="mr-4"
    tooltip={{ text: 'This is a tooltip', position: 'right'}}
  >
    Right
  </ButtonLink>
  <ButtonLink 
    color="light"
    className="mr-4"
    tooltip={{ text: 'This is a tooltip', position: 'bottom'}}
  >
    Bottom
  </ButtonLink>
  <ButtonLink 
    color="light"
    tooltip={{ text: 'This is a tooltip', position: 'left'}}
  >
    Left
  </ButtonLink>
`;

const tooltipPosition = {
  id: 'tooltipPosition',
  name: 'Position',
  html,
  extraComponents: ['ButtonLink']
};

export default tooltipPosition;
