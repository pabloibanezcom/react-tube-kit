const html = `
  <ButtonLink 
    className="mr-4"
    color="light"
    tooltip={{ text: 'This is a tooltip', position: 'left'}}
  >
    Hover me
  </ButtonLink>
  <ButtonLink 
    className="mr-4"
    color="light"
    tooltip={{ text: 'This is a tooltip', position: 'right'}}
    tooltipEvent="click"
  >
    Click me
  </ButtonLink>
`;

const tooltipEvent = {
  id: 'tooltipEvent',
  name: 'Event',
  html,
  extraComponents: ['ButtonLink']
};

export default tooltipEvent;
