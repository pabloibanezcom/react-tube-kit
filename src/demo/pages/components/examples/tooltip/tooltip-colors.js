import colors from '../../../../colors.json';

const html = `
${colors
  .filter(c => c !== 'transparent')
  .map(
    color =>
      `<ButtonLink 
      color="light"
      className="mr-4"
      tooltip={{ text: 'This is a tooltip', position: 'top', color: '${color}'}}
    >
      ${color}
    </ButtonLink>`
  )
  .join('')}
`;

const tooltipColors = {
  id: 'tooltipColors',
  name: 'Colors',
  html,
  extraComponents: ['ButtonLink']
};

export default tooltipColors;
