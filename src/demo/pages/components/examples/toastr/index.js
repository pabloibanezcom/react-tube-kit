import toastrBasic from './toastr-basic';
import toastrColors from './toastr-colors';
import toastrCustomIcon from './toastr-custom-icon';
import toastrTypes from './toastr-types';

export default [toastrBasic, toastrTypes, toastrColors, toastrCustomIcon];
