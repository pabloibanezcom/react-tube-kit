import { info } from '../../../../../lib/Toastr';

const html = `<div className="mr-2">
  <Toastr 
    title="Important message"
    message="Something was done succesfully"
  />
</div>`;

const toastrBasic = {
  id: 'basic',
  name: 'Basic',
  html,
  actions: [
    {
      text: 'Fire toastr',
      func: () => info('Important message', 'Something was done succesfully'),
      icon: 'play'
    }
  ]
};

export default toastrBasic;
