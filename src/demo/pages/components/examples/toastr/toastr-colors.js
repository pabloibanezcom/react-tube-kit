import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column mb-2">
  ${colors
    .filter(c => c !== 'transparent')
    .map(
      color =>
        `<div className="d-flex flex-row mb-4"><Toastr title="Important message" message="Something was done succesfully" color="${color}"/></div>`
    )
    .join('\n  ')}
</div>`;

const toastrColors = {
  id: 'colors',
  name: 'Colors',
  html
};

export default toastrColors;
