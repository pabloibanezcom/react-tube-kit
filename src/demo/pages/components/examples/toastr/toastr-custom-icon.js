import { info } from '../../../../../lib/Toastr';

const html = `<div className="mr-2">
  <Toastr 
    title="Important message"
    message="Something was done succesfully"
    icon="user"
  />
</div>`;

const toastrCustomIcon = {
  id: 'customIcon',
  name: 'Custom Icon',
  html,
  actions: [
    {
      text: 'Fire toastr',
      func: () => info('Important message', 'Something was done succesfully', { icon: 'user' }),
      icon: 'play'
    }
  ]
};

export default toastrCustomIcon;
