import { error, info, success, warning } from '../../../../../lib/Toastr';

const types = ['success', 'info', 'warning', 'error'];

const html = `<div className="d-flex flex-column mb-2">
  ${types
    .map(
      type =>
        `<div className="d-flex flex-row mb-4"><Toastr title="Important message" message="Something was done succesfully" color="secondary" type="${type}"/></div>`
    )
    .join('\n  ')}
</div>`;

const toastrTypes = {
  id: 'types',
  name: 'Types',
  html,
  actions: [
    {
      text: 'Fire info',
      func: () => info('Important message', 'Something was done succesfully'),
      icon: 'info'
    },
    {
      text: 'Fire success',
      func: () => success('Important message', 'Something was done succesfully'),
      icon: 'check'
    },
    {
      text: 'Fire warning',
      func: () => warning('Important message', 'Something was done succesfully'),
      icon: 'warning'
    },
    {
      text: 'Fire error',
      func: () => error('Important message', 'Something was done succesfully'),
      icon: 'error'
    }
  ]
};

export default toastrTypes;
