import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-3 col-md-6">
    <Card
      image={{src: imgSample}}
      color="#8a1717"
      fontColor="#95d08f"
    >
      Some info
    </Card>
  </div>
</div>`;

const cardCustom = {
  id: 'cardCustom',
  name: 'Custom colors',
  html,
  bindings: { imgSample }
};

export default cardCustom;
