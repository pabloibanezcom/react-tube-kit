import colors from '../../../../colors.json';
import imgSample from './sample_img.jpg';

const html = `<div className="row">
  ${colors
    .map(
      color =>
        `<div className="col-lg-2 mb-4">
        <Card color="${color}" image={{src: imgSample}}>Some info</Card>
      </div>`
    )
    .join('\n  ')}
</div>`;

const cardColors = {
  id: 'cardColors',
  name: 'Colors',
  html,
  bindings: { imgSample }
};

export default cardColors;
