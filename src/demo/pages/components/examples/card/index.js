import cardBasic from './card-basic';
import cardColors from './card-colors';
import cardCustom from './card-custom';
import cardImageOptions from './card-image-options';
import cardLink from './card-link';
import cardPositions from './card-positions';
import cardSeparator from './card-separator';

export default [
  cardBasic,
  cardColors,
  cardSeparator,
  cardCustom,
  cardImageOptions,
  cardPositions,
  cardLink
];
