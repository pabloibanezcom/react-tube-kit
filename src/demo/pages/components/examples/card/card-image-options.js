import { info } from 'lib/Notification';
import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-3 col-md-6">
    <Card
      image={{
        src: imgSample, 
        alt: 'My card image', 
        actionText: 'Do some action', 
        showOverlay: true, 
        showHoverAnimation: true
      }}
      onImageClick={handleClick}
    >
      Some info
    </Card>
  </div>
</div>`;

const handleClick = () => {
  info('Image was clicked');
};

const cardImageOptions = {
  id: 'cardImageOptions',
  name: 'Image options',
  html,
  bindings: { imgSample, handleClick }
};

export default cardImageOptions;
