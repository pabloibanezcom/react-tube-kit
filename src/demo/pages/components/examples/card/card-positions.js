import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-2 mb-4">
    <Card
      color="secondary"
      image={{src: imgSample}}
    >
      Some info
    </Card>
  </div>
  <div className="col-lg-2 mb-4">
    <Card
      color="secondary"
      image={{src: imgSample}}
      imagePosition="bottom"
    >
      Some info
    </Card>
  </div>
  <div className="col-lg-2 mb-4">
    <Card
      color="secondary"
      image={{src: imgSample}}
      imagePosition="left"
    >
      Some info
    </Card>
  </div>
  <div className="col-lg-2 mb-4">
    <Card
      color="secondary"
      image={{src: imgSample}}
      imagePosition="right"
    >
      Some info
    </Card>
  </div>
</div>`;

const cardPositions = {
  id: 'cardPositions',
  name: 'Image positions',
  html,
  bindings: { imgSample }
};

export default cardPositions;
