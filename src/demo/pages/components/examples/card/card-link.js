import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-3 col-md-6">
    <Card
      separatorColor="primary"
      to="/"
      image={{
        src: imgSample,
        alt: 'My card image',
        showOverlay: true,
        showHoverAnimation: true
      }}
    >
      Some info (Link to root page)
    </Card>
  </div>
</div>`;

const cardLink = {
  id: 'cardLink',
  name: 'Link',
  html,
  bindings: { imgSample }
};

export default cardLink;
