import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-3 col-md-6">
    <Card
      color="transparent"
      separatorColor="primary"
      image={{src: imgSample}}
    >
      Some info
    </Card>
  </div>
</div>`;

const cardSeparator = {
  id: 'cardSeparator',
  name: 'Separator',
  html,
  bindings: { imgSample }
};

export default cardSeparator;
