import imgSample from './sample_img.jpg';

const html = `<div className="row">
  <div className="col-lg-3 col-md-6">
    <Card
      image={{src: imgSample}}
    >
      Some info
    </Card>
  </div>
</div>`;

const cardBasic = {
  id: 'cardBasic',
  name: 'Basic',
  html,
  bindings: { imgSample }
};

export default cardBasic;
