import panelColors from './panel-colors';
import panelCustomColors from './panel-custom-colors';
import panelHeaderColors from './panel-header-colors';
import panelHeaderIcon from './panel-header-icon';

export default [panelColors, panelHeaderColors, panelCustomColors, panelHeaderIcon];
