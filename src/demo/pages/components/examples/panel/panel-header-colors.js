import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column" className="w-100">
${colors
  .map(
    color =>
      `<div><Panel className="mb-3" headerColor="${color}" headerText="Header ${color}">Some content</Panel></div>`
  )
  .join('\n  ')}
</div>`;

const panelHeaderColors = {
  id: 'panelHeaderColors',
  name: 'Header colors',
  html
};

export default panelHeaderColors;
