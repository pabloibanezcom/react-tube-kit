import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column" className="w-100">
  ${colors
    .map(
      color =>
        `<div><Panel className="mb-3" color="${color}">Panel with ${color} color</Panel></div>`
    )
    .join('\n  ')}
</div>`;

const panelColors = {
  id: 'panelColors',
  name: 'Colors',
  html
};

export default panelColors;
