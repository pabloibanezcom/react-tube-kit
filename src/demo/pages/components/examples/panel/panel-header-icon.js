const html = `<div className="w-100">
  <Panel
    headerText="Header text with icon"
    headerIcon="user"
  >
  This is a panel
  </Panel>
</div>`;

const panelHeaderIcon = {
  id: 'panelHeaderIcon',
  name: 'Header icon',
  html
};

export default panelHeaderIcon;
