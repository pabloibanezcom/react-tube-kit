const html = `<div className="d-flex flex-column" className="w-100">
  <div>
    <Panel 
      className="mb-3"
      color="#8a1717"
      fontColor="#95d08f"
      headerColor="#111111"
      headerFontColor="#98c9f5"
      headerText="Custom colors header"
    >
      Customo colors panel
    </Panel>
  </div>
</div>`;

const panelCustomColors = {
  id: 'panelCustomColors',
  name: 'Custom colors',
  html
};

export default panelCustomColors;
