import colors from '../../../../colors.json';

const html = `<div>
  <Icon iconName="user" />
  ${colors.map(color => `<Icon iconName="user" color="${color}"/>`).join('\n  ')}
</div>`;

const iconColors = {
  id: 'iconColors',
  name: 'Colors',
  html
};

export default iconColors;
