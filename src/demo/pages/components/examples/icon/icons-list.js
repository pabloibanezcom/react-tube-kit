import iconsSelection from 'lib/assets/icons/selection.json';

const html = `<div className="d-flex flex-row flex-wrap">
  ${iconsSelection.icons
    .map(icon => icon.properties.name.split(',')[0])
    .sort((a, b) => a.localeCompare(b))
    .map(
      iconName =>
        `<div className="d-flex flex-column text-left mb-5 mr-2" style={{ width: 150 }}>
      <span className="font-weight-normal text-black-50 mb-1">${iconName}</span>
      <Icon iconName="${iconName}" color="secondary" />
    </div>`
    )
    .join('')}
</div>`;

const iconsList = {
  id: 'iconsList',
  name: 'Icons List',
  html,
  disableCode: true
};

export default iconsList;
