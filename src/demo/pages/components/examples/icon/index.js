import iconBasic from './icon-basic';
import iconColors from './icon-colors';
import iconCustomColor from './icon-custom-color';
import iconSizes from './icon-sizes';
import iconsList from './icons-list';

export default [iconBasic, iconColors, iconCustomColor, iconSizes, iconsList];
