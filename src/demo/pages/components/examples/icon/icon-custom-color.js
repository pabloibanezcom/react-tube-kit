const html = `<div>
  <Icon iconName="user" color="#da35e0"/>
</div>`;

const iconCustomColor = {
  id: 'customColor',
  name: 'Custom color',
  html
};

export default iconCustomColor;
