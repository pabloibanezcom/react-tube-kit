const html = `<Icon iconName="user" />`;

const iconBasic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default iconBasic;
