const html = `<div>
  <Icon iconName="user" size="sm"/>
  <Icon iconName="user" />
  <Icon iconName="user" size="lg"/>
</div>`;

const iconSizes = {
  id: 'sizes',
  name: 'Sizes',
  html
};

export default iconSizes;
