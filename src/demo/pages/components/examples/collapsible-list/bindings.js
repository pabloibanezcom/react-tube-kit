import FakeContent from './fakeContent';
import FakeHeader from './fakeHeader';
import lines from './lines.json';

export default {
  lines: lines.slice(0, 9),
  linesShort: lines.slice(0, 3),
  FakeHeader,
  FakeContent
};
