import bindings from './bindings';

const html = `<div className="w-100">
  <CollapsibleList
    color="light"
    hoverColor="primary"
    elements={linesShort}
    header={FakeHeader}
    content={FakeContent}
  />
</div>`;

const collapseHoverColor = {
  id: 'collapseHoverColor',
  name: 'Hover color',
  html,
  bindings
};

export default collapseHoverColor;
