import bindings from './bindings';

const html = `<div className="w-100">
  <CollapsibleList
    color="#8a1717"
    fontColor="#95d08f"
    elements={linesShort}
    header={FakeHeader}
    content={FakeContent}
  />
</div>`;

const collapseCustomColor = {
  id: 'collapseCustomColor',
  name: 'Custom color',
  html,
  bindings
};

export default collapseCustomColor;
