import collapseBasic from './collapse-basic';
import collapseColors from './collapse-colors';
import collapseCustomColor from './collapse-custom-color';
import collapseHoverColor from './collapse-hover-color';

export default [collapseBasic, collapseColors, collapseCustomColor, collapseHoverColor];
