import colors from '../../../../colors.json';
import bindings from './bindings';

const html = `<div className="w-100">
${colors
  .map(
    color =>
      `<CollapsibleList
        color="${color}"
        elements={linesShort}
        header={FakeHeader}
        content={FakeContent}
        className="mb-6"
    />`
  )
  .join('\n  ')}
  
</div>`;

const collapseColors = {
  id: 'collapseColors',
  name: 'Colors',
  html,
  bindings
};

export default collapseColors;
