import bindings from './bindings';

const html = `<div className="w-100">
  <CollapsibleList
    elements={linesShort}
    header={FakeHeader}
    content={FakeContent}
  />
</div>`;

const collapseBasic = {
  id: 'collapseBasic',
  name: 'Basic',
  html,
  bindings
};

export default collapseBasic;
