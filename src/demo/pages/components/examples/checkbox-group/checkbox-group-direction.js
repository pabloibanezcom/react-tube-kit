import { info } from 'lib/Notification';

const html = `<div>
  <h5>Horizontal</h5>
  <CheckboxGroup
    options={checkBoxOptions}
    value={['C']}
    onChange={handleChange}
    className="mb-4"
  />
  <h5>Vertical</h5>
  <CheckboxGroup
    direction="vertical"
    options={checkBoxOptions}
    value={['C']}
    onChange={handleChange}
  />
</div>`;

const checkBoxOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C'
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const checkboxGroupDirection = {
  id: 'direction',
  name: 'Direction',
  html,
  bindings: { checkBoxOptions, handleChange }
};

export default checkboxGroupDirection;
