import { info } from 'lib/Notification';

const html = `<CheckboxGroup
  options={checkBoxOptions}
  value={['B', 'C']}
  onChange={handleChange}
/>`;

const checkBoxOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C',
    checkbox: {
      color: 'secondary'
    }
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const checkboxGroupCustomCheckBox = {
  id: 'customCheckbox',
  name: 'Custom checkbox',
  html,
  bindings: { checkBoxOptions, handleChange }
};

export default checkboxGroupCustomCheckBox;
