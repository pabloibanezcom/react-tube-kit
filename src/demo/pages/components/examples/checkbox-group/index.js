import checkboxGroupBasic from './checkbox-group-basic';
import checkboxGroupCheckAll from './checkbox-group-check-all';
import checkboxGroupCustomCheckbox from './checkbox-group-custom-checkbox';
import checkboxGroupDirection from './checkbox-group-direction';
import checkboxGroupMax from './checkbox-group-max';

export default [
  checkboxGroupBasic,
  checkboxGroupCustomCheckbox,
  checkboxGroupDirection,
  checkboxGroupCheckAll,
  checkboxGroupMax
];
