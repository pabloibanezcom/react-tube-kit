import { info } from 'lib/Notification';

const html = `<CheckboxGroup
  options={checkBoxOptions}
  max={2}
  onChange={handleChange}
/>`;

const checkBoxOptions = [
  {
    label: 'Item A',
    value: 'A'
  },
  {
    label: 'Item B',
    value: 'B'
  },
  {
    label: 'Item C',
    value: 'C'
  },
  {
    label: 'Item D',
    value: 'D'
  }
];

const handleChange = value => {
  info(`<div>New value: ${value}</div>`);
};

const checkboxGroupMax = {
  id: 'max',
  name: 'Max',
  html,
  bindings: { checkBoxOptions, handleChange }
};

export default checkboxGroupMax;
