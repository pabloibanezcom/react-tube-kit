const html = `<Breadcrumb
  items={breadcrumbItems}
/>`;

const breadcrumbItems = [
  {
    text: 'Home',
    url: '/'
  },
  {
    text: 'Library',
    url: '/library'
  },
  {
    text: 'Data',
    url: null
  }
];

const breadcrumbBasic = {
  id: 'breadcrumbBasic',
  name: 'Basic',
  html,
  bindings: { breadcrumbItems }
};

export default breadcrumbBasic;
