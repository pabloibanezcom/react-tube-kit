const html = `<div className="d-flex flex-column">
  <div><Breadcrumb items={breadcrumbItems} size="sm" className="mb-3" /></div>
  <div><Breadcrumb items={breadcrumbItems} className="mb-3" /></div>
  <div><Breadcrumb items={breadcrumbItems} size="lg" className="mb-3" /></div>
</div>`;

const breadcrumbItems = [
  {
    text: 'Home',
    url: '/'
  },
  {
    text: 'Library',
    url: '/library'
  },
  {
    text: 'Data',
    url: null
  }
];

const breadcrumbSize = {
  id: 'breadcrumbSize',
  name: 'Size',
  html,
  bindings: { breadcrumbItems }
};

export default breadcrumbSize;
