import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
  ${colors
    .map(
      color =>
        `<div><Breadcrumb items={breadcrumbItems} className="mb-2" linkColor="${color}" /></div>`
    )
    .join('\n  ')}
</div>`;

const breadcrumbItems = [
  {
    text: 'Home',
    url: '/'
  },
  {
    text: 'Library',
    url: '/library'
  },
  {
    text: 'Data',
    url: null
  }
];

const breadcrumbColors = {
  id: 'breadcrumbColors',
  name: 'Colors',
  html,
  bindings: { breadcrumbItems }
};

export default breadcrumbColors;
