import breadcrumbBasic from './breadcrumb-basic';
import breadcrumbColors from './breadcrumb-colors';
import breadcrumbSize from './breadcrumb-size';

export default [breadcrumbBasic, breadcrumbColors, breadcrumbSize];
