const html = `<ColorSelector
  inputProps={{ placeholder: "Select a color" }}
/>`;

const basic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default basic;
