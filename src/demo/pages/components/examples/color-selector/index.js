import basic from './basic';
import clearable from './clearable';
import icon from './icon';
import initialColor from './initial-color';

export default [basic, initialColor, icon, clearable];
