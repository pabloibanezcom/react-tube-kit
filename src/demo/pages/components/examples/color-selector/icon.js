const html = `<ColorSelector
  icon="circle"
  color="#ee2112"
/>`;

const icon = {
  id: 'icon',
  name: 'Icon',
  html
};

export default icon;
