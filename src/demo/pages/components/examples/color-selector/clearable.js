const html = `<ColorSelector
  inputProps={{ placeholder: "Select a color", clearable: true }}
/>`;

const clearable = {
  id: 'clearable',
  name: 'Clearable',
  html
};

export default clearable;
