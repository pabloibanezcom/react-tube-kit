import colors from '../../../../colors.json';

const html = `<div className="w-100">
${colors
  .filter(color => color !== 'transparent')
  .map(
    color =>
      `<Pagination pagination={mockPagination} color="light" hoverColor="${color}" className="mb-6" />`
  )
  .join('\n  ')}
</div>`;

const mockPagination = {
  page: 1,
  pages: 10
};

const paginationHoverColor = {
  id: 'paginationHoverColor',
  name: 'HoverColor',
  html,
  bindings: { mockPagination }
};

export default paginationHoverColor;
