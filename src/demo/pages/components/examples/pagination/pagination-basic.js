const html = `<div className="w-100">
  <Pagination
    pagination={mockPagination}
  />
</div>`;

const mockPagination = {
  page: 1,
  pages: 10
};

const paginationBasic = {
  id: 'paginationBasic',
  name: 'Basic',
  html,
  bindings: { mockPagination }
};

export default paginationBasic;
