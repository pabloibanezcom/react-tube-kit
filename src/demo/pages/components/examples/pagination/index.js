import paginationBasic from './pagination-basic';
import paginationColors from './pagination-colors';
import paginationHoverColors from './pagination-hover-colors';
import paginationSizes from './pagination-sizes';

export default [paginationBasic, paginationColors, paginationHoverColors, paginationSizes];
