import colors from '../../../../colors.json';

const html = `<div className="w-100">
${colors
  .filter(color => color !== 'transparent')
  .map(color => `<Pagination pagination={mockPagination} color="${color}" className="mb-6" />`)
  .join('\n  ')}
</div>`;

const mockPagination = {
  page: 1,
  pages: 10
};

const paginationColors = {
  id: 'paginationColors',
  name: 'Colors',
  html,
  bindings: { mockPagination }
};

export default paginationColors;
