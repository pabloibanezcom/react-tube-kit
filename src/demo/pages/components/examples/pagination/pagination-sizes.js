const html = `<div className="w-100">
  <Pagination pagination={mockPagination} className="mb-6" size="sm"/>
  <Pagination pagination={mockPagination} className="mb-6"/>
  <Pagination pagination={mockPagination} className="mb-6" size="lg"/>
</div>`;

const mockPagination = {
  page: 1,
  pages: 10
};

const paginationSizes = {
  id: 'paginationSizes',
  name: 'Sizes',
  html,
  bindings: { mockPagination }
};

export default paginationSizes;
