import checkboxBasic from './checkbox-basic';
import checkboxColors from './checkbox-colors';

export default [checkboxBasic, checkboxColors];
