const html = `<div className="mr-2">
  <Checkbox
    label="Default"
  />
</div>
<div>
  <Checkbox
    label="Checked"
    checked
  />
</div>`;

const checkboxBasic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default checkboxBasic;
