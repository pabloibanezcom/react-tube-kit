import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column mb-2">
  ${colors
    .filter(c => c !== 'transparent')
    .map(
      color =>
        `<div className="d-flex flex-row"><Checkbox checked className="mr-4" color="${color}" label="${color}" /><Checkbox fontColor="${color}" label="${color} label" /></div>`
    )
    .join('\n  ')}
</div>`;

const checkboxColors = {
  id: 'colors',
  name: 'Colors',
  html
};

export default checkboxColors;
