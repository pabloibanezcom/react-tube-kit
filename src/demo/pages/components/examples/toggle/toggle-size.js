const html = `<div className="mr-2">
  <Toggle
    size="lg"
  />
</div>
<div className="mr-2">
  <Toggle
    size="md"
  />
</div>
<div>
  <Toggle
    size="sm"
  />
</div>`;

const toggleSize = {
  id: 'size',
  name: 'Size',
  html
};

export default toggleSize;
