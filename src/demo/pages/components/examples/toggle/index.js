import toggleBasic from './toggle-basic';
import toggleColors from './toggle-colors';
import toggleDisabled from './toggle-disabled';
import toggleSize from './toggle-size';
import toggleTextAndIcon from './toggle-text-and-icon';

export default [toggleBasic, toggleColors, toggleSize, toggleTextAndIcon, toggleDisabled];
