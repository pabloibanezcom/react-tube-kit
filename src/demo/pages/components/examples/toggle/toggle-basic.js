const html = `<div className="mr-2">
  <Toggle />
</div>
<div>
  <Toggle value={true} />
</div>`;

const toggleBasic = {
  id: 'basic',
  name: 'Basic',
  html
};

export default toggleBasic;
