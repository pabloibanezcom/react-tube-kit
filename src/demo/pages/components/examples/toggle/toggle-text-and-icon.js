const html = `<div className="d-flex flex-column">
  <div className="mb-4">
    <Toggle size="lg" textChecked="Open" textUnchecked="Close" className="mr-2"/>
    <Toggle textChecked="Open" textUnchecked="Close" className="mr-2" />
    <Toggle size="sm" textChecked="Open" textUnchecked="Close"/>
  </div>
  <div className="mb-4">
    <Toggle size="lg" iconChecked="check" iconUnchecked="close" className="mr-2"/>
    <Toggle value={true} iconChecked="check" iconUnchecked="close" className="mr-2" />
    <Toggle size="sm" iconChecked="check" iconUnchecked="close"/>
  </div>
  <div>
    <Toggle size="lg" uncheckedColor="secondary" value={true} iconChecked="flight-land" iconUnchecked="flight-takeoff" textChecked="Arrivals" textUnchecked="Departures" className="mr-2"/>
    <Toggle uncheckedColor="secondary" iconChecked="flight-land" iconUnchecked="flight-takeoff" textChecked="Arrivals" textUnchecked="Departures" className="mr-2" />
    <Toggle size="sm" uncheckedColor="secondary" iconChecked="flight-land" iconUnchecked="flight-takeoff" textChecked="Arrivals" textUnchecked="Departures"/>
  </div>
</div>`;

const toggleTextAndIcon = {
  id: 'textAndIcon',
  name: 'Text and icon',
  html
};

export default toggleTextAndIcon;
