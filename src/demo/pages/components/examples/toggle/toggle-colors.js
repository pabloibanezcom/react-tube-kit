import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column mb-2">
  ${colors
    .filter(c => c !== 'transparent')
    .map(
      color => `<div className="d-flex flex-row mb-4"><Toggle value={true} color="${color}"/></div>`
    )
    .join('\n  ')}
  <div className="d-flex flex-row mt-4"><Toggle value={true} color="primary" uncheckedColor="secondary"/></div>
</div>`;

const toggleColors = {
  id: 'colors',
  name: 'Colors',
  html
};

export default toggleColors;
