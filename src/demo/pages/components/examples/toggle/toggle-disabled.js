const html = `<div>
  <Toggle disabled />
</div>
`;

const toggleDisabled = {
  id: 'disabled',
  name: 'Disabled',
  html
};

export default toggleDisabled;
