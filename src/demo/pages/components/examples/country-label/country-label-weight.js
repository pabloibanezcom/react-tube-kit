const html = `<div className="d-flex flex-column">
  <div><CountryLabel weight="light" className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel weight="normal" className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel weight="bold" className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
</div>`;

const countryLabelWeight = {
  id: 'countryLabelWeight',
  name: 'Weight',
  html
};

export default countryLabelWeight;
