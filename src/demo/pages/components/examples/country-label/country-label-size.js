const html = `<div className="d-flex flex-column">
  <div><CountryLabel size="sm" className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel size="lg" className="mb-6" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel size="sm" badge className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel badge className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
  <div><CountryLabel size="lg" badge className="mb-3" country={{code: 'GB', name: 'United Kingdom'}}/></div>
</div>`;

const countryLabelSize = {
  id: 'countryLabelSize',
  name: 'Size',
  html
};

export default countryLabelSize;
