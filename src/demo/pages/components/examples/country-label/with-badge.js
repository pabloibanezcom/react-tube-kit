import colors from '../../../../colors.json';

const html = `<div className="d-flex flex-column">
${colors
  .map(
    color =>
      `<div><CountryLabel className="mb-2" badge color="${color}" country={{code: 'GB', name: 'United Kingdom'}}/></div>`
  )
  .join('\n  ')}
</div>`;

const withBadge = {
  id: 'withBadge',
  name: 'With Badge',
  html
};

export default withBadge;
