import basic from './basic';
import countriesList from './countries-list';
import countryLabelSize from './country-label-size';
import countryLabelWeight from './country-label-weight';
import customName from './custom-name';
import withBadge from './with-badge';

export default [basic, customName, withBadge, countryLabelSize, countryLabelWeight, countriesList];
