const html = `<Modal
  content="This is a basic modal content"
  isOpen={showModal}
/>`;

const modalBasic = {
  id: 'basic',
  name: 'Basic',
  html,
  actions: [{ text: 'Show modal', propName: 'showModal', icon: 'play', propValue: true }]
};

export default modalBasic;
