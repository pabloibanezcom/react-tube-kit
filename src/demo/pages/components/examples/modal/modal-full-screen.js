const html = `<Modal
  content="This is a basic modal content"
  fullScreen
  isOpen={showModal}
/>`;

const modalFullScreen = {
  id: 'modalFullScreen',
  name: 'Full Screen',
  html,
  actions: [{ text: 'Show modal', propName: 'showModal', icon: 'play', propValue: true }]
};

export default modalFullScreen;
