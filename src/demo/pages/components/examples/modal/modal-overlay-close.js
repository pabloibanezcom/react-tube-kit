const html = `<Modal
  content="This is a basic modal content"
  isOpen={showModal}
  shouldCloseOnOverlayClick
/>`;

const modalOverlayClose = {
  id: 'overlayClose',
  name: 'Overlay close',
  html,
  actions: [{ text: 'Show modal', propName: 'showModal', icon: 'play', propValue: true }]
};

export default modalOverlayClose;
