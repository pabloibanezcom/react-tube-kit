const html = `<Modal
  content="This is a basic modal content"
  isOpen={showModal}
  color="secondary"
  closeButtonColor="primary"
/>`;

const modalColor = {
  id: 'color',
  name: 'Color',
  html,
  actions: [{ text: 'Show modal', propName: 'showModal', icon: 'play', propValue: true }]
};

export default modalColor;
