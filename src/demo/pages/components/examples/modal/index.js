import modalBasic from './modal-basic';
import modalColor from './modal-color';
import modalContentComponent from './modal-content-component';
import modalFullScreen from './modal-full-screen';
import modalOverlayClose from './modal-overlay-close';

export default [modalBasic, modalColor, modalOverlayClose, modalContentComponent, modalFullScreen];
