import React from 'react';

const html = `<Modal
  content={mockContent}
  isOpen={showModal}
/>`;

const mockContent = () => (
  <div>
    <span>This content is a rendered component</span>
  </div>
);

const modalContentComponent = {
  id: 'cotentComponent',
  name: 'Content component',
  html,
  bindings: { mockContent },
  actions: [{ text: 'Show modal', propName: 'showModal', icon: 'play', propValue: true }]
};

export default modalContentComponent;
