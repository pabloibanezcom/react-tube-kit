const html = `<div className="demo-element__sample-div" >
  <LoadingSpinner
    loading={isLoading}
  />
</div>`;

const loadingSpinnerBasic = {
  id: 'basic',
  name: 'Basic',
  html,
  actions: [
    { text: 'Start spinner', textActive: 'Stop spinner', propName: 'isLoading', icon: 'play' }
  ]
};

export default loadingSpinnerBasic;
