const html = `<div className="demo-element__sample-div" >
  <LoadingSpinner
    color="secondary"
    loading={isLoading}
  />
</div>`;

const loadingSpinnerColor = {
  id: 'color',
  name: 'Color',
  html,
  actions: [
    { text: 'Start spinner', textActive: 'Stop spinner', propName: 'isLoading', icon: 'play' }
  ]
};

export default loadingSpinnerColor;
