import loadingSpinnerBackground from './loading-spinner-background';
import loadingSpinnerBasic from './loading-spinner-basic';
import loadingSpinnerColor from './loading-spinner-color';
import loadingSpinnerNoSpinner from './loading-spinner-no-spinner';
import loadingSpinnerShape from './loading-spinner-shape';

export default [
  loadingSpinnerBasic,
  loadingSpinnerColor,
  loadingSpinnerBackground,
  loadingSpinnerNoSpinner,
  loadingSpinnerShape
];
