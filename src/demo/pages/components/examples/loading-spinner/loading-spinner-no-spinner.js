const html = `<div className="demo-element__sample-div" >
  <LoadingSpinner
    noSpinner
    loading={isLoading}
  />
</div>`;

const loadingSpinnerNoSpinner = {
  id: 'noSpinner',
  name: 'No Spinner',
  html,
  actions: [
    { text: 'Start spinner', textActive: 'Stop spinner', propName: 'isLoading', icon: 'play' }
  ]
};

export default loadingSpinnerNoSpinner;
