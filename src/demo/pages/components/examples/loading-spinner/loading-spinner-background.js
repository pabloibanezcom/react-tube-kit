const html = `<div className="demo-element__sample-div" >
  <LoadingSpinner
    background="dark"
    loading={isLoading}
  />
</div>`;

const loadingSpinnerBackground = {
  id: 'background',
  name: 'Background',
  html,
  actions: [
    { text: 'Start spinner', textActive: 'Stop spinner', propName: 'isLoading', icon: 'play' }
  ]
};

export default loadingSpinnerBackground;
