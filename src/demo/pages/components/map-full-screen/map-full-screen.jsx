/* eslint-disable import/named */
import ButtoLink from 'lib/ButtonLink';
import Map from 'lib/Map';
import { randomPick, randomPickAndRemove } from 'lib/util/array';
import React, { useEffect, useState } from 'react';
import { animateScroll } from 'react-scroll';
import OptionsPanel from '../../../components/options-panel/options-panel';
import markersList from './markers.json';

const fakeInfoWindow = () => <div>This is an info window</div>;

const MapFullScreen = () => {
  const allMarkers = markersList.map(m => {
    return { ...m, infoWindow: fakeInfoWindow() };
  });

  const [center, setCenter] = useState({
    lat: 51.5099695,
    lng: -0.1371599
  });
  const [zoom, setZoom] = useState(13);
  const [markers, setMarkers] = useState([]);
  const [styles, setStyles] = useState(['plain']);
  const [availableMarkerIndexes, setAvailableMarkerIndexes] = useState(allMarkers.map(m => m.id));

  useEffect(() => {
    animateScroll.scrollToTop();
  }, []);

  const changeCenter = () => {
    setCenter({
      lat: 51.5110128,
      lng: -0.0396797
    });
  };

  const zoomIn = () => {
    setZoom(zoom + 1);
  };

  const zoomOut = () => {
    setZoom(zoom - 1);
  };

  const addMarker = () => {
    const _availableMarkerIndexes = [...availableMarkerIndexes];
    const newMarker = allMarkers[randomPickAndRemove(_availableMarkerIndexes)];
    setAvailableMarkerIndexes(_availableMarkerIndexes);
    setMarkers(existingMarkers => [...existingMarkers, newMarker]);
  };

  const removeMarker = () => {
    const markerToRemove = randomPick(markers);
    setAvailableMarkerIndexes([...availableMarkerIndexes, markerToRemove.id]);
    setMarkers([...markers.filter(m => m.id !== markerToRemove.id)]);
  };

  const addDynamicMarker = () => {
    const dynamicMarker = {
      ...allMarkers[randomPick(availableMarkerIndexes)],
      markerName: 'flight',
      rotation: 90
    };

    let steps = 0;

    setMarkers([dynamicMarker]);
    setInterval(() => {
      steps += 1;
      if (steps >= 10) {
        return;
      }
      setMarkers([
        {
          ...dynamicMarker,
          markerAnimation: 'none',
          position: { ...dynamicMarker.position, lng: dynamicMarker.position.lng + 0.01 * steps }
        }
      ]);
    }, 1000);
  };

  const toggleDarkMode = () => {
    setStyles(styles.includes('dark') ? ['plain'] : ['plain', 'dark']);
  };

  return (
    <div className="h-100">
      <Map
        center={center}
        zoom={zoom}
        mapStyles={styles}
        markers={markers}
        markerAnimation="drop"
      />
      <OptionsPanel
        color="secondary"
        headerColor="primary"
        headerText="Map options"
        headerIcon="map"
      >
        <ButtoLink outline size="sm" block icon="center" className="mb-4" onClick={changeCenter}>
          Change map center
        </ButtoLink>
        <ButtoLink outline size="sm" block icon="zoom-in" className="mb-2" onClick={zoomIn}>
          Zoom in
        </ButtoLink>
        <ButtoLink outline size="sm" block icon="zoom-out" className="mb-4" onClick={zoomOut}>
          Zoom out
        </ButtoLink>
        <ButtoLink
          outline
          size="sm"
          block
          icon="add"
          className="mb-2"
          onClick={addMarker}
          disabled={!availableMarkerIndexes.length}
        >
          Add marker
        </ButtoLink>
        <ButtoLink
          outline
          size="sm"
          block
          icon="trash-o"
          className="mb-2"
          onClick={addDynamicMarker}
        >
          Add dynamic marker
        </ButtoLink>
        <ButtoLink
          outline
          size="sm"
          block
          icon="trash-o"
          className="mb-4"
          onClick={removeMarker}
          disabled={availableMarkerIndexes.length >= allMarkers.length}
        >
          Remove marker
        </ButtoLink>
        <ButtoLink
          outline
          size="sm"
          block
          icon={styles.includes('dark') ? 'circle-o' : 'circle'}
          className="mb-2"
          onClick={toggleDarkMode}
        >
          {styles.includes('dark') ? 'Remove' : 'Apply'} dark mode
        </ButtoLink>
      </OptionsPanel>
    </div>
  );
};

export default MapFullScreen;
