import ButtonLink from 'lib/ButtonLink';
import Input from 'lib/Input';
import { TopNavbar } from 'lib/Layout';
import React from 'react';
import logo from '../../../../assets/img/logo192.png';

const TopNavbarRightContent = () => {
  const RightContent = (
    <>
      <div>
        <Input icon="search" iconPosition="right" iconExtendible />
      </div>
      <div className="d-none d-md-block">
        <ButtonLink type="link">Link A</ButtonLink>
      </div>
      <div className="d-none d-md-block">
        <ButtonLink type="link">Link B</ButtonLink>
      </div>
      <div className="d-none d-lg-block">
        <ButtonLink type="link">Link C</ButtonLink>
      </div>
      <div className="d-none d-lg-block">
        <ButtonLink type="link">Link D</ButtonLink>
      </div>
    </>
  );

  return (
    <>
      <TopNavbar logo={logo} title="Mock application title" rightContent={RightContent} animated />
      <div className="center-screen">
        <div>
          <ButtonLink to="/components/top-navbar">Go back to TopNavbar page</ButtonLink>
        </div>
      </div>
    </>
  );
};

export default TopNavbarRightContent;
