import ButtonLink from 'lib/ButtonLink';
import { TopNavbar } from 'lib/Layout';
import React from 'react';

const TopNavbarColors = () => {
  return (
    <>
      <TopNavbar title="Mock application title" color="primary" />
      <div className="center-screen">
        <div>
          <ButtonLink to="/components/top-navbar">Go back to TopNavbar page</ButtonLink>
        </div>
      </div>
    </>
  );
};

export default TopNavbarColors;
