import ButtonLink from 'lib/ButtonLink';
import { TopNavbar } from 'lib/Layout';
import React from 'react';

const TopNavbarScrollHide = () => {
  return (
    <>
      <TopNavbar title="Mock application title" scrollHide />
      <div className="center-screen" style={{ height: 900 }}>
        <div>
          <ButtonLink to="/components/top-navbar">Go back to TopNavbar page</ButtonLink>
        </div>
      </div>
    </>
  );
};

export default TopNavbarScrollHide;
