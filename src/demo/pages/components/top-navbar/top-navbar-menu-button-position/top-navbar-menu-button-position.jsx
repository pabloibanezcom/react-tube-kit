import ButtonLink from 'lib/ButtonLink';
import { TopNavbar } from 'lib/Layout';
import React from 'react';

const TopNavbarMenuButtonPosition = () => {
  return (
    <>
      <TopNavbar title="Mock application title" menuButtonPosition="left" />
      <div className="center-screen">
        <div>
          <ButtonLink to="/components/top-navbar">Go back to TopNavbar page</ButtonLink>
        </div>
      </div>
    </>
  );
};

export default TopNavbarMenuButtonPosition;
