import Badge from 'lib/Badge';
import Breadcrumb from 'lib/Breadcrumb';
import ButtonLink from 'lib/ButtonLink';
import Card from 'lib/Card';
import Carousel from 'lib/Carousel';
import Checkbox from 'lib/Checkbox';
import CheckboxGroup from 'lib/CheckboxGroup';
import CollapsibleHeading from 'lib/CollapsibleHeading';
import CollapsibleList from 'lib/CollapsibleList';
import ColorLabel from 'lib/ColorLabel';
import ColorSelector from 'lib/ColorSelector';
import Toastr from 'lib/components/toastr/toastr';
import CountryLabel from 'lib/CountryLabel';
import Dropdown from 'lib/Dropdown';
import FileUpload from 'lib/FileUpload';
import Form from 'lib/Form';
import Icon from 'lib/Icon';
import Image from 'lib/Image';
import Input from 'lib/Input';
import Label from 'lib/Label';
import { BottomNavbar, LayoutWrapper, SideNavbar, TopNavbar } from 'lib/Layout';
import LoadingSpinner from 'lib/LoadingSpinner';
import Map from 'lib/Map';
import Modal from 'lib/Modal';
import Pagination from 'lib/Pagination';
import Panel from 'lib/Panel';
import PlaceSelector from 'lib/PlaceSelector';
import Radio from 'lib/Radio';
import RadioGroup from 'lib/RadioGroup';
import Selector from 'lib/Selector';
import Slider from 'lib/Slider';
import TabMenu from 'lib/TabMenu';
import Toggle from 'lib/Toggle';
import Tooltip from 'lib/Tooltip';

const getComponentsForParser = componentsString => {
  const componentNames = componentsString.split(' ');
  const result = {};
  componentNames.forEach(name => {
    result[name] = getComponentFromName(name);
  });
  return result;
};

const getComponentFromName = componentName => {
  switch (componentName) {
    case 'Badge':
      return Badge;
    case 'BottomNavbar':
      return BottomNavbar;
    case 'Breadcrumb':
      return Breadcrumb;
    case 'ButtonLink':
      return ButtonLink;
    case 'Card':
      return Card;
    case 'Carousel':
      return Carousel;
    case 'Checkbox':
      return Checkbox;
    case 'CheckboxGroup':
      return CheckboxGroup;
    case 'CollapsibleHeading':
      return CollapsibleHeading;
    case 'CollapsibleList':
      return CollapsibleList;
    case 'CountryLabel':
      return CountryLabel;
    case 'ColorLabel':
      return ColorLabel;
    case 'ColorSelector':
      return ColorSelector;
    case 'Dropdown':
      return Dropdown;
    case 'FileUpload':
      return FileUpload;
    case 'Form':
      return Form;
    case 'Icon':
      return Icon;
    case 'Image':
      return Image;
    case 'Input':
      return Input;
    case 'Label':
      return Label;
    case 'LayoutWrapper':
      return LayoutWrapper;
    case 'LoadingSpinner':
      return LoadingSpinner;
    case 'Map':
      return Map;
    case 'Modal':
      return Modal;
    case 'Pagination':
      return Pagination;
    case 'Panel':
      return Panel;
    case 'PlaceSelector':
      return PlaceSelector;
    case 'Radio':
      return Radio;
    case 'RadioGroup':
      return RadioGroup;
    case 'Selector':
      return Selector;
    case 'SideNavbar':
      return SideNavbar;
    case 'Slider':
      return Slider;
    case 'TabMenu':
      return TabMenu;
    case 'Toastr':
      return Toastr;
    case 'Toggle':
      return Toggle;
    case 'Tooltip':
      return Tooltip;
    case 'TopNavbar':
      return TopNavbar;
    default:
      return null;
  }
};

export default getComponentsForParser;
