import customProps from 'lib/util/customProps.json';
import SelectorColor from '../components/custom-components/selector-color/selector-color';

export const getFormElementFromProperty = prop => {
  const getFieldProps = () => {
    if (prop.type === 'string') {
      return {
        type: 'input',
        hideInPlayground: prop.hideInPlayground,
        fieldProps: {
          value: prop.default
        }
      };
    }
    if (prop.type === 'bool') {
      return {
        type: 'selector',
        hideInPlayground: prop.hideInPlayground,
        fieldProps: {
          options: [
            { value: true, name: 'Yes' },
            { value: false, name: 'No' }
          ],
          value: prop.default,
          valueProp: 'value'
        }
      };
    }
    if (prop.type === 'color') {
      return {
        type: 'selector',
        hideInPlayground: prop.hideInPlayground,
        fieldProps: {
          options: customProps.color.options.map(o => {
            return { value: o, name: o };
          }),
          custom: SelectorColor,
          customProp: 'color',
          value: prop.default,
          valueProp: 'value'
        }
      };
    }
    if (prop.type === 'position') {
      return {
        type: 'selector',
        hideInPlayground: prop.hideInPlayground,
        fieldProps: {
          options: customProps.position.options.map(o => {
            return { value: o, name: `${o.charAt(0).toUpperCase()}${o.slice(1)}` };
          }),
          value: prop.default,
          valueProp: 'value'
        }
      };
    }
    return null;
  };

  return Object.assign({}, { name: prop.name, label: prop.name }, getFieldProps());
};
