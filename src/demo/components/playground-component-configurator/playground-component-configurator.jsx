/* eslint-disable react-hooks/exhaustive-deps */
import componentsData from 'lib/components/data';
import Form from 'lib/Form';
import React, { useEffect, useState } from 'react';
import ButtonLink from '../../../lib/components/button-link/button-link';
import { getFormElementFromProperty } from '../../util/playground';

const PlaygroundComponentConfigurator = ({ componentName, custom, resetText, onChange }) => {
  const [properties, setProperties] = useState();
  const [defaultFormData, setDefaultFormData] = useState();
  const [formData, setFormData] = useState();

  useEffect(() => {
    const cData = componentsData[componentName];
    if (custom) {
      Object.keys(custom).forEach(prop => {
        cData.properties[prop].default = custom[prop];
      });
    }
    const _properties = Object.keys(cData.properties)
      .map(propName =>
        getFormElementFromProperty({ ...cData.properties[propName], name: propName })
      )
      .filter(fEl => fEl.type && !fEl.hideInPlayground);

    const _defaultFormData = _properties.reduce((acc, cur) => {
      acc[cur.name] = cur.fieldProps.value;
      cur.fieldProps.value = undefined;
      return acc;
    }, {});

    setProperties(_properties);
    setDefaultFormData(_defaultFormData);
    setFormData({ ..._defaultFormData });
  }, []);

  const handleConfigurationChange = data => {
    onChange(data);
  };

  const handleOnValueChange = data => {
    setFormData({ ...data });
  };

  const resetFormData = () => {
    setFormData({ ...defaultFormData });
    handleConfigurationChange({ ...defaultFormData });
  };

  return (
    <div className="border rounded px-4 py-4">
      <Form
        autoSubmit
        className="row"
        formFieldClassName="col-xl-2 col-md-4"
        fields={properties}
        value={formData}
        onValueChange={handleOnValueChange}
        onSubmit={handleConfigurationChange}
      />
      <div className="row">
        <div className="col-xl-2 col-lg-4">
          <ButtonLink block onClick={resetFormData}>
            {resetText}
          </ButtonLink>
        </div>
      </div>
    </div>
  );
};

export default PlaygroundComponentConfigurator;
