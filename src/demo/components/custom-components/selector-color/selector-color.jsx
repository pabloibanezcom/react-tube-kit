import Icon from 'lib/Icon';
import PropTypes from 'prop-types';
import React from 'react';

const SelectorColor = ({ className, color: { name } }) => {
  return (
    <div className={`d-flex align-items-center selector-color ${className}`}>
      <Icon iconName="square" size="sm" color={name} className="mr-2" />
      {name.charAt(0).toUpperCase() + name.slice(1)}
    </div>
  );
};

SelectorColor.defaultProps = {
  className: ''
};

SelectorColor.propTypes = {
  className: PropTypes.string,
  color: PropTypes.shape({
    name: PropTypes.string
  }).isRequired
};

export default SelectorColor;
