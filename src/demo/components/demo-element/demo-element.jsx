import ButtonLink from 'lib/ButtonLink';
import Icon from 'lib/Icon';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import JsxParser from 'react-jsx-parser';
import { withRouter } from 'react-router-dom';
import getComponentsForParser from '../../util/getComponentsForParser';

require('codemirror/mode/xml/xml');

const DemoElement = ({
  history,
  component,
  example: {
    id,
    name,
    html,
    extraHtml,
    extraComponents,
    multi,
    bindings,
    actions,
    disableCode,
    disableExample
  },
  options
}) => {
  const [showCode, setShowCode] = useState(false);
  const [internalBindings, setInternalBindings] = useState(bindings);
  const [actionFlags, setActionFlags] = useState(
    actions ? Object.assign(...actions.map(act => ({ [act.propName]: false }))) : null
  );

  const fireAction = action => {
    if (action.propName) {
      setActionFlags({
        ...actionFlags,
        [action.propName]: action.propValue || !actionFlags[action.propName]
      });
    }
    if (action.to) {
      history.push(action.to);
    }
    if (action.func) {
      action.func(id);
    }
    if (action.updateBindings) {
      setInternalBindings(action.updateBindings());
    }
  };

  const actionButtons = () =>
    actions.map((act, i) => (
      <ButtonLink
        key={i}
        color={null}
        uppercase={false}
        hoverColor="primary"
        className="demo-element__btn demo-show-code-btn d-flex align-items-center ml-3 px-1"
        onClick={() => fireAction(act)}
      >
        <Icon iconName={act.icon} className="mr-2 opacity-7" size="sm" />
        {actionFlags[act.propName] && act.textActive ? act.textActive : act.text}
      </ButtonLink>
    ));

  return (
    <div name={id} className="demo-element">
      <h3 className="mb-2">{name}</h3>
      <div className="demo-element__container">
        <div
          className={`demo-element__show w-${options.width} ${
            options.flexColumn ? 'flexColumn' : ''
          } ${multi ? 'multi' : ''}`}
        >
          {!disableExample ? (
            <JsxParser
              bindings={{ ...internalBindings, ...actionFlags }}
              components={getComponentsForParser(
                !extraComponents ? component : `${component} ${extraComponents.join(' ')}`
              )}
              jsx={`${html}`}
              blacklistedAttrs={[]}
            />
          ) : null}
        </div>
        {!disableCode ? (
          <div className="demo-element__code">
            <div className="demo-element__buttons d-flex">
              {actions ? actionButtons() : null}
              <ButtonLink
                color={null}
                uppercase={false}
                hoverColor="primary"
                className="demo-element__btn demo-show-code-btn d-flex align-items-center ml-3 px-1"
                onClick={() => setShowCode(!showCode)}
              >
                <Icon iconName="code" className="mr-2 opacity-7" size="sm" />
                {!showCode ? 'Show code' : 'Hide code'}
              </ButtonLink>
            </div>
            <div className={`demo-element__codemirror ${showCode ? 'shown' : ''}`}>
              <div className="demo-element__codemirror__overlay" />
              <CodeMirror
                value={html}
                options={{
                  mode: 'xml',
                  theme: 'material',
                  lineNumbers: false,
                  smartIndent: true,
                  indentWithTabs: true,
                  readOnly: true
                }}
              />
            </div>
          </div>
        ) : null}
      </div>
      {extraHtml ? <div className="mt-4" dangerouslySetInnerHTML={{ __html: extraHtml }} /> : null}
    </div>
  );
};

DemoElement.defaultProps = {
  options: {
    width: 'default'
  }
};

DemoElement.propTypes = {
  component: PropTypes.string.isRequired,
  example: PropTypes.shape({
    actions: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
        textActive: PropTypes.string,
        propName: PropTypes.string
      })
    ),
    bindings: PropTypes.object,
    disableCode: PropTypes.bool,
    disableExample: PropTypes.bool,
    extraHtml: PropTypes.string,
    extraComponents: PropTypes.arrayOf(PropTypes.string),
    html: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    multi: PropTypes.bool,
    url: PropTypes.string
  }).isRequired,
  options: PropTypes.shape({
    width: PropTypes.string
  })
};

export default withRouter(DemoElement);
