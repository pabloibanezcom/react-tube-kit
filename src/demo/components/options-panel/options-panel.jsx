import Panel from 'lib/Panel';
import React from 'react';

const OptionsPanel = ({ children, color, headerColor, headerIcon, headerText }) => {
  return (
    <div className="options-panel-container">
      <Panel
        color={color}
        headerColor={headerColor}
        headerIcon={headerIcon}
        headerText={headerText}
        className="options-panel rounded"
      >
        {children}
      </Panel>
    </div>
  );
};

export default OptionsPanel;
