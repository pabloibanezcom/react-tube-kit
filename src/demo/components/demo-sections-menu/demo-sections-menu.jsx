import { isEmpty } from 'lib/util/object';
import React, { Fragment } from 'react';
import { Link } from 'react-scroll';

const DemoSectionsMenu = ({ examples, componentData: { properties, methods, events } }) => {
  const apiLink = name => {
    return (
      <li>
        <Link
          to={name.toLowerCase()}
          smooth
          offset={-70}
          duration={500}
          className="text--dark opacity-8 text-hover-darken--primary"
        >
          {name}
        </Link>
      </li>
    );
  };

  return (
    <div className="demo-sections-menu">
      <h4 className="mb-1">
        <Link
          to="overview"
          className="text--secondary text-hover-darken--primary"
          smooth
          offset={-70}
          duration={500}
        >
          Overview
        </Link>
      </h4>
      {examples && examples.length ? (
        <Fragment>
          <h4 className="mb-1 text--secondary mt-4">Examples</h4>
          <ul>
            {examples.map(example => (
              <li key={example.id}>
                <Link
                  to={example.id}
                  smooth
                  offset={-70}
                  duration={500}
                  className="text--dark opacity-8 text-hover-darken--primary"
                >
                  {example.name}
                </Link>
              </li>
            ))}
          </ul>
        </Fragment>
      ) : null}
      {properties || methods || events ? (
        <Fragment>
          <h4 className="mb-1 text--secondary mt-4">API Reference</h4>
          <ul>
            {!isEmpty(properties) ? apiLink('Properties') : null}
            {!isEmpty(methods) ? apiLink('Methods') : null}
            {!isEmpty(events) ? apiLink('Events') : null}
          </ul>
        </Fragment>
      ) : null}
    </div>
  );
};

export default DemoSectionsMenu;
