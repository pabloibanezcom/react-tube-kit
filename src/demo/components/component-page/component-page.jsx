import ButtonLink from 'lib/ButtonLink';
import React, { Fragment, useEffect } from 'react';
import { animateScroll } from 'react-scroll';
import { getComponentFromComponentName } from '../../util/getComponent';
import CodeHighlight from '../code-highlight/code-highlight';
import DemoElement from '../demo-element/demo-element';
import DemoPropsTable from '../demo-props-table/demo-props-table';
import DemoSectionsMenu from '../demo-sections-menu/demo-sections-menu';

const ComponentPage = ({ componentData, examples }) => {
  useEffect(() => {
    animateScroll.scrollToTop();
  }, []);

  const componentCode = `import ${
    componentData.component
  } from 'react-tube-kit/${componentData.namespace || componentData.component}';`;

  const overviewSection = (
    <div name="overview">
      <h2 className="mb-2">Overview</h2>
      {componentData.description ? <p>{componentData.description}</p> : null}
      {componentData.descriptionHtml ? (
        <div dangerouslySetInnerHTML={{ __html: componentData.descriptionHtml }} />
      ) : null}
      <div className="d-flex align-items-center">
        <span className="font-weight-normal mr-2">Created:</span>
        <ButtonLink type="link" color="secondary" to="/releases" hash={componentData.created}>
          {componentData.created}
        </ButtonLink>
      </div>
      <div className="d-flex align-items-center">
        <span className="font-weight-normal mr-2">Last modified:</span>
        <ButtonLink type="link" color="secondary" to="/releases" hash={componentData.lastModified}>
          {componentData.lastModified}
        </ButtonLink>
      </div>
      <div className="d-flex align-items-center">
        <span className="font-weight-normal mr-2">Component dependencies:</span>
        {!componentData.dependencies.length ? (
          <span>None</span>
        ) : (
          componentData.dependencies.map(dep => (
            <ButtonLink
              key={dep}
              className="mr-1"
              color="secondary"
              badge
              to={getComponentFromComponentName(dep).url}
            >
              {dep}
            </ButtonLink>
          ))
        )}
      </div>
      <div>
        <span className="font-weight-normal mr-2">Library dependencies:</span>
        {!componentData.libraryDependencies.length ? (
          <span>None</span>
        ) : (
          componentData.libraryDependencies.map(dep => (
            <ButtonLink
              key={dep.name}
              badge
              className="mr-1"
              uppercase={false}
              href={dep.url}
              newPage
            >
              {dep.name}
            </ButtonLink>
          ))
        )}
      </div>
      <CodeHighlight className="mt-4" mode="jsx">
        {componentCode}
      </CodeHighlight>
    </div>
  );

  const examplesSection = (
    <Fragment>
      {examples && examples.length ? (
        <div name="examples" className="mt-6">
          <h2 className="mb-6">Examples</h2>
          {examples.map(example => (
            <DemoElement key={example.id} component={componentData.component} example={example} />
          ))}
        </div>
      ) : null}
    </Fragment>
  );

  const apiSection = (
    <div name="api" className="mt-6">
      <h2 className="mb-6">API reference</h2>
      <DemoPropsTable componentData={componentData} />
    </div>
  );

  return (
    <div className="row">
      <div className="col-lg-12">
        <h1 className="right-line mb-8">{componentData.name}</h1>
        <div className="row justify-content-end">
          <div className="col-lg-9">
            {overviewSection}
            {examplesSection}
            {apiSection}
          </div>
          <div className="col-lg-3 .d-none .d-md-block">
            <DemoSectionsMenu examples={examples} componentData={componentData} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ComponentPage;
